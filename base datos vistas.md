

create or REPLACE VIEW `poly3CertificadosPeopleEjemplo`
AS SELECT
   count(`skill_matrizs`.`head_count_id`) AS `total`,
   `work_stations`.`Program_id` AS `Program_id`
FROM `skill_matrizs` , `head_counts` , trade_rols , `work_stations` where (((`skill_matrizs`.`EXPERT` = '1A') or (`skill_matrizs`.`EXPERT` = '2A') or (`skill_matrizs`.`EXPERT` = '3A') or (`skill_matrizs`.`EXPERT` = '1AQ') or (`skill_matrizs`.`EXPERT` = '2AQ') or (`skill_matrizs`.`EXPERT` = '3AQ') or (`skill_matrizs`.`EXPERT` = '1Q') or (`skill_matrizs`.`EXPERT` = '2Q') or (`skill_matrizs`.`EXPERT` = '3Q')) and (`head_counts`.`id` = `skill_matrizs`.`head_count_id`) and isnull(`head_counts`.`FinalDate`) and (`work_stations`.`id` = `skill_matrizs`.`worstation_id`)
 AND head_counts.Traderol_id = trade_rols.id  AND trade_rols.Program_id = work_stations.`Program_id` ) group by `skill_matrizs`.`head_count_id`,`work_stations`.`Program_id`;


create or REPLACE VIEW `POLYPEOPLETREE`
AS SELECT
   sum((case when (`skill_matrizs`.`EXPERT` = '1A') then 1 when (`skill_matrizs`.`EXPERT` = '2A') then 1 when (`skill_matrizs`.`EXPERT` = '3A') then 1 when (`skill_matrizs`.`EXPERT` = '1AQ') then 1 when (`skill_matrizs`.`EXPERT` = '2AQ') then 1 when (`skill_matrizs`.`EXPERT` = '3AQ') then 1 when (`skill_matrizs`.`EXPERT` = '1Q') then 1 when (`skill_matrizs`.`EXPERT` = '2Q') then 1 when (`skill_matrizs`.`EXPERT` = '3Q') then 1 else 0 end)) AS `C`,
   `skill_matrizs`.`head_count_id` AS `head_count_id`
FROM `skill_matrizs` , `head_counts` , trade_rols,work_stations where  trade_rols.Program_id = work_stations.`Program_id`  AND `work_stations`.`id` = `skill_matrizs`.`worstation_id`  
AND `head_counts`.`id` = `skill_matrizs`.`head_count_id` 
and isnull(`head_counts`.`FinalDate`) 
AND trade_rols.id = head_counts.Traderol_id 
group by `skill_matrizs`.`head_count_id`;





create or REPLACE VIEW `PolyPolyOne`
AS SELECT
   (case when (count(`skill_matrizs`.`worstation_id`) >= `work_stations`.`TrainingNeeds`) then `work_stations`.`TrainingNeeds` else count(`skill_matrizs`.`worstation_id`) end) AS `totales`,count(`skill_matrizs`.`worstation_id`) AS `total`,
   `work_stations`.`TrainingNeeds` AS `TrainingNeeds`,
   `work_stations`.`WorkStationID` AS `WorkStationID`,
   `work_stations`.`Program_id` AS `Program_id`
FROM `skill_matrizs` , `work_stations`,`head_counts` , trade_rols where ((`work_stations`.`id` = `skill_matrizs`.`worstation_id`) and (`work_stations`.`ProductionUnit_id` = 1)) AND trade_rols.id = head_counts.Traderol_id and isnull(`head_counts`.`FinalDate`) AND `head_counts`.`id` = `skill_matrizs`.`head_count_id` AND trade_rols.Program_id = work_stations.`Program_id`   group by `skill_matrizs`.`worstation_id`;





create or REPLACE VIEW `PolyPolyOneTresTres`
AS SELECT
   count(`skill_matrizs`.`worstation_id`) AS `total`,
   `work_stations`.`WorkStationID` AS `WorkStationID`,
   `work_stations`.`Program_id` AS `Program_id`
FROM `skill_matrizs` , `work_stations`, `head_counts`,trade_rols where ((`work_stations`.`id` = `skill_matrizs`.`worstation_id`) and (`work_stations`.`ProductionUnit_id` = 1) and ((`skill_matrizs`.`EXPERT` = '1A') or (`skill_matrizs`.`EXPERT` = '2A') or (`skill_matrizs`.`EXPERT` = '3A') or (`skill_matrizs`.`EXPERT` = '1AQ') or (`skill_matrizs`.`EXPERT` = '2AQ') or (`skill_matrizs`.`EXPERT` = '3AQ') or (`skill_matrizs`.`EXPERT` = '1Q') or (`skill_matrizs`.`EXPERT` = '2Q') or (`skill_matrizs`.`EXPERT` = '3Q')) and (`head_counts`.`id` = `skill_matrizs`.`head_count_id`) and isnull(`head_counts`.`FinalDate`)) AND trade_rols.Program_id = work_stations.`Program_id` AND trade_rols.id = head_counts.Traderol_id     group by `skill_matrizs`.`worstation_id` having (count(`skill_matrizs`.`worstation_id`) >= 3);



create or REPLACE VIEW  `PolyPolyTotal`
AS SELECT
   (case when (count(`skill_matrizs`.`worstation_id`) >= `work_stations`.`TrainingNeeds`) then `work_stations`.`TrainingNeeds` else count(`skill_matrizs`.`worstation_id`) end) AS `totales`,count(`skill_matrizs`.`worstation_id`) AS `total`,
   `work_stations`.`TrainingNeeds` AS `TrainingNeeds`,
   `work_stations`.`WorkStationID` AS `WorkStationID`,
   `work_stations`.`Program_id` AS `Program_id`
FROM `skill_matrizs` , `work_stations`,`head_counts` , trade_rols where (`work_stations`.`id` = `skill_matrizs`.`worstation_id`) AND trade_rols.id = head_counts.Traderol_id 
AND trade_rols.Program_id = work_stations.`Program_id` AND  trade_rols.Program_id = work_stations.`Program_id` 
group by `skill_matrizs`.`worstation_id`;





create or REPLACE VIEW  
`PolyPolyTwo`
AS SELECT
   (case when (count(`skill_matrizs`.`worstation_id`) >= `work_stations`.`TrainingNeeds`) then `work_stations`.`TrainingNeeds` else count(`skill_matrizs`.`worstation_id`) end) AS `totales`,count(`skill_matrizs`.`worstation_id`) AS `total`,
   `work_stations`.`TrainingNeeds` AS `TrainingNeeds`,
   `work_stations`.`WorkStationID` AS `WorkStationID`,
   `work_stations`.`Program_id` AS `Program_id`
FROM `skill_matrizs` , `work_stations`,`head_counts` ,trade_rols where ((`work_stations`.`id` = `skill_matrizs`.`worstation_id`) and (`work_stations`.`ProductionUnit_id` = 2)) AND trade_rols.id = head_counts.Traderol_id 
AND trade_rols.Program_id = work_stations.`Program_id` AND  trade_rols.Program_id = work_stations.`Program_id` 
 group by `skill_matrizs`.`worstation_id`;






create or REPLACE VIEW  
`PolyPolyTwoTresTres`
AS SELECT
   count(`skill_matrizs`.`worstation_id`) AS `total`,
   `work_stations`.`WorkStationID` AS `WorkStationID`,
   `work_stations`.`Program_id` AS `Program_id`
FROM `skill_matrizs` , `work_stations` , `head_counts`,`trade_rols` where ((`work_stations`.`id` = `skill_matrizs`.`worstation_id`) and (`work_stations`.`ProductionUnit_id` = 2) and ((`skill_matrizs`.`EXPERT` = '1A') or (`skill_matrizs`.`EXPERT` = '2A') or (`skill_matrizs`.`EXPERT` = '3A') or (`skill_matrizs`.`EXPERT` = '1AQ') or (`skill_matrizs`.`EXPERT` = '2AQ') or (`skill_matrizs`.`EXPERT` = '3AQ')) and (`head_counts`.`id` = `skill_matrizs`.`head_count_id`) and isnull(`head_counts`.`FinalDate`)) AND trade_rols.id = head_counts.Traderol_id 
AND trade_rols.Program_id = work_stations.`Program_id` AND  trade_rols.Program_id = work_stations.`Program_id`   group by `skill_matrizs`.`worstation_id` having (count(`skill_matrizs`.`worstation_id`) >= 3);




create or REPLACE VIEW  
`skillMatrizTabla`
AS SELECT
   sum((case when (`skill_matrizs`.`EXPERT` = 'T') then 1 else 0 end)) AS `T`,sum((case when (`skill_matrizs`.`EXPERT` = '1A') then 1 when (`skill_matrizs`.`EXPERT` = '2A') then 1 when (`skill_matrizs`.`EXPERT` = '3A') then 1 when (`skill_matrizs`.`EXPERT` = '1AQ') then 1 when (`skill_matrizs`.`EXPERT` = '2AQ') then 1 when (`skill_matrizs`.`EXPERT` = '3AQ') then 1 when (`skill_matrizs`.`EXPERT` = '1Q') then 1 when (`skill_matrizs`.`EXPERT` = '2Q') then 1 when (`skill_matrizs`.`EXPERT` = '3Q') then 1 else 0 end)) AS `C`,
   `work_stations`.`NivelDeRiesgoOp` AS `NivelDeRiesgoOp`,
   `work_stations`.`TrainingNeeds` AS `N`,
   `work_stations`.`Name` AS `Wwork_stationsName`,
   `programs`.`Name` AS `Programs_Name`,
   `work_stations`.`id` AS `WroktationID`,
   `programs`.`id` AS `Programs_Id`
FROM `skill_matrizs` , `work_stations` ,  `programs` , `head_counts`,trade_rols where ((`work_stations`.`id` = `skill_matrizs`.`worstation_id`) and (`programs`.`id` = `work_stations`.`Program_id`) and (`head_counts`.`id` = `skill_matrizs`.`head_count_id`) and isnull(`head_counts`.`FinalDate`)) AND trade_rols.id = head_counts.Traderol_id 
AND trade_rols.Program_id = work_stations.`Program_id` AND  trade_rols.Program_id = work_stations.`Program_id` 
group by `work_stations`.`id`;
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductionUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_units', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Code')->nullable();
            $table->string('Name')->nullable();
            $table->string('Description')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('production_units');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateManufacturingInstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturing_instructions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Code')->nullable();
            $table->string('Name')->nullable();
            $table->string('Description')->nullable();
            $table->string('Program')->nullable();
            $table->string('WS')->nullable();
            $table->string('NumePart')->nullable();
            $table->string('Competences')->nullable();
            $table->string('Revision')->nullable();
            $table->date('FechaInicio')->nullable();
            $table->date('FechaFin')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('manufacturing_instructions');
    }
}

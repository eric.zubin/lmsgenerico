<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompetivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competives', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Name')->nullable();
            $table->string('Code')->nullable();
            $table->string('Type')->nullable();
            $table->string('Level')->nullable();
            $table->mediumText('Specs')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('competives');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Code')->nullable();
            $table->string('Name')->nullable();
            $table->string('Type')->nullable();
            $table->string('Instructor')->nullable();
            $table->string('Level')->nullable();
            $table->string('Language')->nullable();
            $table->string('Service')->nullable();
            $table->string('Material')->nullable();
            $table->string('Archivos')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trainings');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHeadCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('head_counts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Num')->nullable();
            $table->string('Name')->nullable();
            $table->string('TradeRol')->nullable();
            $table->string('Company')->nullable();
            $table->string('Sindicalizado')->nullable();
            $table->string('NivelPuesto')->nullable();
            $table->string('Puesto')->nullable();
            $table->string('Email')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('head_counts');
    }
}

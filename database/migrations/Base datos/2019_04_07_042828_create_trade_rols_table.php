<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTradeRolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_rols', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('ProductionUnit')->nullable();
            $table->string('Program')->nullable();
            $table->string('Code')->nullable();
            $table->string('Name')->nullable();
            $table->string('Description')->nullable();
            $table->string('Competives')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trade_rols');
    }
}

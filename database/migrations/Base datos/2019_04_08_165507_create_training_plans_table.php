<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrainingPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Code')->nullable();
            $table->string('Instructor')->nullable();
            $table->string('FechaInicio')->nullable();
            $table->string('FechaFin')->nullable();
            $table->string('Training')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('training_plans');
    }
}

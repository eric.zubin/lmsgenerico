<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSkillMatrizsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_matrizs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('NUM')->nullable();
            $table->string('NOMBRE')->nullable();
            $table->string('WS1')->nullable();
            $table->string('WS2')->nullable();
            $table->string('WS3')->nullable();
            $table->string('WS4')->nullable();
            $table->string('EXPERT')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('skill_matrizs');
    }
}

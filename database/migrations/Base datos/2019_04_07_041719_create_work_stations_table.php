<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_stations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('ProductionUnit')->nullable();
            $table->string('Program')->nullable();
            $table->string('WorkStationID')->nullable();
            $table->string('Name')->nullable();
            $table->string('Description')->nullable();
            $table->string('NivelDeRiesgoOp')->nullable();
            $table->dateTime('FechaInicio')->nullable();
            $table->dateTime('FechaFin')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_stations');
    }
}

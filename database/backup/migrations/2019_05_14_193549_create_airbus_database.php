<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAirbusDatabase extends Migration {

        /**
         * Run the migrations.
         *
         * @return void
         */
         public function up()
         {
            
      /**
       * Migration schema for table activity_log
         * 
       */
      Schema::create('activity_log', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->string('log_name', 255)->nullable();
                $table->text('description');
                $table->integer('subject_id')->nullable();
                $table->string('subject_type', 255)->nullable();
                $table->integer('causer_id')->nullable();
                $table->string('causer_type', 255)->nullable();
                $table->text('properties')->nullable();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
            });


      /**
       * Migration schema for table cells
         * 
       */
      Schema::create('cells', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Code', 255)->nullable();
                $table->string('Description', 255)->nullable();
                $table->string('Program', 255)->nullable();
                $table->string('ProductionUnit', 255)->nullable();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table company_instructors
         * 
       */
      Schema::create('company_instructors', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->string('Code', 255)->nullable();
                $table->string('Name', 255)->nullable();
            });


      /**
       * Migration schema for table competive_specs
         * 
       */
      Schema::create('competive_specs', function(Blueprint $table) {
                $table->integer('competive_id')->unsigned();
                $table->integer('spec_id')->nullable()->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table competives
         * 
       */
      Schema::create('competives', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Code', 255)->nullable();
                $table->string('Type', 255)->nullable();
                $table->string('Level', 255)->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->string('CriticalJOB', 250)->nullable();
            });


      /**
       * Migration schema for table file_trainigs
         * 
       */
      Schema::create('file_trainigs', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->string('Archivo', 250)->nullable();
                $table->integer('Trainig_id')->nullable()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table head_counts
         * 
       */
      Schema::create('head_counts', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Num', 255)->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Company', 255)->nullable();
                $table->string('Sindicalizado', 255)->nullable();
                $table->string('NivelPuesto', 255)->nullable();
                $table->string('Puesto', 255)->nullable();
                $table->string('Email', 255)->nullable();
                $table->integer('Traderol_id')->nullable()->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table instructors
         * 
       */
      Schema::create('instructors', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->string('Num', 255)->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Company', 255)->nullable();
                $table->integer('company_instructor_id')->nullable()->unsigned();
                $table->tinyInteger('Status')->nullable();
            });


      /**
       * Migration schema for table manufacturing_instructions
         * 
       */
      Schema::create('manufacturing_instructions', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Code', 255)->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Description', 255)->nullable();
                $table->string('ProductionArea', 255)->nullable();
                $table->integer('Program_id')->nullable()->unsigned();
                $table->integer('WS_id')->nullable()->unsigned();
                $table->string('NumePart', 255)->nullable();
                $table->string('Revision', 255)->nullable();
                $table->date('FechaInicio')->nullable();
                $table->date('FechaFin')->nullable();
                $table->integer('ProductionUnit_id')->nullable()->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table material_trainigs
         * 
       */
      Schema::create('material_trainigs', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->string('Archivo', 250)->nullable();
                $table->integer('Trainig_id')->nullable()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table mi_competive
         * 
       */
      Schema::create('mi_competive', function(Blueprint $table) {
                $table->integer('mi_id')->nullable()->unsigned();
                $table->integer('competive_id')->nullable()->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table password_resets
         * 
       */
      Schema::create('password_resets', function(Blueprint $table) {
                $table->string('email', 255);
                $table->string('token', 255);
                $table->timestamp('created_at')->nullable();
            });


      /**
       * Migration schema for table permission_role
         * 
       */
      Schema::create('permission_role', function(Blueprint $table) {
                $table->integer('permission_id')->primary()->unsigned();
                $table->integer('role_id')->primary()->unsigned();
            });


      /**
       * Migration schema for table permissions
         * 
       */
      Schema::create('permissions', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->string('name', 255);
                $table->string('label', 255)->nullable();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
            });


      /**
       * Migration schema for table production_units
         * 
       */
      Schema::create('production_units', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Code', 255)->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Description', 255)->nullable();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table profiles
         * 
       */
      Schema::create('profiles', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->integer('user_id')->nullable()->unsigned();
                $table->text('bio')->nullable();
                $table->string('gender', 255)->nullable();
                $table->date('dob')->nullable();
                $table->string('pic', 255)->nullable();
                $table->string('country', 255)->nullable();
                $table->string('state', 255)->nullable();
                $table->string('city', 255)->nullable();
                $table->string('address', 255)->nullable();
                $table->string('postal', 255)->nullable();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
            });


      /**
       * Migration schema for table programs
         * 
       */
      Schema::create('programs', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Code', 255)->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Description', 255)->nullable();
                $table->integer('ProductionUnit_id')->nullable()->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table role_user
         * 
       */
      Schema::create('role_user', function(Blueprint $table) {
                $table->integer('role_id')->primary()->unsigned();
                $table->integer('user_id')->primary()->unsigned();
            });


      /**
       * Migration schema for table roles
         * 
       */
      Schema::create('roles', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->string('name', 255);
                $table->string('label', 255)->nullable();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
            });


      /**
       * Migration schema for table social_accounts
         * 
       */
      Schema::create('social_accounts', function(Blueprint $table) {
                $table->integer('user_id');
                $table->string('provider_user_id', 255);
                $table->string('provider', 255);
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
            });


      /**
       * Migration schema for table specs
         * 
       */
      Schema::create('specs', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Code', 255)->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->string('Version', 100)->nullable();
            });


      /**
       * Migration schema for table trade_rols
         * 
       */
      Schema::create('trade_rols', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->integer('ProductionUnit_id')->nullable()->unsigned();
                $table->integer('Program_id')->nullable()->unsigned();
                $table->string('Code', 255)->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Description', 255)->nullable();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table tradelrol_competive
         * 
       */
      Schema::create('tradelrol_competive', function(Blueprint $table) {
                $table->integer('competive_id')->unsigned();
                $table->integer('traderol_id')->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table tradingplan_headcount
         * 
       */
      Schema::create('tradingplan_headcount', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->integer('trading_plan_id')->nullable()->unsigned();
                $table->integer('head_count_id')->nullable()->unsigned();
            });


      /**
       * Migration schema for table training_plans
         * 
       */
      Schema::create('training_plans', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Code', 255)->nullable();
                $table->dateTime('FechaInicio')->nullable();
                $table->dateTime('FechaFin')->nullable();
                $table->integer('Training_id')->nullable()->unsigned();
                $table->timestamp('deleted_at')->nullable();
                $table->integer('CountPersonal')->nullable();
            });


      /**
       * Migration schema for table training_requests
         * 
       */
      Schema::create('training_requests', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('User', 250)->nullable();
                $table->integer('Training_id')->nullable()->unsigned();
                $table->string('DateToday', 250)->nullable();
                $table->string('DateRequest', 250)->nullable();
                $table->string('Employees', 250)->nullable();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table trainings
         * 
       */
      Schema::create('trainings', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->string('Code', 255)->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Type', 100)->nullable();
                $table->string('Level', 100)->nullable();
                $table->string('Language', 100)->nullable();
                $table->string('Version', 255)->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->string('CriticalJOB', 100)->nullable();
                $table->string('Objetivo', 250)->nullable();
            });


      /**
       * Migration schema for table trainings_competive
         * 
       */
      Schema::create('trainings_competive', function(Blueprint $table) {
                $table->integer('trainings_id')->nullable()->unsigned();
                $table->integer('competive_id')->nullable()->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table trainings_instructors
         * 
       */
      Schema::create('trainings_instructors', function(Blueprint $table) {
                $table->integer('trainings_id')->unsigned();
                $table->integer('instructors_id')->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table users
         * 
       */
      Schema::create('users', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->string('name', 255);
                $table->string('email', 255);
                $table->string('password', 255)->nullable();
                $table->string('provider_id', 255)->nullable();
                $table->string('provider', 255)->nullable();
                $table->integer('status')->nullable()->default(1);
                $table->string('remember_token', 100)->nullable();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table work_stations
         * 
       */
      Schema::create('work_stations', function(Blueprint $table) {
                $table->integer('id')->autoIncrement()->unsigned();
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
                $table->integer('Program_id')->nullable()->unsigned();
                $table->string('WorkStationID', 255)->nullable();
                $table->string('Name', 255)->nullable();
                $table->string('Description', 255)->nullable();
                $table->string('NivelDeRiesgoOp', 255)->nullable();
                $table->dateTime('FechaInicio')->nullable();
                $table->dateTime('FechaFin')->nullable();
                $table->integer('ProductionUnit_id')->nullable()->unsigned();
                $table->tinyInteger('Status')->nullable();
                $table->integer('Certificaciones')->nullable();
                $table->integer('TrainingNeeds')->nullable();
                $table->timestamp('deleted_at')->nullable();
            });


      /**
       * Migration schema for table work_stations_competive
         * 
       */
      Schema::create('work_stations_competive', function(Blueprint $table) {
                $table->integer('work_stations_id')->nullable()->unsigned();
                $table->integer('competive_id')->nullable()->unsigned();
                $table->timestamp('deleted_at')->nullable();
            });



            
                
                
                
                
            Schema::table('competive_specs', function(Blueprint $table) {
                $table->foreign('competive_id')->references('id')->on('competives')->onDelete('RESTRICT');
                $table->foreign('spec_id')->references('id')->on('specs')->onDelete('RESTRICT');
            });


                
                
            Schema::table('file_trainigs', function(Blueprint $table) {
                $table->foreign('Trainig_id')->references('id')->on('trainings')->onDelete('RESTRICT');
            });


                
            Schema::table('head_counts', function(Blueprint $table) {
                $table->foreign('Traderol_id')->references('id')->on('trade_rols')->onDelete('RESTRICT');
            });


                
            Schema::table('instructors', function(Blueprint $table) {
                $table->foreign('company_instructor_id')->references('id')->on('company_instructors')->onDelete('RESTRICT');
            });


                
            Schema::table('manufacturing_instructions', function(Blueprint $table) {
                $table->foreign('ProductionUnit_id')->references('id')->on('production_units')->onDelete('NO ACTION');
                $table->foreign('Program_id')->references('id')->on('programs')->onDelete('RESTRICT');
                $table->foreign('WS_id')->references('id')->on('work_stations')->onDelete('RESTRICT');
            });


                
            Schema::table('material_trainigs', function(Blueprint $table) {
                $table->foreign('Trainig_id')->references('id')->on('trainings')->onDelete('RESTRICT');
            });


                
            Schema::table('mi_competive', function(Blueprint $table) {
                $table->foreign('mi_id')->references('id')->on('manufacturing_instructions')->onDelete('RESTRICT');
                $table->foreign('competive_id')->references('id')->on('competives')->onDelete('RESTRICT');
            });


                
                
            Schema::table('permission_role', function(Blueprint $table) {
                $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('CASCADE');
                $table->foreign('role_id')->references('id')->on('roles')->onDelete('CASCADE');
            });


                
                
                
            Schema::table('profiles', function(Blueprint $table) {
                $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            });


                
            Schema::table('programs', function(Blueprint $table) {
                $table->foreign('ProductionUnit_id')->references('id')->on('production_units')->onDelete('NO ACTION');
            });


                
            Schema::table('role_user', function(Blueprint $table) {
                $table->foreign('role_id')->references('id')->on('roles')->onDelete('CASCADE');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            });


                
                
                
                
            Schema::table('trade_rols', function(Blueprint $table) {
                $table->foreign('ProductionUnit_id')->references('id')->on('production_units')->onDelete('RESTRICT');
                $table->foreign('Program_id')->references('id')->on('programs')->onDelete('RESTRICT');
            });


                
            Schema::table('tradelrol_competive', function(Blueprint $table) {
                $table->foreign('competive_id')->references('id')->on('competives')->onDelete('RESTRICT');
                $table->foreign('traderol_id')->references('id')->on('trade_rols')->onDelete('RESTRICT');
            });


                
            Schema::table('tradingplan_headcount', function(Blueprint $table) {
                $table->foreign('trading_plan_id')->references('id')->on('training_plans')->onDelete('RESTRICT');
                $table->foreign('head_count_id')->references('id')->on('head_counts')->onDelete('RESTRICT');
            });


                
            Schema::table('training_plans', function(Blueprint $table) {
                $table->foreign('Training_id')->references('id')->on('trainings')->onDelete('RESTRICT');
            });


                
            Schema::table('training_requests', function(Blueprint $table) {
                $table->foreign('Training_id')->references('id')->on('trainings')->onDelete('RESTRICT');
            });


                
                
            Schema::table('trainings_competive', function(Blueprint $table) {
                $table->foreign('trainings_id')->references('id')->on('trainings')->onDelete('CASCADE');
                $table->foreign('competive_id')->references('id')->on('competives')->onDelete('CASCADE');
            });


                
            Schema::table('trainings_instructors', function(Blueprint $table) {
                $table->foreign('trainings_id')->references('id')->on('trainings')->onDelete('CASCADE');
                $table->foreign('instructors_id')->references('id')->on('instructors')->onDelete('CASCADE');
            });


                
                
            Schema::table('work_stations', function(Blueprint $table) {
                $table->foreign('ProductionUnit_id')->references('id')->on('production_units')->onDelete('RESTRICT');
                $table->foreign('Program_id')->references('id')->on('programs')->onDelete('RESTRICT');
            });


                
            Schema::table('work_stations_competive', function(Blueprint $table) {
                $table->foreign('work_stations_id')->references('id')->on('work_stations')->onDelete('RESTRICT');
                $table->foreign('competive_id')->references('id')->on('competives')->onDelete('RESTRICT');
            });


         }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
         public function down()
         {
            
                
                
                
                
            Schema::table('competive_specs', function(Blueprint $table) {
                $table->dropForeign('competive_specs_competive_id_foreign');
                $table->dropForeign('competive_specs_spec_id_foreign');
            });


                
                
            Schema::table('file_trainigs', function(Blueprint $table) {
                $table->dropForeign('file_trainigs_Trainig_id_foreign');
            });


                
            Schema::table('head_counts', function(Blueprint $table) {
                $table->dropForeign('head_counts_Traderol_id_foreign');
            });


                
            Schema::table('instructors', function(Blueprint $table) {
                $table->dropForeign('instructors_company_instructor_id_foreign');
            });


                
            Schema::table('manufacturing_instructions', function(Blueprint $table) {
                $table->dropForeign('manufacturing_instructions_ProductionUnit_id_foreign');
                $table->dropForeign('manufacturing_instructions_Program_id_foreign');
                $table->dropForeign('manufacturing_instructions_WS_id_foreign');
            });


                
            Schema::table('material_trainigs', function(Blueprint $table) {
                $table->dropForeign('material_trainigs_Trainig_id_foreign');
            });


                
            Schema::table('mi_competive', function(Blueprint $table) {
                $table->dropForeign('mi_competive_mi_id_foreign');
                $table->dropForeign('mi_competive_competive_id_foreign');
            });


                
                
            Schema::table('permission_role', function(Blueprint $table) {
                $table->dropForeign('permission_role_permission_id_foreign');
                $table->dropForeign('permission_role_role_id_foreign');
            });


                
                
                
            Schema::table('profiles', function(Blueprint $table) {
                $table->dropForeign('profiles_user_id_foreign');
            });


                
            Schema::table('programs', function(Blueprint $table) {
                $table->dropForeign('programs_ProductionUnit_id_foreign');
            });


                
            Schema::table('role_user', function(Blueprint $table) {
                $table->dropForeign('role_user_role_id_foreign');
                $table->dropForeign('role_user_user_id_foreign');
            });


                
                
                
                
            Schema::table('trade_rols', function(Blueprint $table) {
                $table->dropForeign('trade_rols_ProductionUnit_id_foreign');
                $table->dropForeign('trade_rols_Program_id_foreign');
            });


                
            Schema::table('tradelrol_competive', function(Blueprint $table) {
                $table->dropForeign('tradelrol_competive_competive_id_foreign');
                $table->dropForeign('tradelrol_competive_traderol_id_foreign');
            });


                
            Schema::table('tradingplan_headcount', function(Blueprint $table) {
                $table->dropForeign('tradingplan_headcount_trading_plan_id_foreign');
                $table->dropForeign('tradingplan_headcount_head_count_id_foreign');
            });


                
            Schema::table('training_plans', function(Blueprint $table) {
                $table->dropForeign('training_plans_Training_id_foreign');
            });


                
            Schema::table('training_requests', function(Blueprint $table) {
                $table->dropForeign('training_requests_Training_id_foreign');
            });


                
                
            Schema::table('trainings_competive', function(Blueprint $table) {
                $table->dropForeign('trainings_competive_trainings_id_foreign');
                $table->dropForeign('trainings_competive_competive_id_foreign');
            });


                
            Schema::table('trainings_instructors', function(Blueprint $table) {
                $table->dropForeign('trainings_instructors_trainings_id_foreign');
                $table->dropForeign('trainings_instructors_instructors_id_foreign');
            });


                
                
            Schema::table('work_stations', function(Blueprint $table) {
                $table->dropForeign('work_stations_ProductionUnit_id_foreign');
                $table->dropForeign('work_stations_Program_id_foreign');
            });


                
            Schema::table('work_stations_competive', function(Blueprint $table) {
                $table->dropForeign('work_stations_competive_work_stations_id_foreign');
                $table->dropForeign('work_stations_competive_competive_id_foreign');
            });


            
            
              Schema::dropIfExists('activity_log');
              Schema::dropIfExists('cells');
              Schema::dropIfExists('company_instructors');
              Schema::dropIfExists('competive_specs');
              Schema::dropIfExists('competives');
              Schema::dropIfExists('file_trainigs');
              Schema::dropIfExists('head_counts');
              Schema::dropIfExists('instructors');
              Schema::dropIfExists('manufacturing_instructions');
              Schema::dropIfExists('material_trainigs');
              Schema::dropIfExists('mi_competive');
              Schema::dropIfExists('password_resets');
              Schema::dropIfExists('permission_role');
              Schema::dropIfExists('permissions');
              Schema::dropIfExists('production_units');
              Schema::dropIfExists('profiles');
              Schema::dropIfExists('programs');
              Schema::dropIfExists('role_user');
              Schema::dropIfExists('roles');
              Schema::dropIfExists('social_accounts');
              Schema::dropIfExists('specs');
              Schema::dropIfExists('trade_rols');
              Schema::dropIfExists('tradelrol_competive');
              Schema::dropIfExists('tradingplan_headcount');
              Schema::dropIfExists('training_plans');
              Schema::dropIfExists('training_requests');
              Schema::dropIfExists('trainings');
              Schema::dropIfExists('trainings_competive');
              Schema::dropIfExists('trainings_instructors');
              Schema::dropIfExists('users');
              Schema::dropIfExists('work_stations');
              Schema::dropIfExists('work_stations_competive');
         }

}
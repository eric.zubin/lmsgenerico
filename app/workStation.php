<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class workStation extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'work_stations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Program_id', 'Status','WorkStationID', 'Name', 'Description', 'NivelDeRiesgoOp', 'InitialDate', 'FinalDate','ProductionUnit_id','Certificaciones','TrainingNeeds','MegaProcess','OperativeLevelRequired','Active'];

    public function ProductionUnit()
    {
        return $this->hasOne('App\productionUnit', 'id','ProductionUnit_id');
    }

    public function Program()
    {
        return $this->hasOne('App\Program', 'id','Program_id');
    }

    public function Competives()
    {
        return $this->belongsToMany('App\Competive', 'work_stations_competive', 
        'work_stations_id', 'competive_id');
    }
    
    public function Mi()
    {
        return $this->hasMany('App\ManufacturingInstruction','WS_id','id');
    }

 
}

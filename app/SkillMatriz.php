<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SkillMatriz extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'skill_matrizs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['head_count_id', 'worstation_id', 'EXPERT','Nivel','numero','work','EXPER'];

    public function WorkStation()
    {
        return $this->hasOne('App\workStation', 'id','worstation_id');
    }

    public function HeadCount()
    {
        return $this->hasOne('App\headCount', 'id','head_count_id');
    }
    public function Evidencia()
    {
        return $this->hasMany('App\SkillMatrizEvidencia','skill_matrizs_id','id');
    }
    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class instructor extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'instructors';
    protected $hidden = array('pivot');

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Num', 'Name', 'Company','company_instructor_id','Status','pic'];


    public function companyInstructor()
    {
        return $this->hasOne('App\companyInstructor', 'id','company_instructor_id');
    }
    
}
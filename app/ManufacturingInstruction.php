<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ManufacturingInstruction extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'manufacturing_instructions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Code', 'Name', 'Description', 'Program_id', 'WS_id', 'NumePart', 'Revision', 'FechaInicio', 'FechaFin','ProductionUnit_id'];

    public function ProductionUnit()
    {
        return $this->hasOne('App\productionUnit', 'id','ProductionUnit_id');
    }
    public function Program()
    {
        return $this->hasOne('App\Program', 'id','Program_id');
    }

    public function WS()
    {
        return $this->hasOne('App\workStation', 'id','WS_id');
    }

    public function Competives()
    {
        return $this->belongsToMany('App\Spec', 'mi_competive', 
        'mi_id', 'competive_id');
    }
    
}

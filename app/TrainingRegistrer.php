<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingRegistrer extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training_tregistrer';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['status','FechaFin','FechaInicio','status','Training_id','ROI2','ROI1','ROI3','ROI4'];


    public function Training()
    {
        return $this->hasOne('App\TrainingPlan', 'id','Training_id');
    }




    
}
 
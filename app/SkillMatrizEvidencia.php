<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SkillMatrizEvidencia extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'skill_matrizs_evidencia';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [ 'NombreArchivo','Evidencia','skill_matrizs_id'];

    public function SkillMatriz()
    {
        return $this->hasOne('App\SkillMatriz', 'id','skill_matrizs_id');
    }


    
}

<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Role;
use File;
class RoleController extends Controller
{
    public function getIndex(){
        $roles =  Role::paginate(10);
       return view('role.index',compact('roles'));
    }

    public function create(){
        $permissions = Permission::all();
        $roles = json_decode(File::get(base_path('resources/laravel-admin/roles.json')));


        return view('role.create',compact('permissions','roles'));
    }

    public function save(Request $request){
        $this->validate($request,[
           'name' => 'required'
        ]);

        $cadena_devuelta = strtolower($request->name);

        $role = Role::firstOrCreate(['name' => $cadena_devuelta]);
        if($request->permissions != '' || $request->permissions != null){
            $role->permissions()->sync($request->permissions);
        }
        Session::flash('message','Role has been added');
        return back();
    }

    public function delete(Request $request){
        $role = Role::findOrfail($request->id);
        $role->delete();
        Session::flash('message','Role has been deleted');
        return back();
    }

    public function edit(Request $request){
        $permissions = Permission::all();
        $role = Role::findOrfail($request->id);
        $role_permissions = $role->permissions()->pluck('id')->toArray();

        $roles = json_decode(File::get(base_path('resources/laravel-admin/roles.json')));

        return  view('role.edit',compact('role','permissions','role_permissions','roles'));
    }

    public function update(Request $request){
        $this->validate($request,[
            'name' => 'required'
        ]);
        $role = Role::findOrfail($request->id);
        $cadena_devuelta = strtolower($request->name);

        $role->name = $cadena_devuelta;
        $role->save();
        if($request->permissions == null){
            $role->permissions()->detach();
        }else{
            $role->permissions()->sync($request->permissions);
        }

        Session::flash('message','Role has been updated');
        return redirect('role-management');
    }
}

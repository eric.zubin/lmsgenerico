<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\TrainingRequest;
use App\TrainingPlan;


use Illuminate\Http\Request;
use App\Training;
use App\Program;


class TrainingRequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('trainingrequest','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $trainingrequest = TrainingRequest::where('User', 'LIKE', "%$keyword%")
                ->orWhere('Training', 'LIKE', "%$keyword%")
                ->orWhere('DateToday', 'LIKE', "%$keyword%")
                ->orWhere('DateRequest', 'LIKE', "%$keyword%")
                ->orWhere('Employees', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $trainingrequest = TrainingRequest::paginate($perPage);
            }

            return view('training_request.training-request.index', compact('trainingrequest'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $training = Training::all();
        $area = Program::all();
        $trainingPlan = TrainingPlan::all();
  

        $model = str_slug('trainingrequest','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('training_request.training-request.create', compact('training','area','trainingPlan'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('trainingrequest','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
        
           
            
            TrainingRequest::create($requestData);
            return redirect('training_request/training-request')->with('flash_message', 'TrainingRequest added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('trainingrequest','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $trainingrequest = TrainingRequest::findOrFail($id);
            return view('training_request.training-request.show', compact('trainingrequest'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $training = Training::all();

        $area = Program::all();

        $model = str_slug('trainingrequest','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $trainingrequest = TrainingRequest::findOrFail($id);
            return view('training_request.training-request.edit', compact('trainingrequest','training','area'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('trainingrequest','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            

                /**    
            * CheckValidation DateToday DateValidation
            * CheckPlan DateValidation   DatePlan
            * CheckTraining DatePlan DateTraining
            * CheckEvaluation DateTraining DateEvaluation
            * CheckDocumented DateEvaluation DateDocumented
            * CheckArchived DateDocumented DateArchived
            * CheckClosed DateArchived DateClosed */
   


            $trainingrequest = TrainingRequest::findOrFail($id);

        
            //empty
            if(isset($request['CheckValidation']) ) {

                $request['CheckValidation'] = '1' ; 

                $to =Carbon::parse($trainingrequest->DateToday); 

                 $from = Carbon::createFromFormat('Y-m-d', $request['DateValidation']);


                $request['ValidationDays'] = $to->diffInDays($from);
                }

            if(isset($request['CheckPlan']) ) {
                $request['CheckPlan'] = '1' ;
                $to = Carbon::createFromFormat('Y-m-d', $trainingrequest->DateValidation );
                $from = Carbon::createFromFormat('Y-m-d', $request['DatePlan']);
                $request['PlanDays'] = $to->diffInDays($from);                

                }
         
            if(isset($request['CheckTraining']) ) {
                $request['CheckTraining'] = '1' ;
                $to = Carbon::createFromFormat('Y-m-d', $trainingrequest->DatePlan);
                $from = Carbon::createFromFormat('Y-m-d', $request['DateTraining']);
                $request['TrainingDays'] = $to->diffInDays($from);
                }

       
            if(isset($request['CheckEvaluation']) ) {

                $request['CheckEvaluation'] = '1' ;
                $to = Carbon::createFromFormat('Y-m-d', $trainingrequest->DateTraining);
                $from = Carbon::createFromFormat('Y-m-d',  $request['DateEvaluation']);
                $request['EvaluationDays'] = $to->diffInDays($from);
            }                                      
   
            if(isset($request['CheckDocumented']) ) {

                $request['CheckDocumented'] = '1' ;
                $to = Carbon::createFromFormat('Y-m-d', $trainingrequest->DateEvaluation);
                $from = Carbon::createFromFormat('Y-m-d', $request['DateDocumented']);
                $request['DocumentedDays'] = $to->diffInDays($from);
            }     
                                              
        
            if(isset($request['CheckArchived']) ) {
                $request['CheckArchived'] = '1' ;
                $to = Carbon::createFromFormat('Y-m-d', $trainingrequest->DateDocumented);
                $from = Carbon::createFromFormat('Y-m-d', $request['DateArchived']);
                $request['ArchivedDays'] = $to->diffInDays($from);
            } 
                                                            
        
            if(isset($request['CheckClosed']) ) {
                $request['CheckClosed'] = '1' ;
                $to = Carbon::createFromFormat('Y-m-d', $trainingrequest->DateArchived);
                $from =Carbon::createFromFormat('Y-m-d', $request['DateClosed']);
                $request['ClosesDays']= $to->diffInDays($from);
            }  


            $requestData = $request->all();
            $trainingrequest->update($requestData);

            return redirect('training_request/training-request')->with('flash_message', 'TrainingRequest updated!');
        }
         return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('trainingrequest','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            TrainingRequest::destroy($id);

            return redirect('training_request/training-request')->with('flash_message', 'TrainingRequest deleted!');
        }
        return response(view('403'), 403);

    }
    public function getEventos()
    {
        $start = $_GET['start'];
        $end = $_GET['end'];

        
        //https://laravel.com/docs/5.8/database
        $results = DB::select("select Code as title,FechaFin as end,FechaInicio as start from training_plans 
        WHERE FechaInicio >= CAST('".$start."' AS DATE)
        AND FechaFin <= CAST('".$end."' AS DATE)");

        return response()->json($results);
    }
    
}

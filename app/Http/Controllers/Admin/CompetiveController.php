<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Spec;
use App\Competive;
use App\Training;
use Illuminate\Http\Request;

class CompetiveController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('competive','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $competive = Competive::where('Name', 'LIKE', "%$keyword%")
                ->orWhere('Code', 'LIKE', "%$keyword%")
                ->orWhere('Type', 'LIKE', "%$keyword%")
                ->orWhere('Level', 'LIKE', "%$keyword%")
                ->orWhere('Specs', 'LIKE', "%$keyword%");
            } else {
                $competive = Competive::all();
            }

            return view('competive.competive.index', compact('competive'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $specs = Spec::all();
        $training = Training::all();

        $model = str_slug('competive','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('competive.competive.create', compact('specs','training'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('competive','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
           $competive= Competive::create($requestData);






            $competiveSave = Competive::find($competive->id);
            $competiveSave->Specs()->toggle($request->input("Specs"));


            return redirect('competive/competive')->with('flash_message', 'Competive added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('competive','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $competive = Competive::findOrFail($id);
            return view('competive.competive.show', compact('competive'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $specs = Spec::all();
        $training = Training::all();

        $model = str_slug('competive','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $competive = Competive::findOrFail($id);
            return view('competive.competive.edit', compact('competive','specs','training'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('competive','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $competive = Competive::findOrFail($id);
             $competive->update($requestData);
             $competive->Specs()->sync($request->input("Specs"));


             return redirect('competive/competive')->with('flash_message', 'Competive updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('competive','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            Competive::destroy($id);

            return redirect('competive/competive')->with('flash_message', 'Competive deleted!');
        }
        return response(view('403'), 403);

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Training;
use App\TrainingPlan;
use App\productionUnit;
use Illuminate\Http\Request;
use DB;
class TrainingPlanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('trainingplan','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $trainingplan = TrainingPlan::where('Code', 'LIKE', "%$keyword%")
                ->orWhere('FechaInicio', 'LIKE', "%$keyword%")
                ->orWhere('FechaFin', 'LIKE', "%$keyword%")
                ->orWhere('Training', 'LIKE', "%$keyword%")
                ->orWhere('CountPersonal', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $trainingplan = TrainingPlan::paginate($perPage);
            }

            return view('training_plan.training-plan.index', compact('trainingplan'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $training = Training::all();
        $productionUnits = productionUnit::all();

        $model = str_slug('trainingplan','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('training_plan.training-plan.create', compact('training','productionUnits'));
        }
        return response(view('403'), 403);
    }

    public function getEventos()
    {
        $start = $_GET['start'];
        $end = $_GET['end'];

        
        //https://laravel.com/docs/5.8/database
        $results = DB::select("select Code as title,FechaFin as end,FechaInicio as start from training_plans 
        WHERE FechaInicio >= CAST('".$start."' AS DATE)
        AND FechaFin <= CAST('".$end."' AS DATE)");

        return response()->json($results);
    }
    


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('trainingplan','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            TrainingPlan::create($requestData);
            return redirect('training_plan/training-plan')->with('flash_message', 'TrainingPlan added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('trainingplan','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $trainingplan = TrainingPlan::findOrFail($id);
            return view('training_plan.training-plan.show', compact('trainingplan'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $training = Training::all();
        $productionUnits = productionUnit::all();

        $model = str_slug('trainingplan','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $trainingplan = TrainingPlan::findOrFail($id);
            return view('training_plan.training-plan.edit', compact('trainingplan','training','productionUnits'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('trainingplan','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $trainingplan = TrainingPlan::findOrFail($id);
             $trainingplan->update($requestData);

             return redirect('training_plan/training-plan')->with('flash_message', 'TrainingPlan updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('trainingplan','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            TrainingPlan::destroy($id);

            return redirect('training_plan/training-plan')->with('flash_message', 'TrainingPlan deleted!');
        }
        return response(view('403'), 403);

    }
}

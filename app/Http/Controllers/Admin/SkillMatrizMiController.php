<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SkillMatriz;
use Illuminate\Http\Request;
use App\headCount;
use App\workStation;
use App\productionUnit;
use App\Program;
use DB;

use App\SkillMatrizEvidencia;

use Excel;
class SkillMatrizMiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index($id = null,Request $request)
    {
        $model = str_slug('skillmatriz','-');

    

        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
           // $keyword = $request->get('search');
            //$perPage = 25;

            if (!$id) {
                // set start
                $Programfist = Program::first();
                $id = $Programfist->id;
            }

            //$head_count = headCount::all();

                 
            $head_count = headCount::whereHas('TradeRol', function($q) use ($id) {
                $q->where('Program_id',$id);
            })->get();



            $head_count_id = headCount::whereHas('TradeRol', function($q) use ($id) {
                $q->where('Program_id',$id);
            })->select('id')->get();


            $workStation = workStation::where("Program_id",$id)->get();
            $workStationid = workStation::where("Program_id",$id)->select('id')->get();
            $skillMatriz = SkillMatriz::whereIn('worstation_id', $workStationid)->get();
            $productionUnits = productionUnit::all();
            $program = Program::find($id);

            
       
           


            $count = DB::table('skill_matrizs')
                 ->select('head_count_id', DB::raw('count(*) as total'))
                 ->whereIn('worstation_id', $workStationid)
                 ->where(function($q) {
                    $q->where('skill_matrizs.Nivel', '=', 1)
                      ->orWhere('skill_matrizs.Nivel', '=', 2)
                      ->orWhere('skill_matrizs.Nivel', '=', 3)
                      ->orWhere('skill_matrizs.Nivel', '=', 4)
                      ->orWhere('skill_matrizs.Nivel', '=', 5)
                      ->orWhere('skill_matrizs.Nivel', '=', 6);
                })  ->groupBy('head_count_id')
                ->get();


            $count2 = DB::table('skill_matrizs')
                 ->select('worstation_id', DB::raw('count(*) as total'))
                 ->whereIn('worstation_id', $workStationid)
                 ->where(function($q) {
                    $q->where('skill_matrizs.Nivel', '=', 1)
                      ->orWhere('skill_matrizs.Nivel', '=', 2)
                      ->orWhere('skill_matrizs.Nivel', '=', 3)
                      ->orWhere('skill_matrizs.Nivel', '=', 4)
                      ->orWhere('skill_matrizs.Nivel', '=', 5)
                      ->orWhere('skill_matrizs.Nivel', '=', 6);
                })->groupBy('worstation_id')->get();


                $head_count_idStrin = "";

                foreach ($head_count_id as $he) {
                 $head_count_idStrin  =  $head_count_idStrin . $he->id . ",";
                 }
 
 
             $head_count_idStrin2 =  substr($head_count_idStrin, 0, -1);
 
 
             if($head_count_idStrin2 != ""){
                $count3 = DB::select( DB::raw("select COUNT(table1.worstation_id) as total ,work_stations.id as worstation_id from work_stations
                LEFT JOIN  (SELECT * from skill_matrizs where (skill_matrizs.Nivel = 1 OR skill_matrizs.Nivel = 2 OR skill_matrizs.Nivel = 3 OR skill_matrizs.Nivel = 4 OR skill_matrizs.Nivel = 5 OR skill_matrizs.Nivel = 6) AND head_count_id in ( " . $head_count_idStrin2 . " ) ) as table1
               ON work_stations.id = table1.worstation_id 
                  group By (work_stations.id)"));
            }else{
                $count3 = null;
            }

                 
                 

        $contarW = DB::select( DB::raw("select COUNT(*) as resultado from( select COUNT(worstation_id)  as total from skill_matrizs GROUP BY worstation_id having total >= 3) as t
        "));
        $totalW = DB::select( DB::raw("select COUNT(*) as resultado from `work_stations`"));
        $contarH = DB::select( DB::raw("select COUNT(*) as resultado from(  select COUNT(head_count_id) as total from skill_matrizs   GROUP BY head_count_id having total >= 3) as t"));
        $contarT = DB::select( DB::raw("select COUNT(*) as resultado from `head_counts`"));



       return view('skill_vistaCompetenciesMi.index', compact('count3','head_count','workStation','skillMatriz','count','count2','productionUnits','program','contarW','totalW','contarH','contarT'));

        }
        
        return response(view('403'), 403);
    }

    public function Exel($id = null)
    {
        if (!$id) {
            // set start
            $Programfist = Program::first();
            $id = $Programfist->id;
        }

       // $head_count = headCount::all();

             
            $head_count = headCount::whereHas('TradeRol', function($q) use ($id) {
                $q->where('Program_id',$id);
            })->get();


        $workStation = workStation::where("Program_id",$id)->get();

        $workStationid = workStation::where("Program_id",$id)->select('id')->get();

        $skillMatriz = SkillMatriz::whereIn('worstation_id', $workStationid)->get();

        $productionUnits = productionUnit::all();
        
        $program = Program::find($id);

        $count = DB::table('skill_matrizs')
             ->select('head_count_id', DB::raw('count(*) as total'))
             ->whereIn('worstation_id', $workStationid)
             ->where(function($q) {
                $q->where('skill_matrizs.Nivel', '=', 1)
                  ->orWhere('skill_matrizs.Nivel', '=', 2)
                  ->orWhere('skill_matrizs.Nivel', '=', 3)
                  ->orWhere('skill_matrizs.Nivel', '=', 4)
                  ->orWhere('skill_matrizs.Nivel', '=', 5)
                  ->orWhere('skill_matrizs.Nivel', '=', 6);
            })  ->groupBy('head_count_id')
            ->get();


        $count2 = DB::table('skill_matrizs')
             ->select('worstation_id', DB::raw('count(*) as total'))
             ->whereIn('worstation_id', $workStationid)
             ->where(function($q) {
                $q->where('skill_matrizs.Nivel', '=', 1)
                  ->orWhere('skill_matrizs.Nivel', '=', 2)
                  ->orWhere('skill_matrizs.Nivel', '=', 3)
                  ->orWhere('skill_matrizs.Nivel', '=', 4)
                  ->orWhere('skill_matrizs.Nivel', '=', 5)
                  ->orWhere('skill_matrizs.Nivel', '=', 6);
            })->groupBy('worstation_id')->get();



    $contarW = DB::select( DB::raw("select COUNT(*) as resultado from( select COUNT(worstation_id)  as total from skill_matrizs GROUP BY worstation_id having total >= 3) as t
    "));
    $totalW = DB::select( DB::raw("select COUNT(*) as resultado from `work_stations`"));
    $contarH = DB::select( DB::raw("select COUNT(*) as resultado from(  select COUNT(head_count_id) as total from skill_matrizs   GROUP BY head_count_id having total >= 3) as t"));
    $contarT = DB::select( DB::raw("select COUNT(*) as resultado from `head_counts`"));



    $file = Excel::create('Datos', function($excel) use ($head_count,$workStation,$skillMatriz,$count,$count2,$productionUnits,$program,$contarW,$totalW,$contarH,$contarT) {

            $excel->sheet('SkillMatriz', function($sheet) use ($head_count,$workStation,$skillMatriz,$count,$count2,$productionUnits,$program,$contarW,$totalW,$contarH,$contarT) {
        
                $sheet->loadView('skill_vistaCompetenciesMi.exel')->with('head_count',$head_count)
                ->with('workStation',$workStation)->with('skillMatriz',$skillMatriz)->with('count',$count)->with('count2',$count2)->with('productionUnits',$productionUnits)->with('program',$program)->with('contarW',$contarW)->with('totalW',$totalW)->with('contarH',$contarH)->with('contarT',$contarT);

            });
 
        });

        $file->store('xls')->download();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('skillmatriz','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('skill_matriz.skill-matriz.create');
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        
        $model = str_slug('skillmatriz','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
         
            $head_count_id =$request->input('head_count_id');
            $worstation_id =$request->input('worstation_id');

            $skillmatriz = SkillMatriz::where('head_count_id', $head_count_id )->where( 'worstation_id', $worstation_id)->get()->count();

            if($skillmatriz == 0){
                $skillmatriz = new SkillMatriz;
                $skillmatriz->head_count_id = $head_count_id;
                $skillmatriz->worstation_id = $worstation_id;
                $skillmatriz->Nivel = $request->input('Nivel');
                
                switch ($request->input('Nivel')) {
                    case 0:
                        $skillmatriz->EXPERT = "R";
                        break;
                    case 1:
                        $skillmatriz->EXPERT = "1A";
                        break;
                    case 2:
                        $skillmatriz->EXPERT = "2A";
                        break;
                    case 3:
                        $skillmatriz->EXPERT = "3A";
                        break;
                    case 4:
                        $skillmatriz->EXPERT = "1AQ";
                        break;
                    case 5:
                        $skillmatriz->EXPERT = "2AQ";
                        break;
                    case 6:
                        $skillmatriz->EXPERT = "3AQ";
                    case 7:
                        $skillmatriz->EXPERT = "P";
                        break;
                    case 8:
                        $skillmatriz->EXPERT = "T";
                        break;
                    case 9:
                        $skillmatriz->EXPERT = "1I";
                        break;
                    case 10:
                        $skillmatriz->EXPERT = "2I";
                        break;
                    case 11:
                        $skillmatriz->EXPERT = "3I";
                        break;        
                    case 12:
                        $skillmatriz->EXPERT = "C";
                        break;                  
                }
                $skillmatriz->save();
                $evidencias = $request->file('Evidencias');

                if($request->hasFile('Evidencias'))
                {
                    
                    foreach ($evidencias as $file) {
                        $filename =  $file->store('Evidencia/' . $skillmatriz->id );
                        SkillMatrizEvidencia::create([
                            'skill_matrizs_id' => $skillmatriz->id,
                            'archivo' => $filename
                        ]);
                    }
                  
                }

            }else{
                $skillmatriz = SkillMatriz::where('head_count_id', $head_count_id )->where( 'worstation_id', $worstation_id)->get()->last();

                $skillmatriz->head_count_id = $head_count_id;
                $skillmatriz->worstation_id = $worstation_id;
                $skillmatriz->Nivel = $request->input('Nivel');
                switch ($request->input('Nivel')) {
                    case 0:
                    $skillmatriz->EXPERT = "R";
                    break;
                    case 1:
                        $skillmatriz->EXPERT = "1A";
                        break;
                    case 2:
                        $skillmatriz->EXPERT = "2A";
                        break;
                    case 3:
                        $skillmatriz->EXPERT = "3A";
                        break;
                    case 4:
                        $skillmatriz->EXPERT = "1AQ";
                        break;
                    case 5:
                        $skillmatriz->EXPERT = "2AQ";
                        break;
                    case 6:
                        $skillmatriz->EXPERT = "3AQ";
                        break;
                    case 7:
                        $skillmatriz->EXPERT = "P";
                        break;
                    case 8:
                        $skillmatriz->EXPERT = "T";
                        break;
                    case 9:
                        $skillmatriz->EXPERT = "1I";
                        break;
                    case 10:
                        $skillmatriz->EXPERT = "2I";
                        break;
                    case 11:
                        $skillmatriz->EXPERT = "3I";
                        break;        
                    case 12:
                        $skillmatriz->EXPERT = "C";
                        break;                  
                }

          
                $skillmatriz->save();


                $evidencias = $request->file('Evidencias');

                if($request->hasFile('Evidencias'))
                {
                    
                    foreach ($evidencias as $file) {
                        $filename =  $file->store('Evidencia/' . $skillmatriz->id );
                        SkillMatrizEvidencia::create([
                            'skill_matrizs_id' => $skillmatriz->id,
                            'Evidencia' => $filename
                        ]);
                    }
                }
                  


            }
        


            return redirect('skill_matriz/skill-matriz')->with('flash_message', 'SkillMatriz added!');
        }
        return response(view('403'), 403);
    }





}

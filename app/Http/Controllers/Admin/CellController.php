<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cell;
use Illuminate\Http\Request;

class CellController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('cell','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $cell = Cell::where('Code', 'LIKE', "%$keyword%")
                ->orWhere('Description', 'LIKE', "%$keyword%")
                ->orWhere('Program', 'LIKE', "%$keyword%")
                ->orWhere('ProductionUnit', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $cell = Cell::paginate($perPage);
            }

            return view('cell.cell.index', compact('cell'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('cell','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('cell.cell.create');
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('cell','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            Cell::create($requestData);
            return redirect('cell/cell')->with('flash_message', 'Cell added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('cell','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $cell = Cell::findOrFail($id);
            return view('cell.cell.show', compact('cell'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $model = str_slug('cell','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $cell = Cell::findOrFail($id);
            return view('cell.cell.edit', compact('cell'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('cell','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $cell = Cell::findOrFail($id);
             $cell->update($requestData);

             return redirect('cell/cell')->with('flash_message', 'Cell updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('cell','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            Cell::destroy($id);

            return redirect('cell/cell')->with('flash_message', 'Cell deleted!');
        }
        return response(view('403'), 403);

    }
}

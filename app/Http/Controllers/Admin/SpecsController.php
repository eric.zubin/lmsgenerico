<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Spec;
use Illuminate\Http\Request;

class SpecsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('specs','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $specs = Spec::where('Name', 'LIKE', "%$keyword%")
                ->orWhere('Code', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $specs = Spec::paginate($perPage);
            }

            return view('specs.specs.index', compact('specs'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('specs','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('specs.specs.create');
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('specs','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            Spec::create($requestData);
            return redirect('specs/specs')->with('flash_message', 'Spec added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('specs','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $spec = Spec::findOrFail($id);
            return view('specs.specs.show', compact('spec'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $model = str_slug('specs','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $spec = Spec::findOrFail($id);
            return view('specs.specs.edit', compact('spec'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('specs','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $spec = Spec::findOrFail($id);
             $spec->update($requestData);

             return redirect('specs/specs')->with('flash_message', 'Spec updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('specs','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            Spec::destroy($id);

            return redirect('specs/specs')->with('flash_message', 'Spec deleted!');
        }
        return response(view('403'), 403);

    }
}

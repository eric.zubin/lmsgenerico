<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SkillMatriz;
use Illuminate\Http\Request;
use App\headCount;
use App\workStation;
use App\SkillMatrizEvidencia;
use App\productionUnit;
use App\Program;
use DB;

class SkillMatrizVistaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index($idProduction = null,$id = null,Request $request)
    {
        $model = str_slug('skillmatriz','-');

    

        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
           // $keyword = $request->get('search');
            //$perPage = 25;

            if (!$idProduction && !$id) {
            
            $head_count = null;
            $workStation =null;
            $workStationid = null;
            $skillMatriz = null;
            $productionUnits = productionUnit::all();
            $program = null;
            $count =null;
            $count2 =null;
            $count3 =null;
            $idProduction = null;
            $poly = null;
            $poly3Certificados = null;


            }else if( $idProduction &&  $id){

                //$head_count = headCount::all();


                $idProduction = $idProduction;

        
                
                $head_count = headCount::whereHas('TradeRol', function($q) use ($id) {
                    $q->where('Program_id',$id);
                })->get();
    
    
                $head_count_id = headCount::whereHas('TradeRol', function($q) use ($id) {
                    $q->where('Program_id',$id);
                })->select('id')->get();


                $poly = DB::select( DB::raw("
                select SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                 WHEN M > 0 THEN M 
                 ELSE 0 END) as M,Programs_Id from ResultadosTotalesPolyPoly where Programs_Id=". $id ." GROUP BY  Programs_Id"));

                 $poly3Certificados = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,Programs_id from skillMatrizTabla where Programs_id = ". $id ." GROUP BY  Programs_id"));



                $workStation = workStation::where("Program_id",$id)->get();
                $workStationid = workStation::where("Program_id",$id)->select('id')->get();
                $skillMatriz = SkillMatriz::whereIn('worstation_id', $workStationid)->get();
                $productionUnits = productionUnit::all();
                $idProduction = productionUnit::find($idProduction);

                $program = Program::find($id);
    
    
                $count = DB::table('skill_matrizs')
                     ->select('head_count_id', DB::raw('count(*) as total'))
                     ->whereIn('worstation_id', $workStationid)
                     ->where(function($q) {
                        $q->where('skill_matrizs.Nivel', '=', 1)
                          ->orWhere('skill_matrizs.Nivel', '=', 2)
                          ->orWhere('skill_matrizs.Nivel', '=', 3)
                          ->orWhere('skill_matrizs.Nivel', '=', 4)
                          ->orWhere('skill_matrizs.Nivel', '=', 5)
                          ->orWhere('skill_matrizs.Nivel', '=', 6);
                    })  ->groupBy('head_count_id')
                    ->get();
    
    
                $count2 = DB::table('skill_matrizs')
                     ->select('worstation_id', DB::raw('count(*) as total'))
                     ->whereIn('worstation_id', $workStationid)
                     ->where(function($q) {
                        $q->where('skill_matrizs.Nivel', '=', 1)
                          ->orWhere('skill_matrizs.Nivel', '=', 2)
                          ->orWhere('skill_matrizs.Nivel', '=', 3)
                          ->orWhere('skill_matrizs.Nivel', '=', 4)
                          ->orWhere('skill_matrizs.Nivel', '=', 5)
                          ->orWhere('skill_matrizs.Nivel', '=', 6);
                    })->groupBy('worstation_id')->get();
    

                    $head_count_idStrin = "";

                    foreach ($head_count_id as $he) {
                     $head_count_idStrin  =  $head_count_idStrin . $he->id . ",";
                     }
     
     
                 $head_count_idStrin2 =  substr($head_count_idStrin, 0, -1);
     
     
                 if($head_count_idStrin2 != ""){
                    $count3 = DB::select( DB::raw("select COUNT(table1.worstation_id) as total ,work_stations.id as worstation_id from work_stations
                    LEFT JOIN  (SELECT * from skill_matrizs where (skill_matrizs.Nivel = 1 OR skill_matrizs.Nivel = 2 OR skill_matrizs.Nivel = 3 OR skill_matrizs.Nivel = 4 OR skill_matrizs.Nivel = 5 OR skill_matrizs.Nivel = 6) AND head_count_id in ( " . $head_count_idStrin2 . " ) ) as table1
                   ON work_stations.id = table1.worstation_id 
                      group By (work_stations.id)"));
                }else{
                    $count3 = null;
                }
                  
    
            }else if($idProduction &&  !$id){
                $poly = null;
                $poly3Certificados = null;
               
        
                
                $head_count = headCount::whereHas('TradeRol', function($q) use ($idProduction) {
                    $q->where('ProductionUnit_id',$idProduction);
                })->get();
    


                $head_count_id = headCount::whereHas('TradeRol', function($q) use ($idProduction) {
                    $q->where('ProductionUnit_id',$idProduction);
                })->select('id')->get();

                $ProductionUnit_id = Program::where('ProductionUnit_id', $idProduction)->select('id')->get();
                
                $workStation = workStation::whereIn("Program_id",$ProductionUnit_id)->get();



                $workStationid = workStation::whereIn("Program_id",$ProductionUnit_id)->select('id')->get();
                $skillMatriz = SkillMatriz::whereIn('worstation_id', $workStationid)->get();
                $productionUnits = productionUnit::all();
                
                $idProduction = productionUnit::find($idProduction);


                $program = null;

    
                $count = DB::table('skill_matrizs')
                     ->select('head_count_id', DB::raw('count(*) as total'))
                     ->whereIn('worstation_id', $workStationid)
                     ->where(function($q) {
                        $q->where('skill_matrizs.Nivel', '=', 1)
                          ->orWhere('skill_matrizs.Nivel', '=', 2)
                          ->orWhere('skill_matrizs.Nivel', '=', 3)
                          ->orWhere('skill_matrizs.Nivel', '=', 4)
                          ->orWhere('skill_matrizs.Nivel', '=', 5)
                          ->orWhere('skill_matrizs.Nivel', '=', 6);
                    })  ->groupBy('head_count_id')
                    ->get();
    
    
                $count2 = DB::table('skill_matrizs')
                     ->select('worstation_id', DB::raw('count(*) as total'))
                     ->whereIn('worstation_id', $workStationid)
                     ->where(function($q) {
                        $q->where('skill_matrizs.Nivel', '=', 1)
                          ->orWhere('skill_matrizs.Nivel', '=', 2)
                          ->orWhere('skill_matrizs.Nivel', '=', 3)
                          ->orWhere('skill_matrizs.Nivel', '=', 4)
                          ->orWhere('skill_matrizs.Nivel', '=', 5)
                          ->orWhere('skill_matrizs.Nivel', '=', 6);
                    })->groupBy('worstation_id')->get();
    
    
                    $head_count_idStrin = "";

                    foreach ($head_count_id as $he) {
                     $head_count_idStrin  =  $head_count_idStrin . $he->id . ",";
                     }
     
     
                 $head_count_idStrin2 =  substr($head_count_idStrin, 0, -1);
     
     
                 if($head_count_idStrin2 != ""){
                    $count3 = DB::select( DB::raw("select COUNT(table1.worstation_id) as total ,work_stations.id as worstation_id from work_stations
                    LEFT JOIN  (SELECT * from skill_matrizs where (skill_matrizs.Nivel = 1 OR skill_matrizs.Nivel = 2 OR skill_matrizs.Nivel = 3 OR skill_matrizs.Nivel = 4 OR skill_matrizs.Nivel = 5 OR skill_matrizs.Nivel = 6) AND head_count_id in ( " . $head_count_idStrin2 . " ) ) as table1
                   ON work_stations.id = table1.worstation_id 
                      group By (work_stations.id)"));
                }else{
                    $count3 = null;
                }
                  
                     
    

            }


       return view('skill_vista.index', compact('count3','poly3Certificados','poly','idProduction','head_count','workStation','skillMatriz','count','count2','productionUnits','program'));

        }
        
        return response(view('403'), 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('skillmatriz','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('skill_vista.create');
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        
        $model = str_slug('skillmatriz','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
         
            $head_count_id =$request->input('head_count_id');
            $worstation_id =$request->input('worstation_id');

            $skillmatriz = SkillMatriz::where('head_count_id', $head_count_id )->where( 'worstation_id', $worstation_id)->get()->count();

            if($skillmatriz == 0){
                $skillmatriz = new SkillMatriz;
                $skillmatriz->head_count_id = $head_count_id;
                $skillmatriz->worstation_id = $worstation_id;
                $skillmatriz->Nivel = $request->input('Nivel');
                switch ($request->input('Nivel')) {
                    case 0:
                        $skillmatriz->EXPERT = "Requerido";
                        break;
                    case 1:
                        $skillmatriz->EXPERT = "Calificado con supervision";
                        break;
                    case 2:
                        $skillmatriz->EXPERT = "Calificado sin supervision";
                        break;
                    case 3:
                        $skillmatriz->EXPERT = "Calificado entrenado";
                        break;
                    case 4:
                        $skillmatriz->EXPERT = "Planeado a ser calificado";
                        break;
                    case 5:
                        $skillmatriz->EXPERT = "En proceso de entrenamiento";
                        break;
                    case 6:
                        $skillmatriz->EXPERT = "Calificado entrenado / Inactivo";
                        break;
                    case 7:
                        $skillmatriz->EXPERT = "Calificado sin entrenado / Inactivo";
                        break;
                    case 8:
                        $skillmatriz->EXPERT = "Calificado sin entrenador / Inactivo";
                        break;        
                    case 9:
                        $skillmatriz->EXPERT = "Calificado  cancalda";
                        break;                  
                }
                $skillmatriz->save();
            }else{
                $skillmatriz = SkillMatriz::where('head_count_id', $head_count_id )->where( 'worstation_id', $worstation_id)->get()->last();

                $skillmatriz->head_count_id = $head_count_id;
                $skillmatriz->worstation_id = $worstation_id;
                $skillmatriz->Nivel = $request->input('Nivel');
                switch ($request->input('Nivel')) {
                    case 0:
                        $skillmatriz->EXPERT = "Requerido";
                        break;
                    case 1:
                        $skillmatriz->EXPERT = "Calificado con supervision";
                        break;
                    case 2:
                        $skillmatriz->EXPERT = "Calificado sin supervision";
                        break;
                    case 3:
                        $skillmatriz->EXPERT = "Calificado entrenado";
                        break;
                    case 4:
                        $skillmatriz->EXPERT = "Planeado a ser calificado";
                        break;
                    case 5:
                        $skillmatriz->EXPERT = "En proceso de entrenamiento";
                        break;
                    case 6:
                        $skillmatriz->EXPERT = "Calificado entrenado / Inactivo";
                        break;
                    case 7:
                        $skillmatriz->EXPERT = "Calificado sin entrenado / Inactivo";
                        break;
                    case 8:
                        $skillmatriz->EXPERT = "Calificado sin entrenador / Inactivo";
                        break;        
                    case 9:
                        $skillmatriz->EXPERT = "Calificado  cancalda";
                        break;                  
                }

          
                $skillmatriz->save();

            }
        


            return redirect('skill_vista')->with('flash_message', 'SkillMatriz added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function ver($head_count_id,$worstation_id)
    {
        $skillmatriz = SkillMatriz::where('head_count_id', $head_count_id )->where( 'worstation_id', $worstation_id)->get()->last();
        return response()->json($skillmatriz);
    }
    public function evicencia($id)
    {
        $evidencias = SkillMatrizEvidencia::where("skill_matrizs_id",$id)->get();
        
        return response()->json($evidencias);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $model = str_slug('skillmatriz','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $skillmatriz = SkillMatriz::findOrFail($id);
            return view('skill_matriz.skill-matriz.edit', compact('skillmatriz'));
        }
        return response(view('403'), 403);
    }

    public function decargar($id)
    {
        $archivo = SkillMatrizEvidencia::find($id);
        return response()->download(storage_path('app/' . $archivo->Evidencia));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('skillmatriz','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $skillmatriz = SkillMatriz::findOrFail($id);
             $skillmatriz->update($requestData);

             return redirect('skill_matriz/skill-matriz')->with('flash_message', 'SkillMatriz updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('skillmatriz','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            SkillMatriz::destroy($id);

            return redirect('skill_matriz/skill-matriz')->with('flash_message', 'SkillMatriz deleted!');
        }
        return response(view('403'), 403);

    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\companyInstructor;
use Illuminate\Http\Request;

class companyInstructorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('companyinstructor','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $companyinstructor = companyInstructor::where('Code', 'LIKE', "%$keyword%")
                ->orWhere('Name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $companyinstructor = companyInstructor::paginate($perPage);
            }

            return view('company_instructor.company-instructor.index', compact('companyinstructor'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('companyinstructor','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('company_instructor.company-instructor.create');
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('companyinstructor','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            companyInstructor::create($requestData);
            return redirect('company_instructor/company-instructor')->with('flash_message', 'companyInstructor added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('companyinstructor','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $companyinstructor = companyInstructor::findOrFail($id);
            return view('company_instructor.company-instructor.show', compact('companyinstructor'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $model = str_slug('companyinstructor','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $companyinstructor = companyInstructor::findOrFail($id);
            return view('company_instructor.company-instructor.edit', compact('companyinstructor'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('companyinstructor','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $companyinstructor = companyInstructor::findOrFail($id);
             $companyinstructor->update($requestData);

             return redirect('company_instructor/company-instructor')->with('flash_message', 'companyInstructor updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('companyinstructor','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            companyInstructor::destroy($id);

            return redirect('company_instructor/company-instructor')->with('flash_message', 'companyInstructor deleted!');
        }
        return response(view('403'), 403);

    }
}

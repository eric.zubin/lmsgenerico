<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Training;
use App\TrainingPlan;
use App\TrainingRegistrer;
use App\TrainingPlanHeadCount;
use App\headCount;

use Illuminate\Http\Request;
use DB;

use Session;

class TradingRegistrerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $model = str_slug('trainingreigstrer','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $trainingplan = TrainingRegistrer::all();
            } else {
                $trainingplan = TrainingRegistrer::all();
            }

            return view('training_registrer.index', compact('trainingplan'));
        }
        return response(view('403'), 403);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */

    public function createTraidingCreate()
    {
        $TrainingPlan = TrainingPlan::all();

        
      return view('training_registrer.create_new', compact('TrainingPlan'));

        //return response(view('403'), 403);
    }


    public function store(Request $request)
    {
     
            $requestData = $request->all();
            
            TrainingRegistrer::create($requestData);
            return redirect('training_reigstrer')->with('flash_message', 'TrainingPlan added!');
    
    }


    public function create($id)
    {
        $training = TrainingRegistrer::find($id);
        $partes = TrainingPlanHeadCount::where("trading_plan_id",$id)->get();
        return view('training_registrer.create_index', compact('training','partes'));
        
        //return response(view('403'), 403);
    }
    public function createUser($id)
    {

        $partes = TrainingPlanHeadCount::where("trading_plan_id",$id)->select('head_count_id')->get();

        $training = TrainingRegistrer::find($id);

        $headCount = headCount::whereNotIn('id', $partes)->get();

        
        return view('training_registrer.create', compact('training','headCount'));
        
        //return response(view('403'), 403);
    }

    public function edit($id)
    {
        $trainingPlanHeadCount = TrainingPlanHeadCount::find($id)->first();
        $trainingRegistrer = TrainingRegistrer::find($trainingPlanHeadCount->trading_plan_id);
        $training = TrainingPlan::find($trainingRegistrer->Training_id);
        $partes = TrainingPlanHeadCount::where("trading_plan_id",$trainingRegistrer->Training_id)->select('head_count_id')->get();

        $headCountP= headCount::find($trainingPlanHeadCount->head_count_id); 
        //$headCount = headCount::whereNotIn('id', $partes)->get();
        $headCount = [];

        return view('training_registrer.edit', compact('trainingPlanHeadCount','training','headCount','headCountP','trainingRegistrer'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeHeadcount(Request $request)
    {
        $model = str_slug('trainingreigstrer','-');
        
            $requestData = $request->all();
            
            TrainingPlanHeadCount::create($requestData);

            Session::flash('message','Usuario Guardado');


            return back()->withInput();
            
        
    }




    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateHeadcount($id,Request $request)
    {
            $requestData = $request->all();
            $trainingplan = TrainingRegistrer::findOrFail($id);
            $trainingplan->update($requestData);
            
            
            return back()->withInput();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('trainingreigstrer','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            TrainingRegistrer::destroy($id);

            return redirect('training_plan/training-plan')->with('flash_message', 'TrainingPlan deleted!');
        }
        return response(view('403'), 403);

    }
}

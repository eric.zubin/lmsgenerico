<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\instructor;
use App\Training;
use App\MaterialTrainig;
use App\FileTrainig;
use App\Competive;
use App\companyInstructor;
use Illuminate\Http\Request;

class TrainingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 10;

            if (!empty($keyword)) {
                $training = Training::where('Code', 'LIKE', "%$keyword%")
                ->orWhere('Name', 'LIKE', "%$keyword%")
                ->orWhere('Type', 'LIKE', "%$keyword%")
                ->orWhere('Instructor', 'LIKE', "%$keyword%")
                ->orWhere('Level', 'LIKE', "%$keyword%")
                ->orWhere('Language', 'LIKE', "%$keyword%")
                ->orWhere('Service', 'LIKE', "%$keyword%")
                ->orWhere('TrainingSolution', 'LIKE', "%$keyword%")
                ->orWhere('TrainingSolution', 'LIKE', "%$keyword%")
                ->orWhere('Version', 'LIKE', "%$keyword%")
                ->orWhere('instructors', 'LIKE', "%$keyword%");

            } else {
                $training = Training::all();
            }

            return view('training.training.index', compact('training'));
        }
        return response(view('403'), 403);

    }
    
/* 
Es el metodo para buscar los instructores y las companyes que tiene un Training
*/
    public function instructores($id,Request $request)
    {

        $training= Training::with(array('instructors'=>function($query){
            $query->with('companyInstructor');
        }))->where('id','=',$id)->get();

        
        return response()->json($training[0]->instructors);

        
                 
    }
    

    public function decargar($id)
    {
        $archivo = MaterialTrainig::find($id);

        return response()->download(storage_path('app/' . $archivo->Archivo));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $instructor = instructor::all();
        $competives = Competive::all();
        $companyInstructors = companyInstructor::all();


        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('training.training.create', compact('instructor','competives','companyInstructors'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
           
            $trading = Training::create($requestData);
            $tradingSave = Training::find($trading->id);
            $tradingSave->instructors()->toggle($request->input("Instructors"));
            $tradingSave->Competives()->toggle($request->input("Competives"));
            $tradingSave->Companies()->toggle($request->input("Companies"));

            
            $materiales = $request->file('Materiales');

            if($request->hasFile('Materiales'))
            {
                foreach ($materiales as $file) {
                    $filename =  $file->store('Materiales/' . $trading->id );
                    MaterialTrainig::create([
                        'Trainig_id' => $trading->id,
                        'Archivo' => $filename
                    ]);
                }
              
            }


          
            return redirect('training/training')->with('flash_message', 'Training added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $instructor = instructor::all();

        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $training = Training::findOrFail($id);
            return view('training.training.show', compact('training','instructor'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $instructor = instructor::all();
        $competives = Competive::all();
        $companyInstructors = companyInstructor::all();

        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $training = Training::findOrFail($id);
            return view('training.training.edit', compact('training','instructor','competives','companyInstructors'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $training = Training::findOrFail($id);
            $training->update($requestData);

            $training->instructors()->sync($request->input("Instructors"));
            $training->Competives()->sync($request->input("Competives"));
            $training->Companies()->sync($request->input("Companies"));

            if($request->hasFile('Materiales'))
            {
                foreach ($materiales as $file) {
                    $filename =  $file->store('Materiales/' . $trading->id );
                    MaterialTrainig::create([
                        'Trainig_id' => $trading->id,
                        'Archivo' => $filename
                    ]);
                }
              
            }

             return redirect('training/training')->with('flash_message', 'Training updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            Training::destroy($id);

            return redirect('training/training')->with('flash_message', 'Training deleted!');
        }
        return response(view('403'), 403);

    }
}

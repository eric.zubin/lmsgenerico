<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\productionUnit;
use App\workStation;
use App\Competive;
use Illuminate\Http\Request;

class workStationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('workstation','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $workstation = workStation::where('ProductionUnit', 'LIKE', "%$keyword%")
                ->orWhere('Program', 'LIKE', "%$keyword%")
                ->orWhere('WorkStationID', 'LIKE', "%$keyword%")
                ->orWhere('Name', 'LIKE', "%$keyword%")
                ->orWhere('Description', 'LIKE', "%$keyword%")
                ->orWhere('NivelDeRiesgoOp', 'LIKE', "%$keyword%")
                ->orWhere('InitialDate', 'LIKE', "%$keyword%")
                ->orWhere('FinalDate', 'LIKE', "%$keyword%");
            } else {
                $workstation = workStation::all();
            }

            return view('work_station.work-station.index', compact('workstation'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $productionUnits = productionUnit::all();
        $competives = Competive::all();

        $model = str_slug('workstation','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('work_station.work-station.create', compact('productionUnits','competives'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('workstation','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            

            if(isset($request['Status']) ) {
            $request['Status'] = '0' ;
            }
   
           

            $requestData = $request->all();
            
            $workStation = workStation::create($requestData);

            $workStation->Competives()->toggle($request->input("Competives"));

            return redirect('work_station/work-station')->with('flash_message', 'workStation added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {

        $model = str_slug('workstation','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $workstation = workStation::findOrFail($id);
            return view('work_station.work-station.show', compact('workstation'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $productionUnits = productionUnit::all();
        $competives = Competive::all();

        $model = str_slug('workstation','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $workstation = workStation::findOrFail($id);
            return view('work_station.work-station.edit', compact('workstation','productionUnits','competives'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('workstation','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
        }
            if(isset($request['Status']) ) {
                $request['Status'] = '0' ;
                }
                
                if(empty($request['Status']) ) {
                    $request['Status'] = '1' ;
                    }
                
            $requestData = $request->all();
            
            $workstation = workStation::findOrFail($id);
             $workstation->update($requestData);
             $workstation->Competives()->sync($request->input("Competives"));

             return redirect('work_station/work-station')->with('flash_message', 'workStation updated!');
       
       // return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('workstation','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            workStation::destroy($id);

            return redirect('work_station/work-station')->with('flash_message', 'workStation deleted!');
        }
        return response(view('403'), 403);

    }

      /**
     * select_ws the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JSON
     */
    public function select_ws($ProductionUnit_id,$Program_id)
    {
    
        $program = workStation::where('ProductionUnit_id', '=', $ProductionUnit_id)->where('Program_id', '=', $Program_id)->get();


        $html ="";
        foreach($program as $key => $value)
        {

        $html.= "<option value='".$value->id ."'>".$value->Name ."</option>";

        }

        echo $html;
    }

}

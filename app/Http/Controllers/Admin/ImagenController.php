<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TrainingPlan;
use App\Imagen;
use Illuminate\Http\Request;
use Storage;
use File;
use Response;
use DB;
class ImagenController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('imagen','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $imagen = Imagen::where('imagen', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $imagen = Imagen::paginate($perPage);
            }

            return view('imagen.imagen.index', compact('imagen'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = str_slug('imagen','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('imagen.imagen.create');
        }
        return response(view('403'), 403);

    }

    public function dashboard()
    {
        //Poner El dashboard
        /*
        TODO:

CREATE VIEW totalRealCursos AS select (COUNT(training_plans.id )*COSTOPOR_PER) as Total from training_plans,tradingplan_headcount where trading_plan_id  = training_plans.id AND year(FechaInicio) = Year(NOW())  AND  year(FechaFin) = Year(NOW()) group by training_plans.id;


select SUM(Total) from totalRealCursos





select CASE WHEN (COUNT(worstation_id) >=  TrainingNeeds) THEN  TrainingNeeds
            ELSE  COUNT(worstation_id)
    END totales,
COUNT(worstation_id)  as total,TrainingNeeds,WorkStationID,Program_id from skill_matrizs,work_stations where work_stations.id = worstation_id  AND ProductionUnit_id = 1 GROUP BY worstation_id 





select CASE WHEN (COUNT(worstation_id) >=  TrainingNeeds) THEN  TrainingNeeds
            ELSE  COUNT(worstation_id)
    END totales,
COUNT(worstation_id)  as total,TrainingNeeds,WorkStationID,Program_id from skill_matrizs,work_stations where work_stations.id = worstation_id  AND ProductionUnit_id = 2 GROUP BY worstation_id 





SELECT CASE WHEN (COSTOPOR_PER != 1) THEN 'SUM'
            ELSE 'Hello'
       END Name
FROM training_plans



PolyPoly
_________________________________
CREATE VIEW PolyPolyOne AS select CASE WHEN (COUNT(worstation_id) >=  TrainingNeeds) THEN  TrainingNeeds
            ELSE  COUNT(worstation_id)
    END totales,
COUNT(worstation_id)  as total,TrainingNeeds,WorkStationID,Program_id from skill_matrizs,work_stations where work_stations.id = worstation_id  AND ProductionUnit_id = 1 GROUP BY worstation_id 


CREATE VIEW PolyPolyTwo AS select CASE WHEN (COUNT(worstation_id) >=  TrainingNeeds) THEN  TrainingNeeds
            ELSE  COUNT(worstation_id)
    END totales,
COUNT(worstation_id)  as total,TrainingNeeds,WorkStationID,Program_id from skill_matrizs,work_stations where work_stations.id = worstation_id  AND ProductionUnit_id = 2 GROUP BY worstation_id 



CREATE VIEW PolyPolyTotal AS select CASE WHEN (COUNT(worstation_id) >=  TrainingNeeds) THEN  TrainingNeeds
            ELSE  COUNT(worstation_id)
    END totales,
COUNT(worstation_id)  as total,TrainingNeeds,WorkStationID,Program_id from skill_matrizs,work_stations where work_stations.id = worstation_id   GROUP BY worstation_id 



_________________________________

TRES POR TRES

CREATE VIEW PolyPolyOneTresTres AS select COUNT(worstation_id)  as total,WorkStationID,Program_id from skill_matrizs,work_stations where work_stations.id = worstation_id  AND ProductionUnit_id = 1 GROUP BY worstation_id having 
COUNT(worstation_id) >= 3


CREATE VIEW PolyPolyTwoTresTres AS select COUNT(worstation_id)  as total,WorkStationID,Program_id from skill_matrizs,work_stations where work_stations.id = worstation_id  AND ProductionUnit_id = 2 GROUP BY worstation_id having 
COUNT(worstation_id) >= 3



CREATE VIEW PolyPolyTodosTresTres AS select COUNT(worstation_id)  as total,WorkStationID,Program_id from skill_matrizs,work_stations where work_stations.id = worstation_id  GROUP BY worstation_id having 
COUNT(worstation_id) >= 3


CREATE VIEW Cantidad_work_stations as select COUNT(Program_id) as total,Program_id from work_stations GROUP BY Program_id


select COUNT(PolyPolyOneTresTres.Program_id) as count,PolyPolyOneTresTres.Program_id,programs.code,Cantidad_work_stations.total from PolyPolyOneTresTres,programs,Cantidad_work_stations where programs.id=PolyPolyOneTresTres.Program_id AND Cantidad_work_stations.Program_id=programs.id GROUP BY PolyPolyOneTresTres.Program_id 


Select COUNT(PolyPolyTwoTresTres.Program_id) as count,PolyPolyTwoTresTres.Program_id,programs.code,Cantidad_work_stations.total from PolyPolyTwoTresTres,programs,Cantidad_work_stations where programs.id=PolyPolyTwoTresTres.Program_id AND Cantidad_work_stations.Program_id=programs.id GROUP BY PolyPolyTwoTresTres.Program_id 


Select COUNT(PolyPolyTodosTresTres.Program_id) as count,PolyPolyTodosTresTres.Program_id,programs.code,Cantidad_work_stations.total from PolyPolyTodosTresTres,programs,Cantidad_work_stations where programs.id=PolyPolyTodosTresTres.Program_id AND Cantidad_work_stations.Program_id=programs.id GROUP BY PolyPolyTodosTresTres.Program_id 






CREATE VIEW ViewPolyPolyOneTresTres AS select COUNT(PolyPolyOneTresTres.Program_id) as count,PolyPolyOneTresTres.Program_id,programs.code,Cantidad_work_stations.total  from PolyPolyOneTresTres,programs,Cantidad_work_stations where programs.id=PolyPolyOneTresTres.Program_id AND Cantidad_work_stations.Program_id=programs.id GROUP BY PolyPolyOneTresTres.Program_id 

CREATE VIEW ViewPolyPolyTwoTresTres AS Select COUNT(PolyPolyTwoTresTres.Program_id) as count,PolyPolyTwoTresTres.Program_id,programs.code,Cantidad_work_stations.total from PolyPolyTwoTresTres,programs,Cantidad_work_stations where programs.id=PolyPolyTwoTresTres.Program_id AND Cantidad_work_stations.Program_id=programs.id GROUP BY PolyPolyTwoTresTres.Program_id 

CREATE VIEW ViewPolyPolyTOTALTresTres AS Select COUNT(PolyPolyTodosTresTres.Program_id) as count,PolyPolyTodosTresTres.Program_id,programs.code,Cantidad_work_stations.total from PolyPolyTodosTresTres,programs,Cantidad_work_stations where programs.id=PolyPolyTodosTresTres.Program_id AND Cantidad_work_stations.Program_id=programs.id GROUP BY PolyPolyTodosTresTres.Program_id 





        */
        $imagen = Imagen::all();

        //Se hace el conteo por genero 
        //Empleado H/M
        $generes = DB::select( DB::raw("select COUNT(Genere) as 'Genero',Genere from head_counts group by Genere,FinalDate having FinalDate is null"));


          //Se el count total
        //Empleado H/M
        $headCount = DB::select( DB::raw("select COUNT(*) as total from head_counts  where FinalDate is null"));


        //Se hace el total para training_requests Training Requests
        $training_requests1 = DB::select( DB::raw("select COUNT(*) as 'total' from training_requests where CheckValidation = true AND CheckPlan = false and CheckTraining = false and CheckEvaluation = false and CheckDocumented = false and CheckArchived = false and  CheckClosed = false"));
        $training_requests2 = DB::select( DB::raw("select COUNT(*) as 'total' from training_requests where CheckValidation = true AND CheckPlan = true and CheckTraining = false and CheckEvaluation = false and CheckDocumented = false and CheckArchived = false and  CheckClosed = false"));
        $training_requests3 = DB::select( DB::raw("select COUNT(*) as 'total' from training_requests where CheckValidation = true AND CheckPlan = true and CheckTraining = true and CheckEvaluation = false and CheckDocumented = false and CheckArchived = false and  CheckClosed = false"));
        $training_requests4 = DB::select( DB::raw("select COUNT(*) as 'total' from training_requests where CheckValidation = true AND CheckPlan = true and CheckTraining = true and CheckEvaluation = true and CheckDocumented = false and CheckArchived = false and  CheckClosed = false"));
        $training_requests5 = DB::select( DB::raw("select COUNT(*) as 'total' from training_requests where CheckValidation = true AND CheckPlan = true and CheckTraining = true and CheckEvaluation = true and CheckDocumented = true and CheckArchived = false and  CheckClosed = false"));
        $training_requests6 = DB::select( DB::raw("select COUNT(*) as 'total' from training_requests where CheckValidation = true AND CheckPlan = true and CheckTraining = true and CheckEvaluation = true and CheckDocumented = true and CheckArchived = true and  CheckClosed = false"));
        $training_requests7 = DB::select( DB::raw("select COUNT(*) as 'total' from training_requests where CheckValidation = true AND CheckPlan = true and CheckTraining = true and CheckEvaluation = true and CheckDocumented = true and CheckArchived = true and  CheckClosed = true"));


        $resultado1 = DB::select( DB::raw("select SUM(NUM_EMPLOYEES*COSTOPOR_PER) as numero from training_plans where year(FechaInicio) = Year(NOW())  AND  year(FechaFin) = Year(NOW())"));
        $resultado2 = DB::select( DB::raw("select SUM(NUM_EMPLOYEES) as numero from training_plans where year(FechaInicio) = Year(NOW())  AND  year(FechaFin) = Year(NOW()) "));
        $resultado3 = DB::select( DB::raw("select SUM(Total) as numero from totalRealCursos"));
        $resultado4 = DB::select( DB::raw("select COUNT(training_plans.id) as numero from training_plans,tradingplan_headcount where trading_plan_id  = training_plans.id AND year(FechaInicio) = Year(NOW())  AND  year(FechaFin) = Year(NOW())"));

        $resultado5 = DB::select( DB::raw("Select COUNT(training_plans.id) as numero from training_plans,training_tregistrer where training_tregistrer.Training_id  = training_plans.id AND year(training_tregistrer.FechaInicio) = Year(NOW())  AND  year(training_tregistrer.FechaFin) = Year(NOW())
        and (Code = '' OR Code is null)"));
        $resultado6 = DB::select( DB::raw("select COUNT(training_plans.id) as numero from training_plans,training_tregistrer where training_tregistrer.Training_id  = training_plans.id AND year(training_tregistrer.FechaInicio) = Year(NOW())  AND  year(training_tregistrer.FechaFin) = Year(NOW())
        and (Code != '' OR Code is not null)"));
       
        
        $polypolyTotal= DB::select( DB::raw("select SUM(totales),SUM(TrainingNeeds),SUM(totales) *100 / SUM(TrainingNeeds) as porcentaje from PolyPolyTotal"));



        $polypolyOneTotalTresTresGrafica = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,Programs_id,programs.Code,ProductionUnit_id from skillMatrizTabla, programs where programs.id = Programs_id AND ProductionUnit_id=1 GROUP BY  Programs_id"));


        $polypolyTwoTotalTresTres = DB::select( DB::raw("select  SUM(count)  *100 / SUM(total) as porcentaje  from ViewPolyPolyTwoTresTres"));
        $polypolyTwoTotalTresTresGrafica = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,Programs_id,programs.Code,ProductionUnit_id from skillMatrizTabla, programs where programs.id = Programs_id AND ProductionUnit_id=2 GROUP BY  Programs_id"));


        $polyProductionUnit_id = DB::select( DB::raw("select 
        SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                                                   WHEN M > 0 THEN M
                                                   ELSE 0 END) as M,ProductionUnit_id
         from ResultadosTotalesPolyPoly,programs 
         where programs.id = ResultadosTotalesPolyPoly.Programs_Id
         GROUP BY  ProductionUnit_id"));

         $polyProductionUnGenral = DB::select( DB::raw("select 
         SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                                                    WHEN M > 0 THEN M
                                                    ELSE 0 END) as M
          from ResultadosTotalesPolyPoly"));

        $PolyWorkStUnit_id = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,ProductionUnit_id from skillMatrizTabla,programs where programs.id = skillMatrizTabla.Programs_id  GROUP BY  ProductionUnit_id"));
               
        $PolyWorkStatTotal = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly from skillMatrizTabla,programs where programs.id = skillMatrizTabla.Programs_id"));

        $PolyWorkStUnitPeople_id = DB::select( DB::raw("select  SUM(headcountTotal) as headcountTotal, SUM(totalP) as poly from PeopleCertifiedMinimumWorkStations  GROUP BY ProductionUnit_id"));
        $PolyWorkStatPeopleTotal = DB::select( DB::raw("select SUM(headcountTotal) as headcountTotal, SUM(totalP) as poly from PeopleCertifiedMinimumWorkStations"));


        $PolyWorkStatPeopleTotalOne= DB::select( DB::raw("select * from PeopleCertifiedMinimumWorkStations where ProductionUnit_id = 1"));
        $PolyWorkStatPeopleTotalTwo = DB::select( DB::raw("select * from PeopleCertifiedMinimumWorkStations where ProductionUnit_id = 2"));

        $polypolyOne = DB::select( DB::raw("select SUM(totales) ,Program_id ,Code,SUM(TrainingNeeds),SUM(totales) *100 / SUM(TrainingNeeds) as porcentaje  from PolyPolyOne,programs  where programs.id = Program_id  GROUP BY Program_id"));
        $polypolyTwo = DB::select( DB::raw("select SUM(totales) ,Program_id ,Code,SUM(TrainingNeeds),SUM(totales) *100 / SUM(TrainingNeeds) as porcentaje  from PolyPolyTwo,programs  where programs.id = Program_id  GROUP BY Program_id"));



        $porcentajetradingsincode = DB::select( DB::raw("select training_plans_year_sinCODE.numero as tradingplan,trainingexecutados_year_sinCODE.numero as trading_exucutades  from training_plans_year_sinCODE,trainingexecutados_year_sinCODE"));


        $porcentajetradingconcode = DB::select( DB::raw("select training_plans_year_conCODE.numero as tradingplan,trainingexecutados_year_conCODE.numero as trading_exucutades  from training_plans_year_conCODE,trainingexecutados_year_conCODE"));


    


       //Se pone 4 ramas sindicalizados no sindicalizados y Outsurcing
        //4 ramas de sindicalizados y no sindicalizados
        $NOOutsurcingNOSindicalizado = DB::select( DB::raw("select COUNT(Outsurcing) as 'TOutsurcing',Outsurcing,  COUNT(Sindicalizado) as 'TSindicalizado',Sindicalizado  from head_counts group by Outsurcing,Sindicalizado,FinalDate having FinalDate is null AND Outsurcing=0 AND Sindicalizado=0"));
        $NOOutsurcingSISindicalizado = DB::select( DB::raw("select COUNT(Outsurcing) as 'TOutsurcing',Outsurcing,  COUNT(Sindicalizado) as 'TSindicalizado',Sindicalizado  from head_counts group by Outsurcing,Sindicalizado,FinalDate having FinalDate is null AND Outsurcing=0 AND Sindicalizado=1"));
        $SIOutsurcingSISindicalizado = DB::select( DB::raw("select COUNT(Outsurcing) as 'TOutsurcing',Outsurcing,  COUNT(Sindicalizado) as 'TSindicalizado',Sindicalizado  from head_counts group by Outsurcing,Sindicalizado,FinalDate having FinalDate is null AND Outsurcing=1 AND Sindicalizado=1"));
        $SIOutsurcingNOSindicalizado = DB::select( DB::raw("select COUNT(Outsurcing) as 'TOutsurcing',Outsurcing,  COUNT(Sindicalizado) as 'TSindicalizado',Sindicalizado  from head_counts group by Outsurcing,Sindicalizado,FinalDate having FinalDate is null AND Outsurcing=1 AND Sindicalizado=0"));


       return view('dashboard.index', compact('porcentajetradingconcode','porcentajetradingsincode','SIOutsurcingNOSindicalizado','SIOutsurcingSISindicalizado','NOOutsurcingSISindicalizado','NOOutsurcingNOSindicalizado','polypolyOne','polypolyTwo','polypolyTotal','PolyWorkStatPeopleTotalTwo','PolyWorkStatPeopleTotalOne','PolyWorkStatPeopleTotal','PolyWorkStUnitPeople_id','polyProductionUnit_id','polyProductionUnGenral','PolyWorkStatTotal','PolyWorkStUnit_id','polypolyTwoTotalTresTresGrafica','polypolyTwoTotalTresTres','polypolyOneTotalTresTresGrafica','training_requests1','training_requests2','training_requests3','training_requests4','training_requests5','training_requests6','training_requests7','imagen','generes','headCount','resultado1','resultado2','resultado3','resultado4','resultado5','resultado6'));
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('imagen','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            if($request->hasFile('imagen'))
            {

                    $filename =  $request->imagen->store('imagen');

                    Imagen::create([
                        'imagen' => $filename
                    ]);
              
            }

            return redirect('pantalla/imagen')->with('flash_message', 'Imagen added!');
        }
        return response(view('403'), 403);
    }
    public function decargar($id)
    {
 
       

        $path = storage_path('app/imagen/' . $id);

        $file = File::get($path);
        $type = File::mimeType($path);
    
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
    
        return $response;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('imagen','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $imagen = Imagen::findOrFail($id);
            return view('imagen.imagen.show', compact('imagen'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $model = str_slug('imagen','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $imagen = Imagen::findOrFail($id);
            return view('imagen.imagen.edit', compact('imagen'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('imagen','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
                      

             if($request->hasFile('imagen'))
             {
 
                     $filename =  $request->imagen->store('imagen');
                     $imagen = Imagen::findOrFail($id);
                     $imagen->imagen = $filename;
                     $imagen->save();

                  
               
             }

             return redirect('pantalla/imagen')->with('flash_message', 'Imagen updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('imagen','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            $imagenFind =Imagen::find($id);

            Storage::delete($imagenFind->imagen);

            Imagen::destroy($id);

            return redirect('pantalla/imagen')->with('flash_message', 'Imagen deleted!');
        }
        return response(view('403'), 403);

    }
}

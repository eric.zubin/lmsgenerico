<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ManufacturingInstruction;
use Illuminate\Http\Request;
use App\productionUnit;
use App\Competive;
use App\Spec;


class ManufacturingInstructionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('manufacturinginstruction','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $manufacturinginstruction = ManufacturingInstruction::where('Code', 'LIKE', "%$keyword%")
                ->orWhere('Name', 'LIKE', "%$keyword%")
                ->orWhere('Description', 'LIKE', "%$keyword%")
                ->orWhere('Program', 'LIKE', "%$keyword%")
                ->orWhere('WS', 'LIKE', "%$keyword%")
                ->orWhere('NumePart', 'LIKE', "%$keyword%")
                ->orWhere('Competences', 'LIKE', "%$keyword%")
                ->orWhere('Revision', 'LIKE', "%$keyword%")
                ->orWhere('FechaInicio', 'LIKE', "%$keyword%")
                ->orWhere('FechaFin', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $manufacturinginstruction = ManufacturingInstruction::paginate($perPage);
            }

            return view('manufacturing_instruction.manufacturing-instruction.index', compact('manufacturinginstruction'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $productionUnits = productionUnit::all();
        $competives = Spec::all();

        $model = str_slug('manufacturinginstruction','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('manufacturing_instruction.manufacturing-instruction.create', compact('productionUnits','competives'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('manufacturinginstruction','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $manufacturingInstructionSave= ManufacturingInstruction::create($requestData);

            $manufacturingInstructionSave->Competives()->toggle($request->input("Competives"));

            return redirect('manufacturing_instruction/manufacturing-instruction')->with('flash_message', 'ManufacturingInstruction added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('manufacturinginstruction','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $manufacturinginstruction = ManufacturingInstruction::findOrFail($id);
            return view('manufacturing_instruction.manufacturing-instruction.show', compact('manufacturinginstruction'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $productionUnits = productionUnit::all();
        $competives = Spec::all();

        $model = str_slug('manufacturinginstruction','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $manufacturinginstruction = ManufacturingInstruction::findOrFail($id);
            return view('manufacturing_instruction.manufacturing-instruction.edit', compact('manufacturinginstruction','productionUnits','competives'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('manufacturinginstruction','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $manufacturinginstruction = ManufacturingInstruction::findOrFail($id);
             $manufacturinginstruction->update($requestData);
             $manufacturinginstruction->Competives()->sync($request->input("Competives"));            

             return redirect('manufacturing_instruction/manufacturing-instruction')->with('flash_message', 'ManufacturingInstruction updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('manufacturinginstruction','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            ManufacturingInstruction::destroy($id);

            return redirect('manufacturing_instruction/manufacturing-instruction')->with('flash_message', 'ManufacturingInstruction deleted!');
        }
        return response(view('403'), 403);

    }
}

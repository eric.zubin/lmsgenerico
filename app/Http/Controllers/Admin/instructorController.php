<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\instructor;
use App\companyInstructor;
use Illuminate\Http\Request;

class instructorController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('instructor','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $instructor = instructor::where('Num', 'LIKE', "%$keyword%")
                ->orWhere('Name', 'LIKE', "%$keyword%")
                ->orWhere('Company', 'LIKE', "%$keyword%")
                ->paginate($perPage);
            } else {
                $instructor = instructor::paginate($perPage);
            }

            return view('instructor.instructor.index', compact('instructor'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $companyInstructors = companyInstructor::all();
        
        $model = str_slug('instructor','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('instructor.instructor.create', compact('companyInstructors'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('instructor','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            


           if ($file = $request->file('pic_file')) {
            $extension = $file->extension()?: 'png';
            $destinationPath = public_path() . '/storage/uploads/instructor/';
            $safeName = str_random(10) . '.' . $extension;
            $file->move($destinationPath, $safeName);
            $request['pic'] = $safeName;
            }else{
                $request['pic'] = 'no_avatar.png';
            }
            $requestData = $request->all();

            instructor::create($requestData);



           return redirect('instructor/instructor')->with('flash_message', 'instructor added!');
        }
       return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $companyInstructors = companyInstructor::all();
        $model = str_slug('instructor','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $instructor = instructor::findOrFail($id);
            return view('instructor.instructor.show', compact('instructor','companyInstructors'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $companyInstructors = companyInstructor::all();
        $model = str_slug('instructor','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $instructor = instructor::findOrFail($id);
            return view('instructor.instructor.edit', compact('instructor','companyInstructors'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('instructor','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            if ($file = $request->file('pic_file')) {
                $extension = $file->extension()?: 'png';
                $destinationPath = public_path() . '/storage/uploads/instructor/';
                $safeName = str_random(10) . '.' . $extension;
                $file->move($destinationPath, $safeName);
                $request['pic'] = $safeName;
                }
                
            $requestData = $request->all();
            

            $instructor = instructor::findOrFail($id);
            $instructor->update($requestData);

             return redirect('instructor/instructor')->with('flash_message', 'instructor updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('instructor','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            instructor::destroy($id);

            return redirect('instructor/instructor')->with('flash_message', 'instructor deleted!');
        }
        return response(view('403'), 403);

    }
}

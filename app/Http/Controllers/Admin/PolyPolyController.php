<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use DB;

use App\instructor;
use App\Training;
use App\MaterialTrainig;
use App\FileTrainig;
use App\Competive;
use App\productionUnit;
use App\Program;
use App\workStation;

use Illuminate\Http\Request;

class PolyPolyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $workStationsProgram_id = workStation::select('Program_id')->get();
        $workStationsProductionUnit_id = workStation::select('ProductionUnit_id')->get();

        $productionUnits = productionUnit::whereIn('id',$workStationsProductionUnit_id)->get();
        $programs = Program::whereIn('id',$workStationsProgram_id)->get();



        $results = DB::select( DB::raw("select skillMatrizTablaz.T ,skillMatrizTablaz.NivelDeRiesgoOp,skillMatrizTablaz.N,skillMatrizTablaz.Wwork_stationsName,skillMatrizTablaz.Programs_Name,skillMatrizTablaz.WroktationID,
        skillMatrizTablaz.Programs_Id,skillMatrizTabla.C as C,skillMatrizTablaz.C as R
        FROM skillMatrizTablaz
        LEFT JOIN skillMatrizTabla
        ON skillMatrizTabla.WroktationID = skillMatrizTablaz.WroktationID"));

        

        $poly = DB::select( DB::raw("
        select SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
         WHEN M > 0 THEN M
         ELSE 0 END) as M,Programs_Id from ResultadosTotalesPolyPoly GROUP BY  Programs_Id"));

         $polyProductionUnit_id = DB::select( DB::raw("select 
         SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                                                    WHEN M > 0 THEN M
                                                    ELSE 0 END) as M,ProductionUnit_id
          from ResultadosTotalesPolyPoly,programs 
          where programs.id = ResultadosTotalesPolyPoly.Programs_Id
          GROUP BY  ProductionUnit_id"));

          $polyProductionUnGenral = DB::select( DB::raw("select 
          SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                                                     WHEN M > 0 THEN M
                                                     ELSE 0 END) as M
           from ResultadosTotalesPolyPoly"));

          
         $poly3Certificados = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,Programs_id from skillMatrizTabla GROUP BY  Programs_id"));
               
         //$poly3CertificadosPeople = DB::select( DB::raw("Select  COUNT(Program_id) as `totalP`,Program_id from poly3CertificadosPeopleEjemplo where total >= 3 GROUP BY Program_id"));


                    
         $poly3CertificadosPeople = DB::select( DB::raw("Select  headcountTotal,COUNT(poly3CertificadosPeopleEjemplo.Program_id) as `totalP`,poly3CertificadosPeopleEjemplo.Program_id from poly3CertificadosPeopleEjemplo,HeadcountByProgram_id where total >= 3 AND HeadcountByProgram_id.Program_id =poly3CertificadosPeopleEjemplo.Program_id GROUP BY poly3CertificadosPeopleEjemplo.Program_id"));

         
 
         $PolyWorkStUnit_id = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,ProductionUnit_id from skillMatrizTabla,programs where programs.id = skillMatrizTabla.Programs_id  GROUP BY  ProductionUnit_id"));
               
         $PolyWorkStatTotal = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly from skillMatrizTabla,programs where programs.id = skillMatrizTabla.Programs_id"));

          
         
         $PolyWorkStUnitPeople_id = DB::select( DB::raw("select  SUM(headcountTotal) as headcountTotal, SUM(totalP) as poly from PeopleCertifiedMinimumWorkStations  GROUP BY ProductionUnit_id"));
         $PolyWorkStatPeopleTotal = DB::select( DB::raw("select SUM(headcountTotal) as headcountTotal, SUM(totalP) as poly from PeopleCertifiedMinimumWorkStations"));

         //select  * from PeopleCertifiedMinimumWorkStations where ProductionUnit_id = 1
         //select  * from PeopleCertifiedMinimumWorkStations where ProductionUnit_id = 1


       
            return view('poly.index', compact('PolyWorkStatPeopleTotal','PolyWorkStUnitPeople_id','PolyWorkStUnit_id','PolyWorkStatTotal','polyProductionUnGenral','polyProductionUnit_id','productionUnits','results','programs','poly','poly3Certificados','poly3CertificadosPeople'));

    }


    /**
     * Create un exel
     *
     * @return \Illuminate\View\View
     */

    public function exel(Request $request)
    {
        $workStationsProgram_id = workStation::select('Program_id')->get();
        $workStationsProductionUnit_id = workStation::select('ProductionUnit_id')->get();

        $productionUnits = productionUnit::whereIn('id',$workStationsProductionUnit_id)->get();
        $programs = Program::whereIn('id',$workStationsProgram_id)->get();



        $results = DB::select( DB::raw("select * from skillMatrizTabla"));

        $poly = DB::select( DB::raw("
        select SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
         WHEN M > 0 THEN M
         ELSE 0 END) as M,Programs_Id from ResultadosTotalesPolyPoly GROUP BY  Programs_Id"));

         $polyProductionUnit_id = DB::select( DB::raw("select 
         SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                                                    WHEN M > 0 THEN M
                                                    ELSE 0 END) as M,ProductionUnit_id
          from ResultadosTotalesPolyPoly,programs 
          where programs.id = ResultadosTotalesPolyPoly.Programs_Id
          GROUP BY  ProductionUnit_id"));

          $polyProductionUnGenral = DB::select( DB::raw("select 
          SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                                                     WHEN M > 0 THEN M
                                                     ELSE 0 END) as M
           from ResultadosTotalesPolyPoly"));

          
         $poly3Certificados = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,Programs_id from skillMatrizTabla GROUP BY  Programs_id"));
               
         //$poly3CertificadosPeople = DB::select( DB::raw("Select  COUNT(Program_id) as `totalP`,Program_id from poly3CertificadosPeopleEjemplo where total >= 3 GROUP BY Program_id"));


                    
         $poly3CertificadosPeople = DB::select( DB::raw("Select  headcountTotal,COUNT(poly3CertificadosPeopleEjemplo.Program_id) as `totalP`,poly3CertificadosPeopleEjemplo.Program_id from poly3CertificadosPeopleEjemplo,HeadcountByProgram_id where total >= 3 AND HeadcountByProgram_id.Program_id =poly3CertificadosPeopleEjemplo.Program_id GROUP BY poly3CertificadosPeopleEjemplo.Program_id"));

         
 
         $PolyWorkStUnit_id = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,ProductionUnit_id from skillMatrizTabla,programs where programs.id = skillMatrizTabla.Programs_id  GROUP BY  ProductionUnit_id"));
               
         $PolyWorkStatTotal = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly from skillMatrizTabla,programs where programs.id = skillMatrizTabla.Programs_id"));

         
         $PolyWorkStUnitPeople_id = DB::select( DB::raw("select  SUM(headcountTotal) as headcountTotal, SUM(totalP) as poly from PeopleCertifiedMinimumWorkStations  GROUP BY ProductionUnit_id"));
         $PolyWorkStatPeopleTotal = DB::select( DB::raw("select SUM(headcountTotal) as headcountTotal, SUM(totalP) as poly from PeopleCertifiedMinimumWorkStations"));


    $file = Excel::create('Datos', function($excel) use ($PolyWorkStatPeopleTotal,$PolyWorkStUnitPeople_id,$PolyWorkStatTotal,$PolyWorkStUnit_id,$poly3CertificadosPeople,$poly3Certificados,$polyProductionUnGenral,$polyProductionUnit_id,$poly,$productionUnits,$results,$programs) {

        $excel->sheet('Poly', function($sheet) use ($PolyWorkStatPeopleTotal,$PolyWorkStUnitPeople_id,$PolyWorkStatTotal,$PolyWorkStUnit_id,$poly3CertificadosPeople,$poly3Certificados,$polyProductionUnGenral,$polyProductionUnit_id,$poly,$productionUnits,$results,$programs) {
    

            $sheet->loadView('poly.exel')->with('productionUnits',$productionUnits)
            ->with('results',$results)->with('programs',$programs)
            ->with('PolyWorkStatPeopleTotal',$PolyWorkStatPeopleTotal)
            ->with('PolyWorkStUnitPeople_id',$PolyWorkStUnitPeople_id)
            ->with('PolyWorkStatTotal',$PolyWorkStatTotal)
            ->with('PolyWorkStUnit_id',$PolyWorkStUnit_id)
            ->with('poly3CertificadosPeople',$poly3CertificadosPeople)
            ->with('poly3Certificados',$poly3Certificados)
            ->with('polyProductionUnGenral',$polyProductionUnGenral)
            ->with('polyProductionUnit_id',$polyProductionUnit_id)
            ->with('poly',$poly);
            

        });

        $excel->sheet('Poly 3x3', function($sheet) use ($PolyWorkStatPeopleTotal,$PolyWorkStUnitPeople_id,$PolyWorkStatTotal,$PolyWorkStUnit_id,$poly3CertificadosPeople,$poly3Certificados,$polyProductionUnGenral,$polyProductionUnit_id,$poly,$productionUnits,$results,$programs) {


            $sheet->loadView('poly.exelPoly')->with('productionUnits',$productionUnits)
            ->with('results',$results)->with('programs',$programs)
            ->with('PolyWorkStatPeopleTotal',$PolyWorkStatPeopleTotal)
            ->with('PolyWorkStUnitPeople_id',$PolyWorkStUnitPeople_id)
            ->with('PolyWorkStatTotal',$PolyWorkStatTotal)
            ->with('PolyWorkStUnit_id',$PolyWorkStUnit_id)
            ->with('poly3CertificadosPeople',$poly3CertificadosPeople)
            ->with('poly3Certificados',$poly3Certificados)
            ->with('polyProductionUnGenral',$polyProductionUnGenral)
            ->with('polyProductionUnit_id',$polyProductionUnit_id)
            ->with('poly',$poly);
    
        });

    });
    $file->store('xlsx')->download();

    //return view('poly.exel', compact('productionUnits','results','programs'));

}

public function exelPoly(Request $request)
{
    $workStationsProgram_id = workStation::select('Program_id')->orderBy('ProductionUnit_id', 'DESC')->get();
    $workStationsProductionUnit_id = workStation::select('ProductionUnit_id')->orderBy('ProductionUnit_id', 'DESC')->get();

    $productionUnits = productionUnit::whereIn('id',$workStationsProductionUnit_id)->get();
    $programs = Program::whereIn('id',$workStationsProgram_id)->get();


    $results = DB::select( DB::raw("select * from skillMatrizTabla"));

    $poly = DB::select( DB::raw("
    select SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
     WHEN M > 0 THEN M
     ELSE 0 END) as M,Programs_Id from ResultadosTotalesPolyPoly GROUP BY  Programs_Id"));

     $polyProductionUnit_id = DB::select( DB::raw("select 
     SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                                                WHEN M > 0 THEN M
                                                ELSE 0 END) as M,ProductionUnit_id
      from ResultadosTotalesPolyPoly,programs 
      where programs.id = ResultadosTotalesPolyPoly.Programs_Id
      GROUP BY  ProductionUnit_id"));

      $polyProductionUnGenral = DB::select( DB::raw("select 
      SUM(T) as T,SUM(C2) as C,SUM(N) as N, SUM(CASE 
                                                 WHEN M > 0 THEN M
                                                 ELSE 0 END) as M
       from ResultadosTotalesPolyPoly"));

      
     $poly3Certificados = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,Programs_id from skillMatrizTabla GROUP BY  Programs_id"));
           
     //$poly3CertificadosPeople = DB::select( DB::raw("Select  COUNT(Program_id) as `totalP`,Program_id from poly3CertificadosPeopleEjemplo where total >= 3 GROUP BY Program_id"));


                
     $poly3CertificadosPeople = DB::select( DB::raw("Select  headcountTotal,COUNT(poly3CertificadosPeopleEjemplo.Program_id) as `totalP`,poly3CertificadosPeopleEjemplo.Program_id from poly3CertificadosPeopleEjemplo,HeadcountByProgram_id where total >= 3 AND HeadcountByProgram_id.Program_id =poly3CertificadosPeopleEjemplo.Program_id GROUP BY poly3CertificadosPeopleEjemplo.Program_id"));

     

     $PolyWorkStUnit_id = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly,ProductionUnit_id from skillMatrizTabla,programs where programs.id = skillMatrizTabla.Programs_id  GROUP BY  ProductionUnit_id"));
           
     $PolyWorkStatTotal = DB::select( DB::raw("select  COUNT(*) as total,SUM(case when  (C >= 3) then 1 else 0 end) as poly from skillMatrizTabla,programs where programs.id = skillMatrizTabla.Programs_id"));

     
     $PolyWorkStUnitPeople_id = DB::select( DB::raw("select  SUM(headcountTotal) as headcountTotal, SUM(totalP) as poly from PeopleCertifiedMinimumWorkStations  GROUP BY ProductionUnit_id"));
     $PolyWorkStatPeopleTotal = DB::select( DB::raw("select SUM(headcountTotal) as headcountTotal, SUM(totalP) as poly from PeopleCertifiedMinimumWorkStations"));


$file = Excel::create('Datos', function($excel) use ($PolyWorkStatPeopleTotal,$PolyWorkStUnitPeople_id,$PolyWorkStatTotal,$PolyWorkStUnit_id,$poly3CertificadosPeople,$poly3Certificados,$polyProductionUnGenral,$polyProductionUnit_id,$poly,$productionUnits,$results,$programs) {

    $excel->sheet('Poly', function($sheet) use ($PolyWorkStatPeopleTotal,$PolyWorkStUnitPeople_id,$PolyWorkStatTotal,$PolyWorkStUnit_id,$poly3CertificadosPeople,$poly3Certificados,$polyProductionUnGenral,$polyProductionUnit_id,$poly,$productionUnits,$results,$programs) {
    

        $sheet->loadView('poly.exel')->with('productionUnits',$productionUnits)
        ->with('results',$results)->with('programs',$programs)
        ->with('PolyWorkStatPeopleTotal',$PolyWorkStatPeopleTotal)
        ->with('PolyWorkStUnitPeople_id',$PolyWorkStUnitPeople_id)
        ->with('PolyWorkStatTotal',$PolyWorkStatTotal)
        ->with('PolyWorkStUnit_id',$PolyWorkStUnit_id)
        ->with('poly3CertificadosPeople',$poly3CertificadosPeople)
        ->with('poly3Certificados',$poly3Certificados)
        ->with('polyProductionUnGenral',$polyProductionUnGenral)
        ->with('polyProductionUnit_id',$polyProductionUnit_id)
        ->with('poly',$poly);
        

    });

    $excel->sheet('Poly 3x3', function($sheet) use ($PolyWorkStatPeopleTotal,$PolyWorkStUnitPeople_id,$PolyWorkStatTotal,$PolyWorkStUnit_id,$poly3CertificadosPeople,$poly3Certificados,$polyProductionUnGenral,$polyProductionUnit_id,$poly,$productionUnits,$results,$programs) {


        $sheet->loadView('poly.exelPoly')->with('productionUnits',$productionUnits)
        ->with('results',$results)->with('programs',$programs)
        ->with('PolyWorkStatPeopleTotal',$PolyWorkStatPeopleTotal)
        ->with('PolyWorkStUnitPeople_id',$PolyWorkStUnitPeople_id)
        ->with('PolyWorkStatTotal',$PolyWorkStatTotal)
        ->with('PolyWorkStUnit_id',$PolyWorkStUnit_id)
        ->with('poly3CertificadosPeople',$poly3CertificadosPeople)
        ->with('poly3Certificados',$poly3Certificados)
        ->with('polyProductionUnGenral',$polyProductionUnGenral)
        ->with('polyProductionUnit_id',$polyProductionUnit_id)
        ->with('poly',$poly);

    });

});
$file->store('xlsx')->download();



//return view('poly.exel', compact('productionUnits','results','programs'));

}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $instructor = instructor::all();
        $competives = Competive::all();


        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('training.training.create', compact('instructor','competives'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
           
            $trading = Training::create($requestData);
            $tradingSave = Training::find($trading->id);
            $tradingSave->instructors()->toggle($request->input("Instructors"));
            $tradingSave->Competives()->toggle($request->input("Competives"));

            $archivos = $request->file('Archivos');

            if($request->hasFile('Archivos'))
            {
                foreach ($archivos as $file) {
                    echo $file;
                    $filename =  $file->store('Archivos/' . $trading->id );
                    FileTrainig::create([
                        'Trainig_id' => $trading->id,
                        'Archivo' => $filename
                    ]);
                }
               
            }
            
            
            $materiales = $request->file('Materiales');

            if($request->hasFile('Materiales'))
            {
                foreach ($materiales as $file) {
                    $filename =  $file->store('Materiales/' . $trading->id );
                    MaterialTrainig::create([
                        'Trainig_id' => $trading->id,
                        'Archivo' => $filename
                    ]);
                }
              
            }


          
            return redirect('training/training')->with('flash_message', 'Training added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $instructor = instructor::all();

        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $training = Training::findOrFail($id);
            return view('training.training.show', compact('training','instructor'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $instructor = instructor::all();
        $competives = Competive::all();

        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $training = Training::findOrFail($id);
            return view('training.training.edit', compact('training','instructor','competives'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $training = Training::findOrFail($id);
            $training->update($requestData);

            $training->instructors()->sync($request->input("Instructors"));
            $training->Competives()->sync($request->input("Competives"));



             return redirect('training/training')->with('flash_message', 'Training updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('training','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            Training::destroy($id);

            return redirect('training/training')->with('flash_message', 'Training deleted!');
        }
        return response(view('403'), 403);

    }
}

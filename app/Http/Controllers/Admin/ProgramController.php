<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\productionUnit;
use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('program','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $program = Program::where('ProductionUnit', 'LIKE', "%$keyword%")
                ->orWhere('Code', 'LIKE', "%$keyword%")
                ->orWhere('Name', 'LIKE', "%$keyword%")
                ->orWhere('Description', 'LIKE', "%$keyword%");
                        } else {
                $program = Program::all();
            }

            return view('program.program.index', compact('program'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $productionUnits = productionUnit::all();

        $model = str_slug('program','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('program.program.create', compact('productionUnits'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('program','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            $this->validate($request, [
			'Code' => 'required'
		]);
            $requestData = $request->all();
            
            Program::create($requestData);
            return redirect('program/program')->with('flash_message', 'Program added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $productionUnits = productionUnit::all();

        $model = str_slug('program','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $program = Program::findOrFail($id);
            return view('program.program.show', compact('program','productionUnits'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $productionUnits = productionUnit::all();

        $model = str_slug('program','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $program = Program::findOrFail($id);
            return view('program.program.edit', compact('program','productionUnits'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('program','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $this->validate($request, [
			'Code' => 'required'
		]);
            $requestData = $request->all();
            
            $program = Program::findOrFail($id);
             $program->update($requestData);

             return redirect('program/program')->with('flash_message', 'Program updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('program','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            Program::destroy($id);

            return redirect('program/program')->with('flash_message', 'Program deleted!');
        }
        return response(view('403'), 403);

    }


    /**
     * select_program the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return JSON
     */
    public function select_program($id)
    {
    
        $program = Program::where('ProductionUnit_id', '=', $id)->get();
        $programfirst = Program::where('ProductionUnit_id', '=', $id)->first();


        $html ="";
        $html.= "<option value=''></option>";

        foreach($program as $key => $value)
        {

        $html.= "<option value='".$value->id ."'>".$value->Name ."</option>";

        }



        return response()->json([
            'html' => $html,
            'programfirst' => $programfirst
        ]);

    }


    
}

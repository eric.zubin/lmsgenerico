<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\headCount;
use App\TradeRol;
use Illuminate\Http\Request;

class headCountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('headcount','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $headcount = headCount::where('Num', 'LIKE', "%$keyword%")
                ->orWhere('Name', 'LIKE', "%$keyword%")
                ->orWhere('TradeRol', 'LIKE', "%$keyword%")
                ->orWhere('Company', 'LIKE', "%$keyword%")
                ->orWhere('Sindicalizado', 'LIKE', "%$keyword%")
                ->orWhere('NivelPuesto', 'LIKE', "%$keyword%")
                ->orWhere('Puesto', 'LIKE', "%$keyword%")
                ->orWhere('Email', 'LIKE', "%$keyword%")
                ->orWhere('AreaAsigned', 'LIKE', "%$keyword%")
                ->orWhere('CostCenter', 'LIKE', "%$keyword%")
                ->orWhere('DirectLeader', 'LIKE', "%$keyword%")
                ->orWhere('Genere', 'LIKE', "%$keyword%")
                ->orWhere('JobDescripcion', 'LIKE', "%$keyword%");
            } else {
                $headcount = headCount::all();
            }

            return view('head_count.head-count.index', compact('headcount'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tradeRoles = TradeRol::all();

        $model = str_slug('headcount','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('head_count.head-count.create', compact('tradeRoles'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('headcount','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            if ($file = $request->file('pic')) {
                $extension = $file->extension()?: 'png';
                $destinationPath = public_path() . '/storage/uploads/headcount/';
                $safeName = $file->getClientOriginalName() . '.' . $extension;
                $file->move($destinationPath, $safeName);
                $request['pic_file'] = $safeName;
                }else{
                    $request['pic_file'] = 'no_avatar.png';
                }
            $requestData = $request->all();
            
            headCount::create($requestData);
            return redirect('head_count/head-count')->with('flash_message', 'headCount added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $model = str_slug('headcount','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $headcount = headCount::findOrFail($id);
            return view('head_count.head-count.show', compact('headcount'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        
        $tradeRoles = TradeRol::all();

        $model = str_slug('headcount','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $headcount = headCount::findOrFail($id);
            return view('head_count.head-count.edit', compact('headcount','tradeRoles'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('headcount','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
   

                if ($file = $request->file('pic')) {
                    $extension = $file->extension()?: 'png';
                    $destinationPath = public_path() . '/storage/uploads/headcount/';
                    $safeName = $file->getClientOriginalName() . '.' . $extension;
                    $file->move($destinationPath, $safeName);
                    $request['pic_file'] = $safeName;
                    

                    }

                        $requestData = $request->all();
                        $headcount = headCount::findOrFail($id);
                        print_r($requestData);

                        $headcount->update($requestData);
                    

             return redirect('head_count/head-count')->with('flash_message', 'headCount updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('headcount','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            headCount::destroy($id);

            return redirect('head_count/head-count')->with('flash_message', 'headCount deleted!');
        }
        return response(view('403'), 403);

    }
}

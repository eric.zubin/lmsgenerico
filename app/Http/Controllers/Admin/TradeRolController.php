<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\productionUnit;
use App\TradeRol;
use Illuminate\Http\Request;
use App\Competive;

class TradeRolController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $model = str_slug('traderol','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $keyword = $request->get('search');
            $perPage = 25;

            if (!empty($keyword)) {
                $traderol = TradeRol::where('ProductionUnit', 'LIKE', "%$keyword%")
                ->orWhere('Program', 'LIKE', "%$keyword%")
                ->orWhere('Code', 'LIKE', "%$keyword%")
                ->orWhere('Name', 'LIKE', "%$keyword%")
                ->orWhere('Description', 'LIKE', "%$keyword%")
                ->orWhere('Competives', 'LIKE', "%$keyword%")->get();
            
            } else {
                $traderol = TradeRol::all();
            }

            return view('trade_rol.trade-rol.index', compact('traderol'));
        }
        return response(view('403'), 403);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $productionUnits = productionUnit::all();
        $competives = Competive::all();

        $model = str_slug('traderol','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            return view('trade_rol.trade-rol.create', compact('productionUnits','competives'));
        }
        return response(view('403'), 403);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $model = str_slug('traderol','-');
        if(auth()->user()->permissions()->where('name','=','add-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $traderolSave =TradeRol::create($requestData);
            $traderolSave->Competives()->toggle($request->input("Competives"));
  
            

            return redirect('trade_rol/trade-rol')->with('flash_message', 'TradeRol added!');
        }
        return response(view('403'), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {

        $model = str_slug('traderol','-');
        if(auth()->user()->permissions()->where('name','=','view-'.$model)->first()!= null) {
            $traderol = TradeRol::findOrFail($id);
            return view('trade_rol.trade-rol.show', compact('traderol'));
        }
        return response(view('403'), 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $productionUnits = productionUnit::all();
        $competives = Competive::all();

        $model = str_slug('traderol','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            $traderol = TradeRol::findOrFail($id);
            return view('trade_rol.trade-rol.edit', compact('traderol','productionUnits','competives'));
        }
        return response(view('403'), 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $model = str_slug('traderol','-');
        if(auth()->user()->permissions()->where('name','=','edit-'.$model)->first()!= null) {
            
            $requestData = $request->all();
            
            $traderol = TradeRol::findOrFail($id);
             $traderol->update($requestData);
            
             $traderol->Competives()->sync($request->input("Competives"));



             return redirect('trade_rol/trade-rol')->with('flash_message', 'Trade Rol updated!');
        }
        return response(view('403'), 403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $model = str_slug('traderol','-');
        if(auth()->user()->permissions()->where('name','=','delete-'.$model)->first()!= null) {
            TradeRol::destroy($id);

            return redirect('trade_rol/trade-rol')->with('flash_message', 'Trade Rol deleted!');
        }
        return response(view('403'), 403);

    }
}

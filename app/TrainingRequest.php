<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingRequest extends Model
{
    use SoftDeletes;

    //Permite el borrado logico pero no de la base de datos
    //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training_requests';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['User', 'Training_id', 'DateToday', 'DateRequest', 'Employees','DateTrainedNeddFrom','DateTrainedNeddTo','TrainingPlan','DateClosed','DateArchived','DateDocumented','DateEvaluation','DatePlan','DateTraining','DateValidation','CheckClosed','CheckEvaluation','CheckValidation',
    'InstructorComments','SuggetedInstructor','SuggetedAmountOfPeople','InstructorCoachExternal','InstructorAvalable','InstructorApproved','SpecifiationsNeddCouse','Supplier','Company','NotRequired','Rejeted','MaterialText','Approved','PO','Coin','TrainingCost','CheckPlan','CheckTraining',
    'Manager','Curstomer','Area','Signature','Requestor','Phone','Email','HrAprobal','Date','CurrentSituation','DesiredSituation','PrincipalReason','OthrePrincipalReason','RequestName','TrainingType','CourseShould1','CourseShould2','CourseShould3','CourseShould4','TrainingSolutionRequiered','TrainingMaterialStatus','QuantityPeopleTrained','TrainingLevel','EstimatedTotalTime','DateTrainedNedd','TimeWantPeopleWantFrom','TimeWantPeopleWantTo','ShiftsYoWantTRaing1','ShiftsYoWantTRaing2','ShiftsYoWantTRaing2','ValidationDays','PlanDays','TrainingDays','EvaluationDays','DocumentedDays','ArchivedDays','ClosesDays'];

    public function Training()
    {
        return $this->hasOne('App\Training', 'id','Training_id');
    }
    
    public function TrainingPlan()
    {
        return $this->hasOne('App\TrainingPlan', 'id','TrainingPlan_id');
    }


}

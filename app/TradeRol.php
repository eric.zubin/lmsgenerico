<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TradeRol extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trade_rols';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ProductionUnit_id', 'Program_id', 'Code', 'Name', 'Description','RolLevel'];

    public function ProductionUnit()
    {
        return $this->hasOne('App\productionUnit', 'id','ProductionUnit_id');
    }

    public function Program()
    {
        return $this->hasOne('App\Program', 'id','Program_id');
    }

    public function Competives()
    {
        return $this->belongsToMany('App\Competive', 'tradelrol_competive', 
        'traderol_id', 'competive_id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Training extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $hidden = array('pivot');
    protected $table = 'trainings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Code', 'Name', 'Type','Level', 'Language', 'Version','Objetivo','Description','TotalHours','TrainingSolution'];
    
    
    public function instructors()
    {
        return $this->belongsToMany('App\instructor', 'trainings_instructors', 
        'trainings_id', 'instructors_id');
    }

    public function materiales()
    {
        return $this->hasMany('App\MaterialTrainig','Trainig_id','id');
    }

    public function archivos()
    {
        return $this->hasMany('App\FileTrainig','Trainig_id','id');
    }

    public function Competives()
    {
        return $this->belongsToMany('App\Competive', 'trainings_competive', 
        'trainings_id', 'competive_id');
    }

    public function Companies()
    {
        return $this->belongsToMany('App\companyInstructor', 'trainings_companyInstructor', 
        'trainings_id', 'companyInstructor_id');
    }
}

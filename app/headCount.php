<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class headCount extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'head_counts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Num', 'Name', 'Company', 'Sindicalizado', 'NivelPuesto', 'Puesto', 'Email','Traderol_id',
    'AreaAsigned','CostCenter','DirectLeader','Genere','JobDescripcion','InitialDate','FinalDate','Outsurcing','pic_file'];

    public function TradeRol()
    {
     return $this->hasOne('App\TradeRol', 'id','Traderol_id');
    }
}

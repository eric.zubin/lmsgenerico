<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Competive extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'competives';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Name', 'Code', 'Type', 'Level','Description'];

    /*
    public function Training()
    {
        return $this->hasOne('App\Training', 'id','Training_id');
    }*/

    public function Specs()
    {
        return $this->belongsToMany('App\Spec', 'competive_specs', 
        'competive_id', 'spec_id');
    }
    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistrerTrading extends Model
{
    use SoftDeletes;

      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registrer_trading';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [ 'TradingPlan_id', 'HeadCount_id','Evidencia'];

    public function Trainig()
    {
        return $this->hasOne('App\Training', 'id','Trainig_id');
    }
}
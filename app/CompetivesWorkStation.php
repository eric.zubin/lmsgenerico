<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompetivesWorkStation extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'work_stations_competive';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['work_stations_id', 'competive_id','ProductionUnits', 'ProgramIDs', 'WorkStationIDs', 'compenencias'];




 
}

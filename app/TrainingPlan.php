<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingPlan extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'training_plans';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Code' , 'FechaInicio', 'FechaFin','Training_id','HRS','NUM_EMPLOYEES','COSTOPOR_PER'];


    public function Training()
    {
        return $this->hasOne('App\Training', 'id','Training_id');
    }


    public function ProductionUnit()
    {
        return $this->hasOne('App\productionUnit', 'id','ProductionUnit_id');
    }

    public function Program()
    {
        return $this->hasOne('App\Program', 'id','Program_id');
    }

    
}
 
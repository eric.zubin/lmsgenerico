<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialTrainig extends Model
{
    use SoftDeletes;

      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'material_trainigs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [ 'Archivo', 'Trainig_id'];

    public function Trainig()
    {
        return $this->hasOne('App\Training', 'id','Trainig_id');
    }
}
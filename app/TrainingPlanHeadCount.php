<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TrainingPlanHeadCount extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tradingplan_headcount';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['trading_plan_id' , 'head_count_id','Qualification','Assistance','ROI2','ROI1'];


    public function TrainingPlan()
    {
        return $this->hasOne('App\TrainingRegistrer', 'id','trading_plan_id');
    }
    
    public function HeadCount()
    {
        return $this->hasOne('App\headCount', 'id','head_count_id');
    }
}

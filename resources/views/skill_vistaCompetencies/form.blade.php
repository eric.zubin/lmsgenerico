<div class="form-group {{ $errors->has('NUM') ? 'has-error' : ''}}">
    <label for="NUM" class="col-md-4 control-label">{{ 'Num' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="NUM" type="text" id="NUM" value="{{ $skillmatriz->NUM or ''}}" >
        {!! $errors->first('NUM', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('NOMBRE') ? 'has-error' : ''}}">
    <label for="NOMBRE" class="col-md-4 control-label">{{ 'Nombre' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="NOMBRE" type="text" id="NOMBRE" value="{{ $skillmatriz->NOMBRE or ''}}" >
        {!! $errors->first('NOMBRE', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('WS1') ? 'has-error' : ''}}">
    <label for="WS1" class="col-md-4 control-label">{{ 'Ws1' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="WS1" type="text" id="WS1" value="{{ $skillmatriz->WS1 or ''}}" >
        {!! $errors->first('WS1', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('WS2') ? 'has-error' : ''}}">
    <label for="WS2" class="col-md-4 control-label">{{ 'Ws2' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="WS2" type="text" id="WS2" value="{{ $skillmatriz->WS2 or ''}}" >
        {!! $errors->first('WS2', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('WS3') ? 'has-error' : ''}}">
    <label for="WS3" class="col-md-4 control-label">{{ 'Ws3' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="WS3" type="text" id="WS3" value="{{ $skillmatriz->WS3 or ''}}" >
        {!! $errors->first('WS3', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('WS4') ? 'has-error' : ''}}">
    <label for="WS4" class="col-md-4 control-label">{{ 'Ws4' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="WS4" type="text" id="WS4" value="{{ $skillmatriz->WS4 or ''}}" >
        {!! $errors->first('WS4', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('EXPERT') ? 'has-error' : ''}}">
    <label for="EXPERT" class="col-md-4 control-label">{{ 'Expert' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="EXPERT" type="text" id="EXPERT" value="{{ $skillmatriz->EXPERT or ''}}" >
        {!! $errors->first('EXPERT', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>

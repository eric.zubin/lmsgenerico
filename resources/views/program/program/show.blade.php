@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Program {{ $program->id }}</h3>
                    @can('view-'.str_slug('Program'))
                        <a class="btn btn-success pull-right" href="{{ url('/program/program') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $program->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $program->Code }} </td></tr>
                            <tr><th> Production Unit </th><td> {{ $program->ProductionUnit->Name }} </td></tr>
                            <tr><th> Name </th><td> {{ $program->Name }} </td></tr>
                            <tr><th> Description </th><td> {{ $program->Description }} </td></tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


<div class="form-group {{ $errors->has('ProductionUnit_id') ? 'has-error' : ''}}">

<label for="ProductionUnit_id" class="col-md-4 control-label">{{ 'Production Unit' }}</label>
                        <div class="col-md-6 ">
                            <select required class="form-control" name="ProductionUnit_id" id="ProductionUnit_id" value="{{ $program->ProductionUnit_id or ''}}">

                            @isset($program->ProductionUnit_id)
                            <option selected hidden value="{{$program->ProductionUnit_id}}">{{ $program->ProductionUnit->Name }}</option>
                            @endisset
                                @foreach($productionUnits as $item)
                                    <option value="{{ $item->id }}">{{ $item->Name }}</option>
                                @endforeach
                            </select>
                           
                        </div>
        {!! $errors->first('ProductionUnit_id', '<p class="help-block">:message</p>') !!}


</div><div class="form-group {{ $errors->has('Code') ? 'has-error' : ''}}">
    <label for="Code" class="col-md-4 control-label">{{ 'Code' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Code" type="text" id="Code" value="{{ $program->Code or ''}}" required>
        {!! $errors->first('Code', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $program->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
    <label for="Description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Description" type="text" id="Description" value="{{ $program->Description or ''}}" >
        {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>

@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Manufacturing Instruction {{ $manufacturinginstruction->id }}</h3>
                    @can('view-'.str_slug('ManufacturingInstruction'))
                        <a class="btn btn-success pull-right" href="{{ url('/manufacturing_instruction/manufacturing-instruction') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $manufacturinginstruction->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $manufacturinginstruction->Code }} </td></tr>
                            <tr><th> Production Unit </th><td> {{ $manufacturinginstruction->ProductionUnit->Name }} </td></tr>
                            <tr><th> Program </th><td> {{ $manufacturinginstruction->Program->Name }} </td></tr>
                            <tr><th> WS </th><td> {{ $manufacturinginstruction->WS->Name }} </td></tr>
                            <tr><th> Name </th><td> {{ $manufacturinginstruction->Name }} </td></tr>
                            <tr><th> Description </th><td> {{ $manufacturinginstruction->Description }} </td></tr>
                            <tr><th> Revision </th><td> {{ $manufacturinginstruction->Revision }} </td></tr>
                            <tr><th> Fecha Inicio </th><td> {{ $manufacturinginstruction->FechaInicio }} </td></tr>
                            <tr><th> Fecha Fin </th><td> {{ $manufacturinginstruction->FechaFin }} </td></tr>
                            <tr><th> Specs </th><td> 
                            @foreach($manufacturinginstruction->Competives as $item)
                            <ul>{{ $item->Name }} </ul>
                            
                            @endforeach
                            </td></tr>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


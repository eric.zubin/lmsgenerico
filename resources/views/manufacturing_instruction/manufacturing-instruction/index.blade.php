@extends('layouts.master')

@push('css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
@endpush

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Manufacturing Instruction</h3>
                    @can('add-'.str_slug('ManufacturingInstruction'))
                        <a class="btn btn-success pull-right" href="{{ url('/manufacturing_instruction/manufacturing-instruction/create') }}"><i
                                    class="icon-plus"></i> Add Manufacturing Instruction</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table nowrap" id="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Actions</th>

                                <th>Code</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th> Production Unit </th>
                                <th> Program </th>
                                <th> WS </th>
                                <th> Revision </th>
                                <th> Date Start </th>
                                <th> Date End </th>
                                <th> Specs </th>
                                <th>created_at </th>
                                <th>updated_at</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($manufacturinginstruction as $item)
                                <tr>
                                <td>{{ $loop->iteration or $item->id }}</td>

                                <td>
                                        @can('view-'.str_slug('ManufacturingInstruction'))
                                            <a href="{{ url('/manufacturing_instruction/manufacturing-instruction/' . $item->id) }}"
                                               title="View ManufacturingInstruction">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View
                                                </button>
                                            </a>
                                        @endcan

                                        @can('edit-'.str_slug('ManufacturingInstruction'))
                                            <a href="{{ url('/manufacturing_instruction/manufacturing-instruction/' . $item->id . '/edit') }}"
                                               title="Edit ManufacturingInstruction">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                </button>
                                            </a>
                                        @endcan

                                        @can('delete-'.str_slug('ManufacturingInstruction'))
                                            <form method="POST"
                                                  action="{{ url('/manufacturing_instruction/manufacturing-instruction' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Delete ManufacturingInstruction"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                </button>
                                            </form>
                                        @endcan
                                        </td>

                                    <td>{{ $item->Code }}</td>
                                    <td>{{ $item->Name }}</td>
                                    <td>{{ $item->Description }}</td>
                  
                            <td> {{ $item->ProductionUnit->Name }} </td>
                            <td> {{ $item->Program->Name }} </td>
                            
                            <td> 
                            @if($item->WS != null)
                                {{ $item->WS->Name }} 
                            @endif

                            
                            </td>
                            <td> {{ $item->Revision }} </td>
                            <td> {{ $item->FechaInicio }} </td>
                            <td> {{ $item->FechaFin }} </td>
                            <td> 
                            @foreach($item->Competives as $item)
                            <ul>{{ $item->Name }} </ul>
                            
                            @endforeach
                            </td>
                            <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->updated_at }}</td>

                                </tr>
                            @endforeach

                            
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $manufacturinginstruction->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>

    <script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function () {

            @if(\Session::has('message'))
            $.toast({
                heading: 'Success!',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3000,
                stack: 6
            });
            @endif
        })

        $(function () {
            $('#myTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }]
            });

        });
    </script>

@endpush

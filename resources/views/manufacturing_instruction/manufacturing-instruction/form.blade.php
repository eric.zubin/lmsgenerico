@push('css')
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
@endpush


<div class="form-group {{ $errors->has('Code') ? 'has-error' : ''}}">
    <label for="Code" class="col-md-4 control-label">{{ 'Code' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Code" type="text" id="Code" value="{{ $manufacturinginstruction->Code or ''}}" >
        {!! $errors->first('Code', '<p class="help-block">:message</p>') !!}
    </div>
    </div>

    <div class="form-group {{ $errors->has('ProductionUnit_id') ? 'has-error' : ''}}">

    <label for="ProductionUnit_id" class="col-md-4 control-label">{{ 'Production Unit' }}</label>
                        <div class="col-md-6 ">
                            <select class="form-control" name="ProductionUnit_id" id="ProductionUnit_id" value="{{ $manufacturinginstruction->ProductionUnit_id or ''}}">
                                @isset($manufacturinginstruction->ProductionUnit_id)
                                <option selected hidden value="{{$manufacturinginstruction->ProductionUnit_id}}">{{ $manufacturinginstruction->ProductionUnit->Name }}</option>
                                @endisset
                                @foreach($productionUnits as $item)
                                    <option value="{{ $item->id }}">{{ $item->Name }}</option>
                                @endforeach
                            </select>
                        </div>
        {!! $errors->first('ProductionUnit_id', '<p class="help-block">:message</p>') !!}
    
    </div><div class="form-group {{ $errors->has('Program_id') ? 'has-error' : ''}}">
    <label for="Program_id" class="col-md-4 control-label">{{ 'Program_id' }}</label>
    <div class="col-md-6">
            <select class="form-control" name="Program_id" id="Program_id" value="{{ $manufacturinginstruction->Program_id or ''}}">
             
            </select>
        {!! $errors->first('Program', '<p class="help-block">:message</p>') !!}
    </div>

    </div><div class="form-group {{ $errors->has('WS_id') ? 'has-error' : ''}}">
    <label for="WS_id" class="col-md-4 control-label">{{ 'WS_id' }}</label>
    <div class="col-md-6">
            <select class="form-control" name="WS_id" id="WS_id" value="{{ $manufacturinginstruction->WS_id or ''}}">
      
            </select>
        {!! $errors->first('WS_id', '<p class="help-block">:message</p>') !!}
    </div>
    
</div><div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $manufacturinginstruction->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
    <label for="Description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Description" type="text" id="Description" value="{{ $manufacturinginstruction->Description or ''}}" >
        {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('NumePart') ? 'has-error' : ''}}">
    <label for="NumePart" class="col-md-4 control-label">{{ 'Numepart' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="NumePart" type="text" id="NumePart" value="{{ $manufacturinginstruction->NumePart or ''}}" >
        {!! $errors->first('NumePart', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Competences') ? 'has-error' : ''}}">
    <label for="Competences" class="col-md-4 control-label">{{ 'Specs' }}</label>
    <div class="col-md-6">
                    <select class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Choose" name="Competives[]" id="Competives">
                        <optgroup label="Choose Competence">
                        @isset($manufacturinginstruction->Competives)
                                @foreach($manufacturinginstruction->Competives as $item)
                                <option value="{{$item->id}}" selected="selected">{{ $item->Name }}</option>
                                @endforeach
                        @endisset
                        @foreach($competives as $item)
                            <option value="{{ $item->id }}">{{ $item->Name }}</option>
                        @endforeach
                        </optgroup>
                    </select>
        {!! $errors->first('Competences', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Revision') ? 'has-error' : ''}}">
    <label for="Revision" class="col-md-4 control-label">{{ 'Revision' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Revision" type="text" id="Revision" value="{{ $manufacturinginstruction->Revision or ''}}" >
        {!! $errors->first('Revision', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('FechaInicio') ? 'has-error' : ''}}">
    <label for="FechaInicio" class="col-md-4 control-label">{{ 'Fecha Inicio' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="FechaInicio" type="date" id="FechaInicio" value="{{ $manufacturinginstruction->FechaInicio or ''}}" >
        {!! $errors->first('FechaInicio', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('FechaFin') ? 'has-error' : ''}}">
    <label for="FechaFin" class="col-md-4 control-label">{{ 'Fecha Fin' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="FechaFin" type="date" id="FechaFin" value="{{ $manufacturinginstruction->FechaFin or ''}}" >
        {!! $errors->first('FechaFin', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>

@push('js')
    <script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>

<script>

jQuery(document).ready(function() {
         
         // For select 2
         $(".select2").select2();
         $('.selectpicker').selectpicker();

     });

Program_id();
   $("#ProductionUnit_id").change(function () {
					$("#ProductionUnit_id option:selected").each(function () {
                        Program_id();
					});
				})

    $("#Program_id").change(function () {
                        $("#Program_id option:selected").each(function () {
                            WS();
                        });
                    })


                    
        


function WS(params) {
    var Program_id = $("#Program_id").val();
    var ProductionUnit_id = $("#ProductionUnit_id").val();

$.ajax({
    url: "/select_ws/"+ProductionUnit_id+"/"+Program_id,
    type: "post",
    data: { "_token": "{{ csrf_token() }}"} ,
    success: function (response) {
                    $('#WS_id').empty().append( response);
                    @isset($manufacturinginstruction->WS_id)
                                    $("#WS_id").val({{$manufacturinginstruction->WS_id}});
                        @endisset
            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }


});
}

function Program_id() {
    var id = $("#ProductionUnit_id").val();
    $.ajax({
        url: "/select_program/"+id,
        type: "post",
        dataType: 'json',
        data: { "_token": "{{ csrf_token() }}"} ,
        success: function (response) {
                        $('#Program_id').empty().append( response.html);
                        @isset($manufacturinginstruction->Program_id)
                                    $("#Program_id").val({{$manufacturinginstruction->Program_id}});
                        @endisset
                        WS();

                },
                error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                }
    });
   
}


</script>
@endpush

@push('css')
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
  
    @endpush

<h3> {{ $training->Training->Code }} {{ $training->Training->Name }} {{ $training->FechaInicio }} {{ $training->FechaFin }} </h3>

    <div class="col-md-6">
        <input class="form-control" name="trading_plan_id" type="hidden" id="trading_plan_id" value="{{ $training->id }}" >
    </div>

    @isset($trainingPlanHeadCount->training_tregistrer)
<input class="form-control" name="training_tregistrer" type="hidden" id="training_tregistrer" value="{{ $trainingRegistrer->id }}" >
@endisset


<div class="form-group {{ $errors->has('head_count_id') ? 'has-error' : ''}}">
    <label for="head_count_id" class="col-md-2 control-label">{{ 'Employed' }}</label>
    <div class="col-md-6">
             <select class="form-control select2" name="head_count_id" id="head_count_id"  value="{{ $trainingPlanHeadCount->head_count_id or ''}}">
            @isset($trainingPlanHeadCount->head_count_id)
            <option selected  value="{{$headCountP->id}}">{{ $headCountP->Num }} {{ $headCountP->Name }}</option>
            @endisset
      
            @foreach($headCount as $item)
                <option  value="{{ $item->id }}" >{{ $item->Num }} {{ $item->Name }} @if($item->TradeRol ) {{ $item->TradeRol->Name }} 
                @if($item->TradeRol->Program ) 
                {{ $item->TradeRol->Program->Name }} 
                @endif 
                @endif </option>
            @endforeach
            
            </select>        
            {!! $errors->first('head_count_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('ROI1') ? 'has-error' : ''}}">
    <label for="ROI1" class="col-md-4 control-label">{{ 'ROI 1 - Satisfaction Survey' }}</label>
    <div class="col-md-6">
        <input class="form-control" min=0 max=5  step=".1" name="ROI1" type="number" id="ROI1" value="{{ $trainingPlanHeadCount->ROI1 or ''}}" >
        {!! $errors->first('ROI1', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('ROI2') ? 'has-error' : ''}}">
    <label for="ROI2" class="col-md-4 control-label">{{ 'ROI 2 - Score' }}</label>
    <div class="col-md-6">
        <input class="form-control" min=0 max=5 step=".1" name="ROI2" type="number" id="ROI2" value="{{ $trainingPlanHeadCount->ROI2 or ''}}" >
        {!! $errors->first('ROI2', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@isset($trainingPlanHeadCount->Assistance)

<div class="form-group {{ $errors->has('Assistance') ? 'has-error' : ''}}">
    <label for="Assistance" class="col-md-4 control-label">{{ 'Assistance' }}</label>
    <div class="col-md-6">

    @if($trainingPlanHeadCount->Assistance)
    <input type="checkbox"  checked data-color="#f96262" name="Assistance" id="Assistance" value="{{ $trainingPlanHeadCount->Assistance or '0'}}"/>
    @else
    <input type="checkbox"  data-color="#f96262" name="Assistance" id="Assistance" value="{{ $trainingPlanHeadCount->Assistance or '0'}}"/>
    @endif
            {!! $errors->first('Assistance', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endisset                

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>


@push('js')
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>

    
    <script>
        $(document).ready(function () {

            @if(\Session::has('message'))
            $.toast({
                heading: 'Success!',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3000,
                stack: 6
            });
            @endif

            $(".select2").select2();

        })

    </script>

@endpush

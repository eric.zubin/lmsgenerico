@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                <h3> Training Reigstrer</h3>
                <h3> {{ $training->Training->Code }} {{ $training->Training->Name }} {{ $training->FechaInicio }} {{ $training->FechaFin }} </h3>

             <a class="btn btn-success pull-right" href="{{ url('/training_reigstrer_headcount/create') }}/{{ $training->id }}"><i
                                    class="icon-plus"></i> Add Person</a>


                    <hr>
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif



                    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
              
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th><th>Company</th>
                                
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($partes as $item)
                                <tr>
                                    <td>{{ $loop->iteration or $item->id }}</td>
                                    <td>{{ $item->HeadCount->Name }}</td>
                                    <td>{{ $item->HeadCount->Company }}</td>

                                    <td>
                                    
                                            <a href="{{ url('/training_reigstrer/editar/' . $item->id ) }}"
                                               title="Participantes">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> Acciones
                                                </button>
                                            </a>                                   

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

            

@push('js')
    <script src="{{asset('plugins/components/calendar/jquery-ui.min.js')}}"></script>
    <script src="{{asset('plugins/components/moment/moment.js')}}"></script>


    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function () {

            @if(\Session::has('message'))
            $.toast({
                heading: 'Success!',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3000,
                stack: 6
            });
            @endif
        })

        $(function () {
            $('#myTable').DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }]
            });
         

        });
        
    </script>
   <style>
    #myTable_paginate{
        background-color: white !important;
    }
    </style>
@endpush

                </div>
            </div>
        </div>
    </div>
@endsection

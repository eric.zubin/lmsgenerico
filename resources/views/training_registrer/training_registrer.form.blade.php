<h3> {{ $training->Training->Code }} {{ $training->Training->Name }} {{ $training->FechaInicio }} {{ $training->FechaFin }} </h3>


    <div class="col-md-6">
        <input class="form-control" name="trading_plan_id" type="hidden" id="trading_plan_id" value="{{ $training->id }}" >
    </div>




<div class="row">

<div class="form-group {{ $errors->has('head_count_id') ? 'has-error' : ''}}">
    <label for="head_count_id" class="control-label">{{ 'Employed' }}</label>
    <div class="col-md-6">
    
            <select class="form-control" name="head_count_id" id="head_count_id"  value="{{ $training->head_count_id or ''}}">
            @isset($training->head_count_id)
            <option selected disabled hidden value="$headCount>id">{{ $headCount->Name }}</option>
            @endisset
            @foreach($headCount as $item)
            <option value="{{ $item->id }}" >{{ $item->Name }}</option>
            @endforeach
               
            </select>
        {!! $errors->first('Company', '<p class="help-block">:message</p>') !!}
    </div>
</div>

</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>


@push('js')
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>

    
    <script>
        $(document).ready(function () {

            @if(\Session::has('message'))
            $.toast({
                heading: 'Success!',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3000,
                stack: 6
            });
            @endif
        })

    </script>

@endpush

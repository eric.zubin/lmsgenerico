@push('css')
    <link href="{{asset('plugins/components/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />

@endpush

<div class="form-group {{ $errors->has('Training_id') ? 'has-error' : ''}}">

    <label for="Training_id" class="col-md-4 control-label">{{ 'Traiding' }}</label>
                        <div class="col-md-6 ">
                            <select class="form-control select2" name="Training_id" id="Training_id" value="{{ $trainingplan->Training_id or ''}}">
                                @isset($trainingplan->Training_id)
                                <option selected hidden value="{{$trainingplan->Training_id}}">{{ $item->Training_id }} {{ $trainingplan->Training->Code }} {{ $trainingplan->Training->Name }}  Fecha: {{ $trainingplan->Training->FechaInicio }}  , {{ $trainingplan->Training->FechaFin }}  </option>
                                @endisset
                                @foreach($TrainingPlan as $item)
                                    <option value="{{ $item->id }}">{{ $item->id }} {{ $item->Code }} {{ $item->Training->Name }}  Fecha: {{ $item->FechaInicio }}  , {{ $item->FechaFin }} </option>
                                @endforeach
                            </select>
                        </div>
        {!! $errors->first('Traiding', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('Instructor_id') ? 'has-error' : ''}}">
        <label for="Instructor_id" class="col-md-4 control-label">{{ 'Instructor_id' }}</label>
        <div class="col-md-6">
                <select class="form-control" name="Instructor_id" id="Instructor_id" value="{{ $workstation->Instructor_id or ''}}">
                </select>
            {!! $errors->first('Instructor_id', '<p class="help-block">:message</p>') !!}
        </div>  
</div>

<div class="form-group {{ $errors->has('FechaInicio') ? 'has-error' : ''}}">
    <label for="FechaInicio" class="col-md-4 control-label">{{ 'Date Init' }}</label>
    <div class="col-md-6">
        @isset($trainingplan->FechaInicio)
        <input class="form-control" name="FechaInicio" type="date" id="FechaInicio" value="{{ Carbon\Carbon::parse($trainingplan->FechaInicio)->format('Y-m-d')}}" >
        @endisset
        @empty($trainingplan->FechaInicio)
        <input class="form-control" name="FechaInicio" type="date" id="FechaInicio" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" >
        @endisset
        {!! $errors->first('FechaInicio', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('FechaFin') ? 'has-error' : ''}}">
    <label for="FechaFin" class="col-md-4 control-label">{{ 'Date end' }}</label>
    <div class="col-md-6">
        @isset($trainingplan->FechaFin)
        <input class="form-control" name="FechaFin" type="date" id="FechaFin" value="{{ Carbon\Carbon::parse($trainingplan->FechaFin)->format('Y-m-d')}}" >
        @endisset
        @empty($trainingplan->FechaFin)
        <input class="form-control" name="FechaFin" type="date" id="FechaFin" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" >
        @endisset
        {!! $errors->first('FechaFin', '<p class="help-block">:message</p>') !!}
    </div>
        {!! $errors->first('FechaFin', '<p class="help-block">:message</p>') !!}
    </div>


<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-4 control-label">{{ 'Status' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="status" type="text" id="status" value="{{ $trainingplan->status or ''}}" >
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<!--
<div class="form-group {{ $errors->has('ROI1') ? 'has-error' : ''}}">
    <label for="ROI1" class="col-md-4 control-label">{{ 'ROI 1' }}</label>
    <div class="col-md-6">
        <input class="form-control" min="1" max="4"  name="ROI1" type="number" id="ROI1" value="{{ $trainingplan->ROI1 or ''}}" >
        {!! $errors->first('ROI1', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('ROI2') ? 'has-error' : ''}}">
    <label for="ROI2" class="col-md-4 control-label">{{ 'ROI 2' }}</label>
    <div class="col-md-6">
        <input class="form-control" min="1" max="4"  name="ROI2" type="number" id="ROI2" value="{{ $trainingplan->ROI2 or ''}}" >
        {!! $errors->first('ROI2', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('ROI3') ? 'has-error' : ''}}">
    <label for="ROI3" class="col-md-4 control-label">{{ 'ROI 3' }}</label>
    <div class="col-md-6">
        <input class="form-control" min="1" max="4"  name="ROI3" type="number" id="ROI3" value="{{ $trainingplan->ROI3 or ''}}" >
        {!! $errors->first('ROI3', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('ROI4') ? 'has-error' : ''}}">
    <label for="ROI4" class="col-md-4 control-label">{{ 'ROI 4' }}</label>
    <div class="col-md-6">
        <input class="form-control" min="1" max="4" name="ROI4" type="number" id="ROI4" value="{{ $trainingplan->ROI4 or ''}}" >
        {!! $errors->first('ROI4', '<p class="help-block">:message</p>') !!}
    </div>
</div>-->

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>



@push('js')
<script src="{{asset('plugins/components/switchery/dist/switchery.min.js')}}"></script>
<script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
         
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

        });
    </script>

<script>
      jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });




         


             $("#Training_id").change(function () {
                                $("#Training_id option:selected").each(function () {
                                    var id = $("#Training_id").val();
                              
                                        $.ajax({
                                            url: "/instructores/"+id,
                                            type: "post",
                                            dataType: 'json',
                                            data: { "_token": "{{ csrf_token() }}"} ,
                                            success: function (response) {
                                                console.log(response);
                                                removeOptions(document.getElementById("Instructor_id"));

                                                var x = document.getElementById("Instructor_id");

                                                for(var item in response) {
                                                    var option = document.createElement("option");
                                                    option.text = response[item].Num + " " + response[item].Name + " / " + response[item].company_instructor.Name ;
                                                    option.value = response[item].id;
                                                    x.add(option); 
                                                    console.log(response[item].Num);
                                                }
                                                            //$('#Instructor_id').empty().append( response.html);
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }
                                        });
                                });
                });

                var id = $("#Training_id").val();
             
                $.ajax({
                    url: "/instructores/"+id,
                    type: "post",
                    data: { "_token": "{{ csrf_token() }}"} ,
                    success: function (response) {
                        console.log(response);
                        var x = document.getElementById("Instructor_id");
                        //$('#Instructor_id').empty().append( response.html);

                                        for(var item in response) {
                                            var option = document.createElement("option");
                                                    option.text = response[item].Num + " " + response[item].Name + " " + response[item].company_instructor.Name ;
                                                    option.value = response[item].id;
                                                    x.add(option); 
                                                }
                                                $("#Instructor_id").val({{ $workstation->Instructor_id or '-1'}});
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            }


                });

        });

        function removeOptions(selectbox)
{
    var i;
    for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
    {
        selectbox.remove(i);
    }
}
</script>
@endpush
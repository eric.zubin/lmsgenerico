@push('css')
    <link href="{{asset('plugins/components/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />

@endpush


<div class="form-group {{ $errors->has('ProductionUnit_id') ? 'has-error' : ''}}">

    <label for="ProductionUnit_id" class="col-md-4 control-label">{{ 'Production Unit' }}</label>
                        <div class="col-md-6 ">
                            <select class="form-control" name="ProductionUnit_id" id="ProductionUnit_id" value="{{ $trainingplan->ProductionUnit_id or ''}}">
                                @isset($trainingplan->ProductionUnit_id)
                                <option selected hidden value="{{$trainingplan->ProductionUnit_id}}">{{ $trainingplan->ProductionUnit->Name }}</option>
                                @endisset
                                @foreach($productionUnits as $item)
                                    <option value="{{ $item->id }}">{{ $item->Name }}</option>
                                @endforeach
                            </select>
                        </div>
        {!! $errors->first('ProductionUnit_id', '<p class="help-block">:message</p>') !!}
</div>
    <div class="form-group {{ $errors->has('Program_id') ? 'has-error' : ''}}">
        <label for="Program_id" class="col-md-4 control-label">{{ 'Area' }}</label>
        <div class="col-md-6">
                <select class="form-control" name="Program_id" id="Program_id" value="{{ $trainingplan->Program_id or ''}}">
                </select>
            {!! $errors->first('Program_id', '<p class="help-block">:message</p>') !!}
        </div>  
    </div>




<div class="form-group {{ $errors->has('Code') ? 'has-error' : ''}}">
    <label for="Code" class="col-md-4 control-label">{{ 'ATP Code' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Code" type="text" id="Code" value="{{ $trainingplan->Code or ''}}" >
        {!! $errors->first('Code', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('Training') ? 'has-error' : ''}}">
    <label for="Training" class="col-md-4 control-label">{{ 'Training' }}</label>
    <div class="col-md-6">


        <select class="form-control" name="Training_id" id="Training_id" value="{{ $trainingplan->Training_id or ''}}">
                @isset($trainingplan->Training_id)
                     <option selected hidden value="{{$trainingplan->Training_id}}">{{ $trainingplan->Training->Code }}_{{$trainingplan->Training->Name}}</option>
                @endisset
                @foreach($training as $item)
                    <option value="{{ $item->id }}">{{ $item->Code }}_{{ $item->Name }}</option>
                @endforeach
               
            </select>
        {!! $errors->first('Training', '<p class="help-block">:message</p>') !!}

    </div>
</div><div class="form-group {{ $errors->has('FechaInicio') ? 'has-error' : ''}}">
    <label for="FechaInicio" class="col-md-4 control-label">{{ 'Date Init' }}</label>
    <div class="col-md-6">
        @isset($trainingplan->FechaInicio)
        <input class="form-control" name="FechaInicio" type="date" id="FechaInicio" value="{{ Carbon\Carbon::parse($trainingplan->FechaInicio)->format('Y-m-d')}}" >
        @endisset
        @empty($trainingplan->FechaInicio)
        <input class="form-control" name="FechaInicio" type="date" id="FechaInicio" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" >
        @endisset
        {!! $errors->first('FechaInicio', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('FechaFin') ? 'has-error' : ''}}">
    <label for="FechaFin" class="col-md-4 control-label">{{ 'Date end' }}</label>
    <div class="col-md-6">
        @isset($trainingplan->FechaFin)
        <input class="form-control" name="FechaFin" type="date" id="FechaFin" value="{{ Carbon\Carbon::parse($trainingplan->FechaFin)->format('Y-m-d')}}" >
        @endisset
        @empty($trainingplan->FechaFin)
        <input class="form-control" name="FechaFin" type="date" id="FechaFin" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" >
        @endisset
        {!! $errors->first('FechaFin', '<p class="help-block">:message</p>') !!}
    </div>
        {!! $errors->first('FechaFin', '<p class="help-block">:message</p>') !!}
    </div>





<div class="form-group {{ $errors->has('HRS') ? 'has-error' : ''}}">
    <label for="HRS" class="col-md-4 control-label">{{ 'HRS' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="HRS" type="number" id="HRS" value="{{ $trainingplan->HRS or ''}}" >
        {!! $errors->first('HRS', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('NUM_EMPLOYEES') ? 'has-error' : ''}}">
    <label for="NUM_EMPLOYEES" class="col-md-4 control-label">{{ 'NUM EMPLOYEES' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="NUM_EMPLOYEES" type="number s" id="NUM_EMPLOYEES" value="{{ $trainingplan->NUM_EMPLOYEES or ''}}" >
        {!! $errors->first('NUM_EMPLOYEES', '<p class="help-block">:message</p>') !!}
    </div>
</div> 

<div class="form-group {{ $errors->has('COSTOPOR_PER') ? 'has-error' : ''}}">
    <label for="COSTOPOR_PER" class="col-md-4 control-label">{{ 'COST HEADCOUNT' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="COSTOPOR_PER" type="text" id="COSTOPOR_PER" value="{{ $trainingplan->COSTOPOR_PER or ''}}" >
        {!! $errors->first('COSTOPOR_PER', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>



@push('js')
<script src="{{asset('plugins/components/switchery/dist/switchery.min.js')}}"></script>
<script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
         
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

        });
    </script>

<script>
      jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
                         
             $("#ProductionUnit_id").change(function () {
                                $("#ProductionUnit_id option:selected").each(function () {
                                    var id = $("#ProductionUnit_id").val();

                                        $.ajax({
                                            url: "/select_program/"+id,
                                            type: "post",
                                            dataType: 'json',
                                            data: { "_token": "{{ csrf_token() }}"} ,
                                            success: function (response) {
                                                            $('#Program_id').empty().append( response.html);
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }


                                        });

                                });
                })


                var id = $("#ProductionUnit_id").val();
                $.ajax({
                    url: "/select_program/"+id,
                    type: "post",
                    data: { "_token": "{{ csrf_token() }}"} ,
                    success: function (response) {
                                    $('#Program_id').empty().append( response.html);
                                    @isset($workstation->Program_id)
                                    $("#Program_id").val({{$workstation->Program_id}});
                                    @endisset
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            }


                });

        });

</script>
@endpush
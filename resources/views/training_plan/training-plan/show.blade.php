@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Training Plan {{ $trainingplan->id }}</h3>
                    @can('view-'.str_slug('TrainingPlan'))
                        <a class="btn btn-success pull-right" href="{{ url('/training_plan/training-plan') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>IATP CodeD</th>
                                <td>{{ $trainingplan->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $trainingplan->Code }} </td></tr><tr>
                            <tr><th> Area </th><td> 
                            @if($trainingplan->Program != null)
                                    {{ $trainingplan->Program->Name }}
                                    @endif
                            </td></tr><tr>

                         
                            <th> Training </th><td> {{ $trainingplan->Training->Name  }} </td></tr>
                            <tr><th> Date Init </th><td> {{ $trainingplan->FechaInicio }} </td></tr>
                            <tr><th> Date Endn </th><td> {{ $trainingplan->FechaFin }} </td></tr>
                            
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@extends('layouts.master')

@push('css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/components/calendar/dist/fullcalendar.css')}}" rel="stylesheet" />

@endpush

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Training Plan</h3>
                    @can('add-'.str_slug('TrainingPlan'))
                        <a class="btn btn-success pull-right" href="{{ url('/training_plan/training-plan/create') }}"><i
                                    class="icon-plus"></i> Add Training Plan</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>ATP Code</th><th>Training</th>
                                <th>Area</th>
                             
                                <th>Date Init</th>
                                <th>Date End</th>
                                <th>HRS</th>
                                <th>NUM_EMPLOYEES</th>
                                <th>COST HEADCOUNT</th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trainingplan as $item)
                                <tr>
                                    <td>{{ $loop->iteration or $item->id }}</td>
                                    <td>{{ $item->Code }}</td>
                                    <td>
                                    @if($item->Training != null)
                                    {{ $item->Training->Name }}
                                    @endif
                                    </td>
                                    <td>
                                    @if($item->Program != null)
                                    {{ $item->Program->Name }}
                                    @endif
                                    </td>
                                    <td>{{ $item->FechaInicio }}</td>
                                    <td>{{ $item->FechaFin }}</td>
                                    <td>{{ $item->HRS }}</td>
                                    <td>{{ $item->NUM_EMPLOYEES }}</td>
                                    <td>{{ $item->COSTOPOR_PER }}</td>

                                    <td>
                                        @can('view-'.str_slug('TrainingPlan'))
                                            <a href="{{ url('/training_plan/training-plan/' . $item->id) }}"
                                               title="View TrainingPlan">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View
                                                </button>
                                            </a>
                                        @endcan

                                        @can('edit-'.str_slug('TrainingPlan'))
                                            <a href="{{ url('/training_plan/training-plan/' . $item->id . '/edit') }}"
                                               title="Edit TrainingPlan">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                </button>
                                            </a>
                                        @endcan

                                        @can('delete-'.str_slug('TrainingPlan'))
                                            <form method="POST"
                                                  action="{{ url('/training_plan/training-plan' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Delete TrainingPlan"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                </button>
                                            </form>
                                        @endcan


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $trainingplan->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
                <div class="white-box">
                    <div id="calendar"></div>
                </div>
            </div>
            
@endsection

@push('js')
    <script src="{{asset('plugins/components/calendar/jquery-ui.min.js')}}"></script>
    <script src="{{asset('plugins/components/moment/moment.js')}}"></script>
    <script src="{{asset('plugins/components/calendar/dist/fullcalendar.min.js')}}"></script>
    <script src="{{asset('plugins/components/calendar/dist/trading_plan.fullcalendar.js')}}"></script>


    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->

    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>   

    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function () {

            @if(\Session::has('message'))
            $.toast({
                heading: 'Success!',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3000,
                stack: 6
            });
            @endif
        })

        $(function () {
                 $('#myTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel'
        ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }]
            });
         
         

        });
        
    </script>
   <style>
    #myTable_paginate{
        background-color: white !important;
    }
    </style>
@endpush

@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Training {{ $training->id }}</h3>
                    @can('view-'.str_slug('Training'))
                        <a class="btn btn-success pull-right" href="{{ url('/training/training') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $training->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $training->Code }} </td></tr><tr>
                            <th> Name </th><td> {{ $training->Name }} </td></tr><tr>
                            <th> Type </th><td> {{ $training->Type }} </td></tr>
                            <th> Instructor </th><td>
                                @foreach($training->instructors as $item)
                                {{ $item->Name }} ,
                                @endforeach
                             </td></tr>

                             <th> Competence </th><td>
                                @foreach($training->Competives as $item)
                                {{ $item->Name }} ,
                                @endforeach
                             </td></tr>

                            <th> Level </th><td> {{ $training->Level }} </td></tr>
                            <th> Language </th><td> {{ $training->Language }} </td></tr>
                            <th> Version </th><td> {{ $training->Version }} </td></tr>

                            <th> Materiales </th><td> {{ $training->materiales }} </td></tr>
                            <th> Archivos </th><td> {{ $training->archivos }} </td></tr>

                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


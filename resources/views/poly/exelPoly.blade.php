<body id="tabla">

    <style>
    .low{
        background-color: green;
    }
    .mid{
        background-color: #b2c6df;
    }
    .high{
        background-color: #FF4C4C;
    }
    .ancho{
        width: 3400px;
    }
    .anchoP{
        width: 340px;
        display: flex;
        flex-direction: column;
        padding: 20px;

    }
    .anchoH{
        display: flex;
        overflow: scroll;
        overflow-x: scroll;
    }
    </style>


    
<table>

<thead>

<td></td>

@foreach($productionUnits as $productionUnit)   
@foreach($programs as $program)  
@if($program->ProductionUnit_id == $productionUnit->id)
<th  align="right"> <h1>{{$productionUnit->Name}}</h1></th>
<th></th>
<th></th>
<th></th>
@endif
@endforeach
@endforeach
<tr></tr>
<td></td>
@foreach($productionUnits as $productionUnit)   
@foreach($programs as $program)  
@if($program->ProductionUnit_id == $productionUnit->id)
<th><h3>{{$program->Name}}</h3></th>
<th></th>
<th></th>
<th></th>
@endif
@endforeach
@endforeach
<tr></tr>
<tr></tr>
</thead>




<tbody>

@foreach($productionUnits as $productionUnit)   

<td> 
@foreach($programs as $program)   
@if($program->ProductionUnit_id == $productionUnit->id)

<td>

            @foreach($results as $item)
            <table>

            @if($program->id == $item->Programs_Id)

                @if($item->NivelDeRiesgoOp == "LOW")
                <tr> <h2 class="box-title low">{{ $item->Wwork_stationsName}}</h2></tr>
                @endif
                @if($item->NivelDeRiesgoOp == "MID")
                <tr><h2 class="box-title mid">{{ $item->Wwork_stationsName}}</h2></tr>
                @endif
                @if($item->NivelDeRiesgoOp == "HIGH")
                <tr> <h2 class="box-title high">{{ $item->Wwork_stationsName}}</h2></tr>
                @endif
                            <tr>
              
                                <td>People Certificate</td>
                                <td>Poly >=3 people certified</td>

                            </tr>
                       
                            <tr>
                                <td>{{ $item->C}}</td>
                                <td>
                                @if($item->C >= 3)
                                1
                                @else
                                0
                                @endif
                                </td>
                    
                                

                            </tr>

                        
                     

            @endif
            </table>

            @endforeach



            
            @foreach($poly3Certificados as $item)
            @if($program->id == $item->Programs_id)
           
            <table class="table table-bordered">
                            <thead>
                                <tr> <h4>Poly 3X3 Work Stations Certified with 3 employees</h4> </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $item->total}}</td>
                                <td>
                                {{ $item->poly}}
                                </td>

                            </tr>
                            
                            </tbody>
            </table>

            <table class="table table-bordered center">
                            <thead>
              
                            </thead>
                            <tbody>
                            <tr>
                            <td 
                                class="@if($item->poly * 100 / $item->total >= 90 && $item->poly * 100 / $item->total<= 100) 
                                verde
                                @endif 
                                @if($item->poly * 100 / $item->total >= 70 && $item->poly * 100 / $item->total < 90) 
                                amarillo
                                @endif 
                                @if($item->poly * 100 / $item->total < 70) 
                                rojo
                                @endif">
                                {{ round($item->poly * 100 / $item->total,2) }} %
                                </td>

                            </tr> 
                            
                            </tbody>
            </table>

            @endif
            @endforeach
            @foreach($poly3CertificadosPeople as $polu)
            @if($program->id == $polu->Program_id)
            
            <table class="table table-bordered">
                            <thead>
                            <tr><h4>Poly 3X3 People Certified in minimum 3 Work Stations</h4></tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $polu->totalP}}</td>
                                <td>{{ $polu->headcountTotal}}  </td>

                            </tr>
                            </tbody>
            </table>

            <table class="table table-bordered center">
                            <thead>
              
                            </thead>
                            <tbody>
                            <tr>
                                <td 
                                class="@if($polu->totalP * 100 / $polu->headcountTotal >= 90 && $polu->totalP * 100 / $polu->headcountTotal<= 100) 
                                verde
                                @endif 
                                @if($polu->totalP * 100 / $polu->headcountTotal >= 70 && $polu->totalP * 100 / $polu->headcountTotal < 90) 
                                amarillo
                                @endif 
                                @if($polu->totalP * 100 / $polu->headcountTotal < 70) 
                                rojo
                                @endif">
                                {{ round($polu->totalP * 100 / $polu->headcountTotal,2) }} %
                                </td>

                            </tr> 
                            
                            </tbody>
            </table>


            @endif
            @endforeach




            
</td>
<!-- Espacios a la derecha-->
<td></td>
<td></td>
<td></td>

@endif

@endforeach

</td>


@endforeach

</tbody>


</table>

<table>


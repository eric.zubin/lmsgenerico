@extends('layouts.master')

@push('css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
    <style>
    .low{
        background-color: green;
        color: white;
    }
    .mid{
        background-color: blue;
        color: white;
    }
    .high{
        background-color: red;
        color: white;
    }
    .ancho{
        width: 3400px;
    }
    .anchoP{
        width: 340px;
        display: flex;
        flex-direction: column;
        padding: 20px;

    }
    .anchoH{
        display: flex;
    }
    .anchoscroll{
        overflow: scroll;
        overflow-x: scroll;
    }
    .verde{
        background-color: green;
        color: white;
    }
    .rojo{
        background-color: red;
        color: white;
    }
    .amarillo{
        background-color: yellow;
        color: black;
    }
    </style>
@endpush



@section('content')
    <div class="">
        <!-- .row 

        <div class="">
            <div class="">
                <div class="white-box">
                    <h3 class="box-title pull-left">SKILLS BOARD – PEOPLE CERTIFIED</h3>
                    <a href="/poly/exel" class="btn btn-primary"  target="_blank">Export Exel Poly</a> 
                    <a href="/poly/exelPoly" class="btn btn-primary"  target="_blank">Export Exel Poly Certificado</a> 
       


                    <div class="">
<div class="form-group {{ $errors->has('ProductionUnit_id') ? 'has-error' : ''}}">

<label for="ProductionUnit_id" class="col-md-4 control-label">{{ 'Production Unit' }}</label>
                <div class="col-md-6 ">
                    <select class="form-control" name="ProductionUnit_id" id="ProductionUnit_id" value="{{ $workstation->ProductionUnit_id or ''}}">
                            @isset($program->ProductionUnit_id)
                            <option selected hidden value="{{$program->ProductionUnit_id}}">{{ $program->ProductionUnit->Name }}</option>
                            @endisset
                            @foreach($productionUnits as $item)
                            <option value="{{ $item->id }}">{{ $item->Name }}</option>
                            @endforeach
                    </select>
                </div>
{!! $errors->first('ProductionUnit_id', '<p class="help-block">:message</p>') !!}
</div>
<br>
<div class="form-group {{ $errors->has('Program_id') ? 'has-error' : ''}}">
<label for="Program_id" class="col-md-4 control-label">{{ 'Program_id' }}</label>
<div class="col-md-6">
        <select class="form-control" name="Program_id" id="Program_id" value="{{ $workstation->Program_id or ''}}">
        </select>
    {!! $errors->first('Program_id', '<p class="help-block">:message</p>') !!}
</div>  
</div>


</div>
</div>-->
</div>

<br>
<br>

<a href="/poly/exelPoly" class="btn btn-primary"  target="_blank">
Export Exel
</a> 


<h1>POLY POLY / WS PEOPLE CERTIFIED [WS COVERED]</h1>


<table class="table table-bordered">
                    
                            </thead>
                            <tbody>
                                <tr>
                                   <td><h3> ASSEMBLY COUNT</h3> </td>
                                    <td>{{ $polyProductionUnit_id[0]->C }} </td>
                                    <td> <h3> ASSEMBLY NEED</h3></td>
                                    <td>{{ $polyProductionUnit_id[0]->N }} </td>
                                </tr>

                                <tr>
                                    <td> <h3>SINGLE PARTS COUNT</h3></td>
                                    <td>{{ $polyProductionUnit_id[1]->C }} </td>
                                    <td> <h3>SINGLE PARTS NEED</h3></td>
                                    <td>{{  $polyProductionUnit_id[1]->N }} </td>
                                </tr>

                                <tr>
                                    <td> <h3>General COUNT</h3></td>
                                    <td>{{ $polyProductionUnGenral[0]->C }} </td>
                                    <td> <h3>General NEED</h3></td>
                                    <td>{{ $polyProductionUnGenral[0]->N }} </td>
                                </tr>
                            </tbody>

    </table>



<table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> <h2> ASSEMBLY</h2></th>
                                <th> <h2>SINGLE PARTS</h2></th>
                                <th> <h2>General</h2></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ round($polyProductionUnit_id[0]->C * 100 / $polyProductionUnit_id[0]->N,2) }} %</td>
                                <td>{{ round($polyProductionUnit_id[1]->C * 100 / $polyProductionUnit_id[1]->N,2) }} %</td>
                                <td>{{ round($polyProductionUnGenral[0]->C * 100 / $polyProductionUnGenral[0]->N,2) }} %</td>
                            <tr>
                            </tbody>

    </table>


<div class="anchoscroll">
<div class="anchoH">

@foreach($productionUnits as $productionUnit)   
<h2>{{$productionUnit->Name}}</h2>

<div class="anchoH">
@foreach($programs as $program)   
@if($program->ProductionUnit_id == $productionUnit->id)
<div class="anchoP">

<h4>{{$program->Code}}</h4>

@foreach($results as $item)
@if($program->id ==  $item->Programs_id)
            <div class="">
            <div class="white-box">
                @if($item->NivelDeRiesgoOp == "LOW")
                <h3 class="box-title low">{{ $item->Wwork_stationsName}}</h3>
                @endif
                @if($item->NivelDeRiesgoOp == "MID")
                <h3 class="box-title mid">{{ $item->Wwork_stationsName}}</h3>
                @endif
                @if($item->NivelDeRiesgoOp == "HIGH")
                <h3 class="box-title high">{{ $item->Wwork_stationsName}}</h3>
                @endif
                    <div class="">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>N</th>
                                <th>C</th>
                                <th>M</th>
                                <th>T</th>
                                <th>NR</th>


                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $item->N}}</td>
                             
                                <td>
                               
                                
                                @if($item->C != 0 )
                                    @if($item->C <= $item->N)
                                    {{ $item->C}}
                                    @else
                                    {{$item->N}}                        
                                    @endif
                                @else
                                0
                                @endif
                                </td>

                                <td @if($item->N-$item->C > 0) class="high" @endif> 
                                @if($item->N-$item->C > 0) 
                                {{$item->N-$item->C}} 
                                @else 0 
                                @endif 
                                </td>
                                <td 
                                @if($item->N-$item->C > 0) 
                                @if($item->T == $item->N-$item->C ) class="verde" @endif    
                                @if($item->T > $item->N-$item->C ) class="amarillo" @endif  
                                @if($item->T < $item->N-$item->C ) class="rojo" @endif  

                                @else 
                                
                                @if($item->T == 0 ) class="verde" @endif    
                                @if($item->T > 0 ) class="amarillo" @endif  
                                @if($item->T < 0 ) class="rojo" @endif  

                                @endif 


                       
                                
                                
                                <td>{{ $item->T}}</td>
                                
                                <td>{{ $item->R}}</td>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
            @foreach($poly as $item)
            @if($program->id == $item->Programs_Id)
            <h6>Totales</h6>
            <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>N</th>
                                <th>C</th>
                                <th>M</th>
                                <th>T</th>


                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $item->N}}</td>
                                <td>
                                {{ $item->C}}
                                </td>
                                <td> 
                                {{ $item->M}}
                                </td>
                                <td>{{ $item->T}}</td>
                                

                            </tr>
                            </tbody>
            </table>
     
            <h6>Advance</h6>
            <table class="table table-bordered">
                            <tbody>
                            <tr   class="@if($item->C * 100 / $item->N >= 90 && $item->C * 100 / $item->N<= 100) 
                                verde
                                @endif 
                                @if($item->C * 100 / $item->N >= 70 && $item->C * 100 / $item->N < 90) 
                                amarillo
                                @endif 
                                @if($item->C * 100 / $item->N < 70) 
                                rojo
                                @endif">
                                <td>{{ round($item->C * 100 / $item->N,2) }} %</td>
                            </tr>
                            </tbody>
            </table>


            @endif
            @endforeach


</div>
@endif

@endforeach
</div>

@endforeach
</div>

</div>





<h1>POLY-POLY/3X3 (3 Employees certified by WS X People certified in 3 Work Stations</h1>


<table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> <h2> Poly 3X3 Work Stations Certified with 3 employees</h2></th>
                                <th> <h2> ASSEMBLY</h2></th>
                                <th> <h2>SINGLE PARTS</h2></th>
                                <th> <h2>General</h2></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td> {{ round($PolyWorkStUnit_id[0]->poly * 100 / $PolyWorkStUnit_id[0]->total,2) }} %</td>
                                <td> {{ round($PolyWorkStUnit_id[1]->poly * 100 / $PolyWorkStUnit_id[1]->total,2) }} %</td>
                                <td> {{ round($PolyWorkStatTotal[0]->poly * 100 / $PolyWorkStatTotal[0]->total,2) }} %</td>
                            <tr>
                            </tbody>

    </table>
    

    <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> <h2> Poly 3X3 Work Stations Certified with 3 employees</h2></th>
                     

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <tr>
                                <th> <h2> ASSEMBLY Poly >=3 people certified</h2></th>
                                <td> {{ $PolyWorkStUnit_id[0]->poly  }} </td>
                                <th> <h2> ASSEMBLY Workstation </h2></th>
                                <td> {{ $PolyWorkStUnit_id[0]->total }} </td>
                                </tr>

                                <tr>
                                <td> <h2>SINGLE PARTS Poly >=3 people certified</h2></td>
                                <td> {{ $PolyWorkStUnit_id[1]->poly   }} </td>
                                <td> <h2>SINGLE PARTS Workstation</h2></td>
                                <td> {{  $PolyWorkStUnit_id[1]->total }} </td>
                                </tr>

                                <tr>
                                <th> <h2>General Poly >=3 people certified</h2></th>
                                <td> {{ $PolyWorkStatTotal[0]->poly   }} </td>
                                <td> <h2>General Workstation</h2></td>
                                <td> {{ $PolyWorkStatTotal[0]->total }} </td>
                                </tr>


                            <tr>
                            </tbody>

    </table>



    <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> <h2> People Certified in minimum 3 Work Stations</h2></th>
                                <th> <h2> ASSEMBLY</h2></th>
                                <th> <h2>SINGLE PARTS</h2></th>
                                <th> <h2>General</h2></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td> {{ round($PolyWorkStUnitPeople_id[0]->poly * 100 / $PolyWorkStUnitPeople_id[0]->headcountTotal,2) }} %</td>
                                <td> {{ round($PolyWorkStUnitPeople_id[1]->poly * 100 / $PolyWorkStUnitPeople_id[1]->headcountTotal,2) }} %</td>
                                <td> {{ round($PolyWorkStatPeopleTotal[0]->poly * 100 / $PolyWorkStatPeopleTotal[0]->headcountTotal,2) }} %</td>
                            <tr>
                            </tbody>

    </table>


    <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th> <h2> People Certified in minimum 3 Work Stations</h2></th>
                      
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <tr>
                                <th> <h3> ASSEMBLY</h3></th>
                                <td> {{  $PolyWorkStUnitPeople_id[0]->poly }}</td>
                                <th> <h3> ASSEMBLY HEADCOUNT</h3></th>
                                <td> {{  $PolyWorkStUnitPeople_id[0]->headcountTotal }} %</td>
                                </tr>

                                <tr>
                                <th> <h3>SINGLE PARTS</h3></th>
                                <td> {{ $PolyWorkStUnitPeople_id[1]->poly  }} </td>
                                <th> <h3>SINGLE PARTS HEADCOUNT</h3></th>
                                <td> {{ $PolyWorkStUnitPeople_id[1]->headcountTotal }} </td>
                                </tr>

                                <tr>
                                <th> <h3>General</h3></th>
                                <td> {{  $PolyWorkStatPeopleTotal[0]->poly }} </td>
                                <th> <h3>General HEADCOUNT</h3></th>
                                <td> {{  $PolyWorkStatPeopleTotal[0]->headcountTotal }} </td>
                                </tr>

                            <tr>
                            </tbody>

    </table>




<div class="anchoscroll">
<div class="anchoH">

@foreach($productionUnits as $productionUnit)   
<h2>{{$productionUnit->Name}}</h2>

<div class="anchoH">
@foreach($programs as $program)   
@if($program->ProductionUnit_id == $productionUnit->id)
<div class="anchoP">

<h4>{{$program->Code}}</h4>

@foreach($results as $item)
@if($program->id == 2)
            <div class="">
            <div class="white-box">
                @if($item->NivelDeRiesgoOp == "LOW")
                <h3 class="box-title low">{{ $item->Wwork_stationsName}}</h3>
                @endif
                @if($item->NivelDeRiesgoOp == "MID")
                <h3 class="box-title mid">{{ $item->Wwork_stationsName}}</h3>
                @endif
                @if($item->NivelDeRiesgoOp == "HIGH")
                <h3 class="box-title high">{{ $item->Wwork_stationsName}}</h3>
                @endif
                    <div class="">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>People Certificate</th>
                                <th>Poly >=3 people certified</th>
                      
                           

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $item->C}}</td>
                                <td>
                                @if($item->C >= 3)
                                1
                                @else
                                0
                                @endif
                                </td>
                    
                                

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
            @foreach($poly3Certificados as $item)
            @if($program->id == 0)
            <h6>Poly 3X3 Work Stations Certified with 3 employees</h6>
            <table class="table table-bordered">
                            <thead>
              
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $item->total}}</td>
                                <td>
                                {{ $item->poly}}
                                </td>

                            </tr>
                            
                            </tbody>
            </table>

            <table class="table table-bordered center">
                            <thead>
              
                            </thead>
                            <tbody>
                            <tr>
                            <td 
                                class="@if($item->poly * 100 / $item->total >= 90 && $item->poly * 100 / $item->total<= 100) 
                                verde
                                @endif 
                                @if($item->poly * 100 / $item->total >= 70 && $item->poly * 100 / $item->total < 90) 
                                amarillo
                                @endif 
                                @if($item->poly * 100 / $item->total < 70) 
                                rojo
                                @endif">
                                {{ round($item->poly * 100 / $item->total,2) }} %
                                </td>

                            </tr> 
                            
                            </tbody>
            </table>

            @endif
            @endforeach
            @foreach($poly3CertificadosPeople as $polu)
            @if($program->id == $polu->Program_id)
            <h6>Poly 3X3 People Certified in minimum 3 Work Stations</h6>
            <table class="table table-bordered">
                            <thead>
              
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $polu->totalP}}</td>
                                <td>{{ $polu->headcountTotal}}  </td>

                            </tr>
                            </tbody>
            </table>

            <table class="table table-bordered center">
                            <thead>
              
                            </thead>
                            <tbody>
                            <tr>
                                <td 
                                class="@if($polu->totalP * 100 / $polu->headcountTotal >= 90 && $polu->totalP * 100 / $polu->headcountTotal<= 100) 
                                verde
                                @endif 
                                @if($polu->totalP * 100 / $polu->headcountTotal >= 70 && $polu->totalP * 100 / $polu->headcountTotal < 90) 
                                amarillo
                                @endif 
                                @if($polu->totalP * 100 / $polu->headcountTotal < 70) 
                                rojo
                                @endif">
                                {{ round($polu->totalP * 100 / $polu->headcountTotal,2) }} %
                                </td>

                            </tr> 
                            
                            </tbody>
            </table>


            @endif
            @endforeach

            
</div>
@endif

@endforeach
</div>

@endforeach
</div>

</div>



              
@endsection

@push('js')
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>

    <script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <!-- end - This is for export functionality only -->
    <script>
     function myFunction(workstation,headCount) {

  
                $('#worstation_id').val(headCount);
                $('#head_count_id').val(workstation);
                $('#modal').modal('show');   

                $.ajax({
                url: "/skill_vista/"+workstation+"/"+headCount ,
                type: "get",
                data: { "_token": "{{ csrf_token() }}"} ,
                success: function (response) {

                        $.ajax({
                        url: "/skill_vista_evicencia/"+response.id,
                        type: "get",
                        data: { "_token": "{{ csrf_token() }}"} ,
                        success: function (response) {
                            document.getElementById("evidencias").innerHTML = ""; 

                                response.forEach(function(element) {
                                console.log(element.id);
                                document.getElementById("evidencias").innerHTML +=  "<a type='button' href='/evidencia/" + element.id + "' target='_blank' class='btn btn-default waves-effect'>Evidencia</a>"; 

                                });
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                        }

                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                }

            });

    }


        $(document).ready(function () {


        $("#ProductionUnit_id").change(function () {
                                $("#ProductionUnit_id option:selected").each(function () {
                                    var id = $("#ProductionUnit_id").val();

                                        $.ajax({
                                            url: "/select_program/"+id,
                                            type: "post",
                                            dataType: 'json',
                                            data: { "_token": "{{ csrf_token() }}"} ,
                                            success: function (response) {
                                                            //$('#Program_id').empty().append( response.html);
                                                            //window.location.href = "/skill_matriz_vista/"+response.programfirst.id;  // Sadly this reloads

                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }


                                        });

                                });
                })

                $("#Program_id").change(function () {
                                $("#Program_id option:selected").each(function () {
                                    var id = $("#Program_id").val();
                                    //window.location.href = "/skill_matriz_vista/"+id;  // Sadly this reloads
                                  

                                });
                })


                var id = $("#ProductionUnit_id").val();
                $.ajax({
                    url: "/select_program/"+id,
                    type: "post",
                    data: { "_token": "{{ csrf_token() }}"} ,
                    success: function (response) {
                                    $('#Program_id').empty().append( response.html);
                                    @isset($program->id)
                                    $("#Program_id").val({{$program->id}});
                                    @endisset

                                    

                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            }


                });

            })



        $(function () {
            $('#myTable').DataTable({
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }],
                fixedHeader: {
                    header: true,
                    footer: true
                }
            });

           

        });
    </script>
   <style>
    #myTable_paginate{
        background-color: white !important;
    }
    </style>
@endpush

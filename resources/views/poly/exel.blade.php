<body id="tabla">

    <style>
    .low{
        background-color: green;
    }
    .mid{
        background-color: #b2c6df;
    }
    .high{
        background-color: #FF4C4C;
    }
    .ancho{
        width: 3400px;
    }
    .anchoP{
        width: 340px;
        display: flex;
        flex-direction: column;
        padding: 20px;

    }
    .anchoH{
        display: flex;
        overflow: scroll;
        overflow-x: scroll;
    }
    </style>


    
<table>

<thead>

<td></td>

@foreach($productionUnits as $productionUnit)   
@foreach($programs as $program)  
@if($program->ProductionUnit_id == $productionUnit->id)
<th  align="right"> <h1>{{$productionUnit->Name}}</h1></th>
<th></th>
<th></th>
<th></th>
@endif
@endforeach
@endforeach
<tr></tr>
<td></td>
@foreach($productionUnits as $productionUnit)   
@foreach($programs as $program)  
@if($program->ProductionUnit_id == $productionUnit->id)
<th><h3>{{$program->Name}}</h3></th>
<th></th>
<th></th>
<th></th>
@endif
@endforeach
@endforeach
<tr></tr>
<tr></tr>
</thead>




<tbody>

@foreach($productionUnits as $productionUnit)   

<td> 
@foreach($programs as $program)   
@if($program->ProductionUnit_id == $productionUnit->id)

<td>

            @foreach($results as $item)
            <table>

            @if($program->id == $item->Programs_Id)

                @if($item->NivelDeRiesgoOp == "LOW")
                <tr> <h2 class="box-title low">{{ $item->Wwork_stationsName}}</h2></tr>
                @endif
                @if($item->NivelDeRiesgoOp == "MID")
                <tr><h2 class="box-title mid">{{ $item->Wwork_stationsName}}</h2></tr>
                @endif
                @if($item->NivelDeRiesgoOp == "HIGH")
                <tr> <h2 class="box-title high">{{ $item->Wwork_stationsName}}</h2></tr>
                @endif
                            <tr>
                                <td>N</td>
                                <td>C</td>
                                <td>M</td>
                                <td>T</td>
                                <th>NR</th>

                            </tr>
                       
                            <tr>
                                <td>{{ $item->N}}</td>
                                <td>
                                @if($item->C != 0 )
                                    @if($item->C <= $item->N)
                                    {{ $item->C}}
                                    @else
                                    {{$item->N}}                        
                                    @endif
                                @else
                                0
                                @endif
                                </td>
                                <td @if($item->N-$item->C > 0) class="high" @endif> 
                                @if($item->N-$item->C > 0) 
                                {{$item->N-$item->C}} 
                                @else 0 
                                @endif 
                                </td>
                                <td>{{ $item->T}}</td>
                                <td>{{ $item->R}}</td>


                            </tr>
                     

            @endif
            </table>

            @endforeach
</td>
<!-- Espacios a la derecha-->
<td></td>
<td></td>
<td></td>

@endif

@endforeach

</td>


@endforeach

</tbody>


</table>

<table>


@push('css')
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
    
    @endpush
<div class="form-group {{ $errors->has('Code') ? 'has-error' : ''}}">
    <label for="Code" class="col-md-4 control-label">{{ 'Code' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Code" type="text" id="Code" value="{{ $training->Code or ''}}" >
        {!! $errors->first('Code', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
    <label for="Description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Description" type="text" id="Description" value="{{ $training->Description or ''}}" >
        {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('TrainingSolution') ? 'has-error' : ''}}">
    <label for="TrainingSolution" class="col-md-4 control-label">{{ 'Training Solution' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="TrainingSolution" type="text" id="TrainingSolution" value="{{ $training->TrainingSolution or ''}}" >
        {!! $errors->first('TrainingSolution', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('TotalHours') ? 'has-error' : ''}}">
    <label for="TotalHours" class="col-md-4 control-label">{{ 'TotalHours' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="TotalHours" type="text" id="TotalHours" value="{{ $training->TotalHours or ''}}" >
        {!! $errors->first('TotalHours', '<p class="help-block">:message</p>') !!}
    </div>
</div>





<div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $training->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Type') ? 'has-error' : ''}}">
    <label for="Type" class="col-md-4 control-label">{{ 'Type' }}</label>
    <div class="col-md-6">
         <select class="form-control" name="Type" type="text" id="Type" value="{{ $training->Type or ''}}">
                @isset($training->Type)
                <option selected value="{{$training->Type}}">{{ $training->Type }}</option>
                @endisset
                <option>BASIC</option>
                <option>CERTIFICATION</option>
                <option>CULTURAL</option>
                <option>SOFT</option>
                <option>SPECIFIC</option>
                <option>FUNCIONAL</option>
        </select>
        {!! $errors->first('Type', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Instructors') ? 'has-error' : ''}}">
    <label for="Instructors" class="col-md-4 control-label">{{ 'Instructors' }}</label>
    <div class="col-md-6">
                    <select class="select2 m-b-10 select2-multiple" multiple="multiple"  multiple data-placeholder="Choose" name="Instructors[]" id="Instructors" >

              
                    <optgroup label="Choose tearchers">
                         <option value="">NA</option>
                         @isset($training->instructors)
                            @foreach($training->instructors as $item)
                            <option value="{{$item->id}}" selected="selected">{{ $item->Name }}</option>
                            @endforeach
                        @endisset
                         @foreach($instructor as $item)
                             <option value="{{ $item->id }}">{{ $item->Name }}</option>
                         @endforeach
                         </optgroup>
                     </select>
        {!! $errors->first('Instructors', '<p class="help-block">:message</p>') !!}
    </div>
    </div><div class="form-group {{ $errors->has('Competives') ? 'has-error' : ''}}">
    <label for="Competives" class="col-md-4 control-label">{{ 'Competence' }}</label>
    <div class="col-md-6">
                    <select class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Choose" name="Competives[]" id="Competives">
                        <optgroup label="Choose Competives">
                        @isset($training->Competives)
                                @foreach($training->Competives as $item)
                                <option value="{{$item->id}}" selected="selected">{{ $item->Name }}</option>
                                @endforeach
                        @endisset
                        @foreach($competives as $item)
                        
                            <option value="{{ $item->id }}">{{ $item->Name }}</option>
                        @endforeach
                        </optgroup>
                    </select>
        {!! $errors->first('Competives', '<p class="help-block">:message</p>') !!}
    </div>


</div><div class="form-group {{ $errors->has('Level') ? 'has-error' : ''}}">
    <label for="Level" class="col-md-4 control-label">{{ 'Level' }}</label>
    <div class="col-md-6">
            <select class="form-control" name="Level" id="Level" value="{{ $training->Level or ''}}">
                @isset($training->Level)
                <option selected value="{{$training->Level}}">{{ $training->Level }}</option>
                @endisset
                <option>0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>

            </select>
        {!! $errors->first('Level', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Language') ? 'has-error' : ''}}">
    <label for="Language" class="col-md-4 control-label">{{ 'Language' }}</label>
    <div class="col-md-6">
            <select class="form-control" name="Language" id="Language" value="{{ $training->Language or ''}}">
                @isset($training->Language)
                <option selected value="{{$training->Language}}">{{ $training->Language }}</option>
                @endisset
                <option>SPANISH</option>
                <option>ENGLISH</option>
                <option>PORTUGUES</option>
                <option>FRENCH</option>
                <option>GERMAN</option>
            </select>
        {!! $errors->first('Language', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<!-- Checar el trade Rol -->
<div class="form-group {{ $errors->has('Companies') ? 'has-error' : ''}}">
    <label for="Companies" class="col-md-4 control-label">{{ 'Companies' }}</label>
    <div class="col-md-6">
                    <select class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Choose" name="Companies[]" id="Company">
                        <optgroup label="Choose Companies">
                        @isset($training->Companies)
                                @foreach($training->Companies as $item)
                                <option value="{{$item->id}}" selected="selected">{{ $item->Name }}</option>
                                @endforeach
                        @endisset
                        @foreach($companyInstructors as $item)
                            <option value="{{ $item->id }}">{{ $item->Name }}</option>
                        @endforeach
                        </optgroup>
                    </select>
        {!! $errors->first('Company', '<p class="help-block">:message</p>') !!}
    </div>
</div>




<div class="form-group {{ $errors->has('Version') ? 'has-error' : ''}}">
    <label for="Version" class="col-md-4 control-label">{{ 'Version' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Version" type="text" id="Version" value="{{ $training->Version or ''}}" >
        {!! $errors->first('Version', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('Objetivo') ? 'has-error' : ''}}">
    <label for="Objetivo" class="col-md-4 control-label">{{ 'Objetivo' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Objetivo" type="text" id="Objetivo" value="{{ $training->Objetivo or ''}}" >
        {!! $errors->first('Objetivo', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('Materiales') ? 'has-error' : ''}}">
    <label for="Materiales" class="col-md-4 control-label">{{ 'Materiales' }}</label>
    <div class="col-md-6">
        <input type="file" name="Materiales[]" id="Materiales" multiple /> 
        <!--<input class="form-control" name="Material" type="text" id="Material" value="{{ $training->Material or ''}}" >-->
        {!! $errors->first('Material', '<p class="help-block">:message</p>') !!}
    </div>
</div>




<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>


@push('js')
    <script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>

    <script>
        jQuery(document).ready(function() {
         
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

        });
    </script>
@endpush
@push('css')
    <link href="{{asset('plugins/components/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('plugins/components/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{asset('plugins/components/icheck/skins/all.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet"/>
    {{--<link href="{{asset('plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">--}}
    <link href="{{asset('plugins/components/jqueryui/jquery-ui.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />

@endpush




<div class="form-group {{ $errors->has('Num') ? 'has-error' : ''}}">
    <label for="Num" class="col-md-4 control-label">{{ 'Num' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Num" type="text" id="Num" value="{{ $headcount->Num or ''}}" >
        {!! $errors->first('Num', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $headcount->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->first('pic', 'has-error') }}">
                    <label for="pic" class="col-md-4  control-label">Profile picture</label>
                    <div class="col-md-6">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail"
                                    style="width: 200px; height: 200px;">
                                <img src="/storage/uploads/headcount/{{ $headcount->pic_file or 'no_avatar.png'}}" alt="profile pic">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                    style="max-width: 200px; max-height: 200px;"></div>
                            <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input id="pic" name="pic" type="file" class="form-control"/>
                            </span>
                                <a href="#" class="btn btn-danger fileinput-exists"
                                    data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                        <span class="help-block">{{ $errors->first('pic', ':message') }}</span>
                    </div>
                </div>


<div class="form-group {{ $errors->has('TradeRol') ? 'has-error' : ''}}">
    <label for="TradeRol" class="col-md-4 control-label">{{ 'Trade Rol' }}</label>
    <div class="col-md-6">

      <select class="select2 form-control" name="Traderol_id" id="Traderol_id"  value="{{ $headcount->Traderol_id or ''}}">
             @isset($headcount->Traderol_id)
            <option selected  value="{{$headcount->Traderol_id}}">{{ $headcount->TradeRol->Name }}</option>
            @endisset
            @foreach($tradeRoles as $item)
            <option value="{{ $item->id }}" >{{ $item->Name }}</option>
            @endforeach
               
            </select>
          
        {!! $errors->first('TradeRol', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Company') ? 'has-error' : ''}}">
    <label for="Company" class="col-md-4 control-label">{{ 'Company' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Company" type="text" id="Company" value="{{ $headcount->Company or ''}}" >
        {!! $errors->first('Company', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Sindicalizado') ? 'has-error' : ''}}">
    <label for="Sindicalizado" class="col-md-4 control-label">{{ 'Unionized' }}</label>
    <div class="col-md-6">
        <select class="form-control" name="Sindicalizado" id="Sindicalizado"  value="{{ $headcount->Sindicalizado or ''}}">
            @isset($headcount->Sindicalizado)
                @if($headcount->Sindicalizado == 1)
                <option selected disabled hidden value="1">YES</option>
                @else
                <option selected disabled hidden value="0">NO</option>                        
                @endif
            @endisset
            <option value="1" >YES</option>
            <option value="0" >NO</option>
         </select>

        {!! $errors->first('Sindicalizado', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('Outsurcing') ? 'has-error' : ''}}">
    <label for="Outsurcing" class="col-md-4 control-label">{{ 'Outsurcing' }}</label>
    <div class="col-md-6">
        <select class="form-control" name="Outsurcing" id="Outsurcing"  value="{{ $headcount->Outsurcing or ''}}">
            @isset($headcount->Genere)
                @if($headcount->Outsurcing == 1)
                <option selected disabled hidden value="1">YES</option>
                @else
                <option selected disabled hidden value="0">NO</option>                        
                @endif
            @endisset
            <option value="1" >YES</option>
            <option value="0" >NO</option>
         </select>

        {!! $errors->first('Outsurcing', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('NivelPuesto') ? 'has-error' : ''}}">
    <label for="NivelPuesto" class="col-md-4 control-label">{{ 'Job Position' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="NivelPuesto" type="text" id="NivelPuesto" value="{{ $headcount->NivelPuesto or ''}}" >
        {!! $errors->first('NivelPuesto', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Puesto') ? 'has-error' : ''}}">
    <label for="Puesto" class="col-md-4 control-label">{{ 'Job' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Puesto" type="text" id="Puesto" value="{{ $headcount->Puesto or ''}}" >
        {!! $errors->first('Puesto', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Email') ? 'has-error' : ''}}">
    <label for="Email" class="col-md-4 control-label">{{ 'Email' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Email" type="email" id="Email" value="{{ $headcount->Email or ''}}" >
        {!! $errors->first('Email', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('AreaAsigned') ? 'has-error' : ''}}">
    <label for="AreaAsigned" class="col-md-4 control-label">{{ 'Area Asigned' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="AreaAsigned" type="text" id="AreaAsigned" value="{{ $headcount->AreaAsigned or ''}}" >
        {!! $errors->first('AreaAsigned', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('CostCenter') ? 'has-error' : ''}}">
    <label for="CostCenter" class="col-md-4 control-label">{{ 'Cost Center' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="CostCenter" type="text" id="CostCenter" value="{{ $headcount->CostCenter or ''}}" >
        {!! $errors->first('CostCenter', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('DirectLeader') ? 'has-error' : ''}}">
    <label for="DirectLeader" class="col-md-4 control-label">{{ 'Direct Leader' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="DirectLeader" type="text" id="DirectLeader" value="{{ $headcount->DirectLeader or ''}}" >
        {!! $errors->first('DirectLeader', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('JobDescripcion') ? 'has-error' : ''}}">
    <label for="JobDescripcion" class="col-md-4 control-label">{{ 'JobDescripcion' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="JobDescripcion" type="text" id="JobDescripcion" value="{{ $headcount->JobDescripcion or ''}}" >
        {!! $errors->first('JobDescripcion', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Genere') ? 'has-error' : ''}}">
    <label for="Genere" class="col-md-4 control-label">{{ 'Genere' }}</label>
    <div class="col-md-6">

      <select class="form-control" name="Genere" id="Genere"  value="{{ $headcount->Genere or ''}}">
            @isset($headcount->Genere)
                @if($headcount->Genere == 1)
                <option selected disabled hidden value="1">Male</option>
                @else
                <option selected disabled hidden value="0">Female</option>                        
                @endif
            @endisset
            <option value="1" >Male</option>
            <option value="0" >Female</option>
         </select>
        {!! $errors->first('Genere', '<p class="help-block">:message</p>') !!}
    </div>
</div>




<div class="form-group {{ $errors->has('InitialDate') ? 'has-error' : ''}}">
    <label for="InitialDate" class="col-md-4 control-label">{{ 'Initial Date' }}</label>
    <div class="col-md-6">
        @isset($workstation->InitialDate)
        <input class="form-control" name="InitialDate" type="date" id="InitialDate" value="{{ Carbon\Carbon::parse($headcount->InitialDate)->format('Y-m-d')}}" >
        @endisset
        @empty($workstation->InitialDate)
        <input class="form-control" name="InitialDate" type="date" id="InitialDate" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" >
        @endisset

        
        {!! $errors->first('InitialDate', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('FinalDate') ? 'has-error' : ''}}">
    <label for="FinalDate" class="col-md-4 control-label">{{ 'Final Date' }}</label>
    <div class="col-md-6">
          @isset($workstation->FinalDate)
        <input class="form-control" name="FinalDate" type="date" id="FinalDate" value="{{ Carbon\Carbon::parse($headcount->FinalDate)->format('Y-m-d')}}" >
        @endisset
        @empty($workstation->FinalDate)
        <input class="form-control" name="FinalDate" type="date" id="FinalDate"  >
        @endisset
        {!! $errors->first('FinalDate', '<p class="help-block">:message</p>') !!}
    </div>
    </div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>
@push('js')
<script src="{{asset('plugins/components/switchery/dist/switchery.min.js')}}"></script>
<script src="{{ asset('plugins/components/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{asset('plugins/components/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('plugins/components/icheck/icheck.init.js')}}"></script>
    <script src="{{asset('plugins/components/moment/moment.js')}}"></script>
    {{--<script src="{{asset('plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>--}}
    <script src="{{asset('plugins/components/jqueryui/jquery-ui.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"
            type="text/javascript"></script>
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{ asset('/js/adduser.js') }}"></script>
    <script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {

            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

});
</script>
@endpush
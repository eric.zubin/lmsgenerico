@extends('layouts.master')

@push('css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
@endpush

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Headcount</h3>
                    @can('add-'.str_slug('headCount'))
                        <a class="btn btn-success pull-right" href="{{ url('/head_count/head-count/create') }}"><i
                                    class="icon-plus"></i> Add Headcount</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Num</th>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Unionized</th>
                                <th>Outsurcing</th>

                                <th>Job Position</th>
                                <th>Job</th>
                                <th>Email</th>
                                <th>Area Asigned</th>
                                <th>Cost Center</th>
                                <th>DirectLeader</th>
                                <th>Genere</th>
                                <th>Job Descripcion</th>
                                <th>Initial Date</th>
                                <th>Final Date</th>
                                <th>Trade Rol</th>

                                <th>created_at </th>
                                <th>updated_at</th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($headcount as $item)
                                <tr>
                                    <td>{{ $loop->iteration or $item->id }}</td>
                                    <td>{{ $item->Num }}</td>
                                    <td>{{ $item->Name }}</td>
                                    <td>{{ $item->Company }}</td>
                                    <td>

                                    @if($item->Sindicalizado == 1)
                                    YES
                                    @else
                                    NO
                                    @endif

                                    
                                    </td>
                                    <td>
                                    @if($item->Outsurcing == 1)
                                    YES
                                        @else
                                    NO
                                    @endif
                                    </td>

                                    
                                    <td>{{ $item->NivelPuesto }}</td>
                                    <td>{{ $item->Puesto }}</td>
                                    <td>{{ $item->Email }}</td>
                                    <td>{{ $item->AreaAsigned }}</td>
                                    <td>{{ $item->CostCenter }}</td>
                                    <td>{{ $item->DirectLeader }}</td>

                                    <td>
                                    @if($item->Genere == 1)
                                    Male
                                    @else
                                    Female
                                    @endif
                                    </td>
                                    <td>{{ $item->JobDescripcion }}</td>
                                    <td>{{ $item->InitialDate }}</td>
                                    <td>{{ $item->FinalDate }}</td>


                                    
                                    <td>
                                    @if($item->TradeRol != null)
                                    {{ $item->TradeRol->Name }}
                                    @endif
                                    </td>

                                    <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->updated_at }}</td>


                                    <td>
                                        @can('view-'.str_slug('headCount'))
                                            <a href="{{ url('/head_count/head-count/' . $item->id) }}"
                                               title="View headCount">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View
                                                </button>
                                            </a>
                                        @endcan

                                        @can('edit-'.str_slug('headCount'))
                                            <a href="{{ url('/head_count/head-count/' . $item->id . '/edit') }}"
                                               title="Edit headCount">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                </button>
                                            </a>
                                        @endcan

                                        @can('delete-'.str_slug('headCount'))
                                            <form method="POST"
                                                  action="{{ url('/head_count/head-count' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Delete headCount"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                </button>
                                            </form>
                                        @endcan


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>

<script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- start - This is for export functionality only -->
    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function () {

            @if(\Session::has('message'))
            $.toast({
                heading: 'Success!',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3000,
                stack: 6
            });
            @endif
        })

        $(function () {
            $('#myTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }]
            });

        });
    </script>
   <style>
    #myTable_paginate{
        background-color: white !important;
    }
    </style>
@endpush

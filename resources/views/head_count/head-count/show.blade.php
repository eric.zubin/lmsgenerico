@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Headcount {{ $headcount->id }}</h3>
                    @can('view-'.str_slug('headCount'))
                        <a class="btn btn-success pull-right" href="{{ url('/head_count/head-count') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $headcount->id }}</td>
                            </tr>
                            <tr><th> Num </th><td> {{ $headcount->Num }} </td></tr>
                            <tr><th> Name </th><td> {{ $headcount->Name }} </td></tr>
                            <tr><th> Company </th><td> {{ $headcount->Company }} </td></tr>
                            <tr><th> unionized </th><td> {{ $headcount->Sindicalizado }} </td></tr>
                            <tr><th> Job position </th><td> {{ $headcount->NivelPuesto }} </td></tr>
                            <tr><th> Position </th><td> {{ $headcount->Puesto }} </td></tr>
                            <tr><th> Email </th><td> {{ $headcount->Email }} </td></tr>
                            <tr><th> Area Asigned </th><td> {{ $headcount->AreaAsigned }} </td></tr>
                            <tr><th> CostCenter </th><td> {{ $headcount->CostCenter }} </td></tr>
                            <tr><th> Direct Leader </th><td> {{ $headcount->DirectLeader }} </td></tr>
                            <tr><th> Genere </th><td> {{ $headcount->Genere }} </td></tr>
                            <tr><th> Job Descripcion </th><td> {{ $headcount->JobDescripcion }} </td></tr>
                            <tr><th> Initial Date </th><td> {{ $headcount->InitialDate }} </td></tr>
                            <tr><th> Final Date </th><td> {{ $headcount->FinalDate }} </td></tr>

                            <tr><th> TradeRol </th><td> {{ $headcount->TradeRol->Name }} </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">workStation {{ $workstation->id }}</h3>
                    @can('view-'.str_slug('workStation'))
                        <a class="btn btn-success pull-right" href="{{ url('/work_station/work-station') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $workstation->id }}</td>
                            </tr>
                            <tr><th> Production Unit </th><td> {{ $workstation->ProductionUnit->Name }} </td></tr>
                            <tr><th> Program </th><td> {{ $workstation->Program->Name }} </td></tr>

                            <tr><th> TrainingNeeds </th><td> {{ $workstation->TrainingNeeds }} </td></tr>

                            
                            <tr><th> WorkStationID </th><td> {{ $workstation->WorkStationID }} </td></tr>
                            <tr><th> Name </th><td> {{ $workstation->Name }} </td></tr>

                            <tr><th> Description </th><td> {{ $workstation->Description }} </td></tr>
                            <tr><th> NivelDeRiesgoOp </th><td> {{ $workstation->NivelDeRiesgoOp }} </td></tr>
                            <tr><th> InitialDate </th><td> {{ $workstation->InitialDate }} </td></tr>
                            <tr><th> FinalDate </th><td> {{ $workstation->FinalDate }} </td></tr>

                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


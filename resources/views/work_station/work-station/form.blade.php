@push('css')
    <link href="{{asset('plugins/components/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />

@endpush




<input type="checkbox"  class="js-switch" data-color="#f96262" name="Status" id="Status" 
 @isset($workstation->Status)
 @if($workstation->Status) checked 
@endif 
@endisset  
  >


<div class="form-group {{ $errors->has('ProductionUnit_id') ? 'has-error' : ''}}">

    <label for="ProductionUnit_id" class="col-md-4 control-label">{{ 'Production Unit' }}</label>
                        <div class="col-md-6 ">
                            <select class="form-control" name="ProductionUnit_id" id="ProductionUnit_id" value="{{ $workstation->ProductionUnit_id or ''}}">
                                @isset($workstation->ProductionUnit_id)
                                <option selected hidden value="{{$workstation->ProductionUnit_id}}">{{ $workstation->ProductionUnit->Name }}</option>
                                @endisset
                                @foreach($productionUnits as $item)
                                    <option value="{{ $item->id }}">{{ $item->Name }}</option>
                                @endforeach
                            </select>
                        </div>
        {!! $errors->first('ProductionUnit_id', '<p class="help-block">:message</p>') !!}
</div>
    <div class="form-group {{ $errors->has('Program_id') ? 'has-error' : ''}}">
        <label for="Program_id" class="col-md-4 control-label">{{ 'Program_id' }}</label>
        <div class="col-md-6">
                <select class="form-control" name="Program_id" id="Program_id" value="{{ $workstation->Program_id or ''}}">
                </select>
            {!! $errors->first('Program_id', '<p class="help-block">:message</p>') !!}
        </div>  
    </div>
    
    <div class="form-group {{ $errors->has('Certificaciones') ? 'has-error' : ''}}">
    <label for="Certificaciones" class="col-md-4 control-label">{{ 'Trading Init' }}</label>
    <div class="col-md-6">
            <input type="number" class="form-control" name="Certificaciones" id="Certificaciones" value="{{ $workstation->Certificaciones or ''}}">
        {!! $errors->first('Certificaciones', '<p class="help-block">:message</p>') !!}
    </div>


    </div>
    
    <div class="form-group {{ $errors->has('TrainingNeeds') ? 'has-error' : ''}}">
    <label for="TrainingNeeds" class="col-md-4 control-label">{{ 'Training Needs' }}</label>
    <div class="col-md-6">
            <input type="number"  class="form-control" name="TrainingNeeds" id="TrainingNeeds" value="{{ $workstation->TrainingNeeds or ''}}">
        {!! $errors->first('TrainingNeeds', '<p class="help-block">:message</p>') !!}
    </div>

    </div><div class="form-group {{ $errors->has('Competives') ? 'has-error' : ''}}">
    <label for="Competives" class="col-md-4 control-label">{{ 'Competence' }}</label>
    <div class="col-md-6">
                    <select class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Choose" name="Competives[]" id="Competives">
                        <optgroup label="Choose Competives">
                        @isset($workstation->Competives)
                                @foreach($workstation->Competives as $item)
                                <option value="{{$item->id}}" selected="selected">{{ $item->Name }}</option>
                                @endforeach
                        @endisset

                        @foreach($competives as $item)
                            <option value="{{ $item->id }}">{{ $item->Name }}</option>
                        @endforeach
                        </optgroup>
                    </select>
        {!! $errors->first('Competives', '<p class="help-block">:message</p>') !!}
    </div>


</div><div class="form-group {{ $errors->has('WorkStationID') ? 'has-error' : ''}}">
    <label for="WorkStationID" class="col-md-4 control-label">{{ 'WorkStationID' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="WorkStationID" type="text" id="WorkStationID" value="{{ $workstation->WorkStationID or ''}}" >
        {!! $errors->first('WorkStationID', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $workstation->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
    <label for="Description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Description" type="text" id="Description" value="{{ $workstation->Description or ''}}" >
        {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('MegaProcess') ? 'has-error' : ''}}">
    <label for="MegaProcess" class="col-md-4 control-label">{{ 'MegaProcess' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="MegaProcess" type="text" id="MegaProcess" value="{{ $workstation->MegaProcess or ''}}" >
        {!! $errors->first('MegaProcess', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('QBPLevelRequired') ? 'has-error' : ''}}">
    <label for="QBPLevelRequired" class="col-md-4 control-label">{{ 'QBP Level Required' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="QBPLevelRequired" type="number" min="0" max="3" id="QBPLevelRequired" value="{{ $workstation->QBPLevelRequired or ''}}" >
        {!! $errors->first('QBPLevelRequired', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('OperativeLevelRequired') ? 'has-error' : ''}}">
    <label for="OperativeLevelRequired" class="col-md-4 control-label">{{ 'Operative Level Required' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="OperativeLevelRequired" type="number" min="0" max="3" id="OperativeLevelRequired" value="{{ $workstation->OperativeLevelRequired or ''}}" >
        {!! $errors->first('OperativeLevelRequired', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('NivelDeRiesgoOp') ? 'has-error' : ''}}">
    <label for="NivelDeRiesgoOp" class="col-md-4 control-label">{{ 'Risk Level' }}</label>
    <div class="col-md-6">
            <select class="form-control" name="NivelDeRiesgoOp" id="NivelDeRiesgoOp" value="{{ $workstation->NivelDeRiesgoOp or ''}}">
                @isset($workstation->NivelDeRiesgoOp)
                    <option selected hidden value="{{$workstation->NivelDeRiesgoOp}}">{{ $workstation->NivelDeRiesgoOp }}</option>
                @endisset
                <option value="LOW">LOW</option>
                <option value="MID">MID</option>
                <option value="HIGH">HIGH</option>

            </select>
        {!! $errors->first('NivelDeRiesgoOp', '<p class="help-block">:message</p>') !!}
    </div>

    </div>
    
    <div class="form-group {{ $errors->has('InitialDate') ? 'has-error' : ''}}">
    <label for="InitialDate" class="col-md-4 control-label">{{ 'Initial Date' }}</label>
    <div class="col-md-6">
        @isset($workstation->InitialDate)
        <input class="form-control" name="InitialDate" type="date" id="InitialDate" value="{{ Carbon\Carbon::parse($workstation->InitialDate)->format('Y-m-d')}}" >
        @endisset
        @empty($workstation->InitialDate)
        <input class="form-control" name="InitialDate" type="date" id="InitialDate" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" >
        @endisset

        
        {!! $errors->first('InitialDate', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('FinalDate') ? 'has-error' : ''}}">
    <label for="FinalDate" class="col-md-4 control-label">{{ 'Final Date' }}</label>
    <div class="col-md-6">
          @isset($workstation->FinalDate)
        <input class="form-control" name="FinalDate" type="date" id="FinalDate" value="{{ Carbon\Carbon::parse($workstation->FinalDate)->format('Y-m-d')}}" >
        @endisset
        @empty($workstation->FinalDate)
        <input class="form-control" name="FinalDate" type="date" id="FinalDate"  >
        @endisset
        {!! $errors->first('FinalDate', '<p class="help-block">:message</p>') !!}
    </div>
    </div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>


@push('js')
<script src="{{asset('plugins/components/switchery/dist/switchery.min.js')}}"></script>
<script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
         
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

        });
    </script>

<script>
      jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
                         
             $("#ProductionUnit_id").change(function () {
                                $("#ProductionUnit_id option:selected").each(function () {
                                    var id = $("#ProductionUnit_id").val();

                                        $.ajax({
                                            url: "/select_program/"+id,
                                            type: "post",
                                            dataType: 'json',
                                            data: { "_token": "{{ csrf_token() }}"} ,
                                            success: function (response) {
                                                            $('#Program_id').empty().append( response.html);
                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }


                                        });

                                });
                })


                var id = $("#ProductionUnit_id").val();
                $.ajax({
                    url: "/select_program/"+id,
                    type: "post",
                    data: { "_token": "{{ csrf_token() }}"} ,
                    success: function (response) {
                                    $('#Program_id').empty().append( response.html);
                                    @isset($workstation->Program_id)
                                    $("#Program_id").val({{$workstation->Program_id}});
                                    @endisset
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            }


                });

        });

</script>
@endpush
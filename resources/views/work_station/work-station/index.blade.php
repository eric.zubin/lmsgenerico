@extends('layouts.master')

@push('css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
@endpush

@section('content')
    <div class="section-title">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Workstation</h3>
                    @can('add-'.str_slug('workStation'))
                        <a class="btn btn-success pull-right" href="{{ url('/work_station/work-station/create') }}"><i
                                    class="icon-plus"></i> Add Workstation</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table nowrap" id="myTable">
                            <thead>
                            <tr>
                                 <th>#</th>

                                <th>Actions</th>

                                <th>Production Unit</th>
                                <th>Program</th>
                                <th>Name</th>
                                <th>WorkStationID</th>
                                <th>Description</th>
                                <th>Initial Date</th>
                                <th>Final Date</th>
                                <th>TrainingNeeds</th>
                                <th>MegaProcess</th>
                                <th>OperativeLevelRequired</th>
                                <th>Active</th>
                                <th>created_at </th>
                                <th>updated_at</th>
                                
                            
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($workstation as $item)
                                <tr>
                                <td>{{ $loop->iteration or $item->id }}</td>

                                <td>
                                        @can('view-'.str_slug('workStation'))
                                            <a href="{{ url('/work_station/work-station/' . $item->id) }}"
                                               title="View workStation">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View
                                                </button>
                                            </a>
                                        @endcan

                                        @can('edit-'.str_slug('workStation'))
                                            <a href="{{ url('/work_station/work-station/' . $item->id . '/edit') }}"
                                               title="Edit workStation">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                </button>
                                            </a>
                                        @endcan

                                        @can('delete-'.str_slug('workStation'))
                                            <form method="POST"
                                                  action="{{ url('/work_station/work-station' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Delete workStation"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                </button>
                                            </form>
                                        @endcan


                                    </td>
                                    <td>{{ $item->ProductionUnit->Name }}</td>
                                    <td>
                                    @if( $item->Program != null)
                                    {{ $item->Program->Name }}
                                    @endif
                                    </td>
                                    <td>{{ $item->Name }}</td>
                                    <td>{{ $item->WorkStationID }}</td>
                                    <td> {{ $item->Description }} </td>
                                    <td> {{ $item->InitialDate }} </td>
                                    <td> {{ $item->FinalDate }} </td>
                                    <td> {{ $item->TrainingNeeds }} </td>
                                    <td> {{ $item->MegaProcess }} </td>
                                    <td> {{ $item->OperativeLevelRequired }} </td>
                                    <td> {{ $item->Active }} </td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->updated_at }}</td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>

    <script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function () {

            @if(\Session::has('message'))
            $.toast({
                heading: 'Success!',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3000,
                stack: 6
            });
            @endif
        })

        $(function () {
            $('#myTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }]
            });

        });
    </script>
   <style>
    #myTable_paginate{
        background-color: white !important;
    }
    .section-title {
    overflow: scroll;
    overflow-x: scroll;
    overflow-y:hidden
    }
    </style>
@endpush

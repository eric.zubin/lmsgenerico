@extends('layouts.master')

@push('css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .requerio{
            background-color: #fdbf2d;
            color: black;
            
        }
        .calificado{
            background-color: #94ce58;
            color: black;	
        }
        .calificadoQ{
            background-color: #516b37;
            color: white;
        }
        .planeado{
            background-color: #1072bd;
            color: black;
        }
        .proceso{
            background-color: #feeaa1;
            color: black;
        }
        .calificadoI{
            background-color: #c7eecf;
            color: black; 
        }
        .cancelado{
            background-color: #fc0d1b;
            color: white;
        }
        .niveles{
            width: 900px;
        }
    </style>
@endpush



@section('content')
    <div class="container-fluid">
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
            @if($program != null)
            <a href="/skill_matriz/skill-matriz-exel/{{$program->ProductionUnit_id}}/{{$program->id}}" class="btn btn-primary"  target="_blank">Export Exel</a> 
            @endif
            <a href="/skill_matriz_vista" class="btn btn-default" >Skill Matrix by WorkStation</a> 
            <a href="/skill_matriz_mi" class="btn btn-primary" >Skill Matrix by Manufacturing Instruction</a> 
            <a href="/skill_matriz_co" class="btn btn-primary" >Skill Matrix by Competencies</a>   
                

            <div class="clearfix"></div>
            <img class="niveles" src="{{asset('plugins/images/image001.png')}}"  >
                <div class="clearfix"></div


                <div class="white-box">
                    <h3 class="box-title pull-left">Skill Matrix</h3>
             
                                               


                    <div class="row col-sm-12">

<div class="form-group {{ $errors->has('ProductionUnit_id') ? 'has-error' : ''}}">

<label for="ProductionUnit_id" class="col-md-4 control-label">{{ 'Production Unit' }}</label>
                <div class="col-md-6 ">
                    <select class="form-control" name="ProductionUnit_id" id="ProductionUnit_id" value="{{ $workstation->ProductionUnit_id or ''}}">
                            @isset($program->ProductionUnit_id)
                            <option selected hidden value="{{$program->ProductionUnit_id}}">{{ $program->ProductionUnit->Name }}</option>
                            @endisset
                            @foreach($productionUnits as $item)
                            <option value="{{ $item->id }}">{{ $item->Name }}</option>
                            @endforeach
                    </select>
                </div>
{!! $errors->first('ProductionUnit_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('Program_id') ? 'has-error' : ''}}">
<label for="Program_id" class="col-md-4 control-label">{{ 'Program_id' }}</label>
<div class="col-md-6">
        <select class="form-control" name="Program_id" id="Program_id" value="{{ $workstation->Program_id or ''}}">
        </select>
    {!! $errors->first('Program_id', '<p class="help-block">:message</p>') !!}
</div>  
</div>
<button class="btn btn-danger" onclick="irVista()">ANALIZE</button>


</div>
@if($program != null)

                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead>
                            <tr>
                                <th>Nombre Empleado</th>
                                @foreach($workStation as $item)
                                <th>{{  $item->Name }}</th>

                                @endforeach
                                <th>Cantidad</th>
                                <th>Nivel</th>


                          
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($head_count as $item)
                                <tr>
                                    <td>{{  $item->Name }}</td>
                     
                                        @foreach($workStation as $item2)
                                        <td>   
                                            <button type="button" class="btn
        
                                            @foreach($skillMatriz  as $key => $item3)
                                                        @if($item->id === $item3->head_count_id   && $item2->id ==  $item3->worstation_id )
                                                        @switch($item3->Nivel)
                                                            @case(0)
                                                                requerio
                                                                @break

                                                            @case(1)
                                                                calificado
                                                                @break
                                                            @case(2)
                                                                calificado
                                                                @break
                                                            @case(3)

                                                                calificado
                                                                @break
                                                            @case(4)

                                                                calificadoQ
                                                                @break
                                                            @case(5)
                                                                calificadoQ
                                                                @break
                                                            
                                                            @case(6)
                                                                calificadoQ
                                                                @break
                                                            
                                                            @case(7)
                                                                planeado
                                                                @break
                                                            
                                                            @case(8)
                                                                proceso
                                                                @break
                                                            
                                                            @case(9)

                                                                calificadoI
                                                                @break

                                                            @case(10)
                                                                calificadoI
                                                                @break
                                                            @case(11)
                                                                calificadoI
                                                                @break
                                                            @case(12)
                                                                cancelado
                                                                @break
                                                        @endswitch
                                             
                                                    
                                                        @break
                                                        @endif
                                                    
                                                    @endforeach "
                                                    onclick="myFunction({{ $item->id }},{{ $item2->id }}, '{{ $item->Name }}','{{ $item->pic_file }}')">
                                                    @foreach($skillMatriz  as $key => $item3)
                                                            @if($item->id === $item3->head_count_id   && $item2->id ==  $item3->worstation_id )
                                                            {{ $item3->EXPERT}}
                                                            <?php 
                                                                unset($skillMatriz[$key]);
                                                            ?>
                                                        
                                                            @break
                                                            @endif
                                                        
                                                        @endforeach
                                                </button>

                                        </td>
                                   

                                        @endforeach
                                        @foreach($count as $key => $cantidad)
                                        @if($item->id === $cantidad->head_count_id )
                                     
                                        <td>   
                                        {{ $cantidad->total}}
                                        </td>

                                        <td>   
                                        @switch($cantidad->total)
                                            @case(1)
                                                Junior
                                                @break

                                            @case(2)
                                            Junior
                                                @break
                                            @case(3)
                                                Professional
                                                @break
                                            @case(4)
                                                Professional
                                                @break
                                            @case(5)
                                                Professional
                                                @break
                                            @case(6)
                                                Senior
                                                @break
                                            @case(7)
                                                Senior
                                                @break
                                            @case(8)
                                                Expert
                                                @break
                                            @case(9)
                                                Expert
                                                @break
                                            @case(10)
                                                Expert
                                                @break
                                            @case(11)
                                                Expert
                                                @break
                                            @default
                                                SrExpert
                                                @break
                                        @endswitch
                                        
                                        </td>

                                        <?php 
                                         unset($count[$key]);
                                        ?>
                                        @break
                                                    @endif
                                        @endforeach

                                    
                                </tr>
                           
                                @endforeach
                            
                                <tfoot>

                                <tr>
                                <td>TOTAL Real</td>
                                @foreach($workStation as $item4)
                                    @foreach($count3 as $key => $cantidad)
                                        @if( $item4->id = $cantidad->worstation_id)
                                        <td>   {{ $cantidad->total}} </td>
                                        <?php 
                                         unset($count3[$key]);
                                        ?>
                                        @break
                                  
                                        @endif


                                        @if( $item4->id = $count3->last()->worstation_id )
                                        <td>0</td>
                                        @endif
                                    @endforeach
                                @endforeach
                                </tr>

                                <tr>

                                <td>TOTAL </td>
                                @foreach($workStation as $item2)
                                    @foreach($count2 as $key => $cantidad)
                                        @if( $item2->id = $cantidad->worstation_id)
                                        <td>   {{ $cantidad->total}} </td>
                                        <?php 
                                         unset($count2[$key]);
                                        ?>
                                        @endif
                                    @endforeach
                                @endforeach
                                </tr>

                           
                                <tr>
                                <td>Training Needs</td>
                                @foreach($workStation as $item3)
                                        <td>   {{ $item3->TrainingNeeds}} </td>                                     
                                @endforeach
                                </tr>

                                </tfoot>

                                <td></td>
                                <td></td>

                            </tr>

                          
                            </tbody>
                        </table>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
 

                    <!-- /.modal -->
                    <div id="modal" class="modal fade" tabindex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                    </button>
                                    <h4 class="modal-title">Evidencias</h4></div>
                                <div class="modal-body">
                                        <div class="form-group">
                                        
                                        <img class="col-sm-4" id="foto">
                                        <div class="col-sm-8">
                                        <h2 id="nombre"></h2>
                                        </div>



                                        <div id="evidencias"></div>
                                </div>
                                <div class="modal-footer">
                             
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">
                                        Close
                                    </button>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                  
              
@endsection

@push('js')
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>

    <script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <!-- end - This is for export functionality only -->
    <script>


function irVista(){
var select = document.getElementById("Program_id");
var ProductionUnit_id = document.getElementById("ProductionUnit_id");

window.location.href = "/skill_matriz_vista/"+ProductionUnit_id.value + "/"+select.value;  // Sadly this reloads

}

     function myFunction(workstation,headCount,Name,pic_file) {

                $('#worstation_id').val(headCount);
                $('#head_count_id').val(workstation);
                $('#modal').modal('show');   
                document.getElementById("nombre").innerHTML = Name;
                document.getElementById("foto").src = "/storage/uploads/headcount/" + pic_file; 

                $.ajax({
                url: "/skill_vista/"+workstation+"/"+headCount ,
                type: "get",
                data: { "_token": "{{ csrf_token() }}"} ,
                success: function (response) {

                        $.ajax({
                        url: "/skill_vista_evicencia/"+response.id,
                        type: "get",
                        data: { "_token": "{{ csrf_token() }}"} ,
                        success: function (response) {
                                response.forEach(function(element) {
                                console.log(element.id);
                                document.getElementById("evidencias").innerHTML +=  "<a type='button' href='/evidencia/" + element.id + "' target='_blank' class='btn btn-default waves-effect'>"+ element.NombreArchivo+ "</a>"; 
                                document.getElementById("evidencias").innerHTML += "</br>"; 
                                document.getElementById("evidencias").innerHTML += "</br>"; 

                                });

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                        }

                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                }

            });

    }


        $(document).ready(function () {


        $("#ProductionUnit_id").change(function () {
                                $("#ProductionUnit_id option:selected").each(function () {
                                    var id = $("#ProductionUnit_id").val();

                                        $.ajax({
                                            url: "/select_program/"+id,
                                            type: "post",
                                            dataType: 'json',
                                            data: { "_token": "{{ csrf_token() }}"} ,
                                            success: function (response) {
                                                            //$('#Program_id').empty().append( response.html);
                                                            //window.location.href = "/skill_matriz_vista/"+response.programfirst.id;  // Sadly this reloads

                                                    },
                                                    error: function(jqXHR, textStatus, errorThrown) {
                                                    console.log(textStatus, errorThrown);
                                                    }


                                        });

                                });
                })

                $("#Program_id").change(function () {
                                $("#Program_id option:selected").each(function () {
                                    var id = $("#Program_id").val();
                                    //window.location.href = "/skill_matriz_vista/"+id;  // Sadly this reloads
                                  

                                });
                })


                var id = $("#ProductionUnit_id").val();
                $.ajax({
                    url: "/select_program/"+id,
                    type: "post",
                    data: { "_token": "{{ csrf_token() }}"} ,
                    success: function (response) {
                                    $('#Program_id').empty().append( response.html);
                                    @isset($program->id)
                                    $("#Program_id").val({{$program->id}});
                                    @endisset

                                    

                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            }


                });

            })

    </script>
   <style>
    #myTable_paginate{
        background-color: white !important;
    }
    </style>
@endpush

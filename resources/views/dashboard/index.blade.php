@extends('layouts.master')

@push('css')
    <style>
        .info-box .info-count {
            margin-top: 0px !important;
        }
    </style>
@endpush


@push('css')
    <link href="{{asset('plugins/components/owl.carousel/owl.carousel.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('plugins/components/owl.carousel/owl.theme.default.css')}}" rel="stylesheet" type="text/css"/>
    <style>
    .carousel-inner{
        max-height: 567px;
        width: 800px;
    }
    </style>
@endpush


@section('content')


        <div class="row m-0">

            <div class="col-md-3 col-sm-6 info-box">

                <div class="media">

                    <div class="media-left">
                        <span class="icoleaf bg-primary text-white"><i
                                    class="mdi mdi-checkbox-marked-circle-outline"></i></span>
                    </div>
                    <div class="media-body">
                        <h3 class="info-count text-blue">{{$headCount[0]->total}}</h3>
                        <p class="info-text font-12">HeadCount</p>
                        <span class="hr-line"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 info-box">
                <div class="media">
                    <div class="media-left">
                        <span class="icoleaf bg-primary text-white"><i class="mdi mdi-comment-text-outline"></i></span>
                    </div>
                    <div class="media-body">
                        <h3 class="info-count text-blue">{{$generes[0]->Genero}}</h3>
                        <p class="info-text font-12">Female</p>
                        <span class="hr-line"></span>
                        <h3 class="info-count text-blue">{{$generes[1]->Genero}}</h3>
                        <p class="info-text font-12">Male</p>
                    
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 info-box">
                <div class="media">
                    <div class="media-left">
                        <span class="icoleaf bg-primary text-white"><i class="mdi mdi-coin"></i></span>
                    </div>
                    <div class="media-body">
                        <h3 class="info-count text-blue">{{$NOOutsurcingNOSindicalizado[0]->TOutsurcing}}</h3>
                        <p class="info-text font-12">WHITE COLLAR AIRBUS</p>
                        <span class="hr-line"></span>
                        <h3 class="info-count text-blue">{{$SIOutsurcingNOSindicalizado[0]->TOutsurcing}}</h3>
                        <p class="info-text font-12">WHITE COLLAR OUTSOURCING</p>
             
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 info-box">
                <div class="media">
                    <div class="media-left">
                        <span class="icoleaf bg-primary text-white"><i class="mdi mdi-coin"></i></span>
                    </div>
                    <div class="media-body">
                    <h3 class="info-count text-blue">{{$NOOutsurcingSISindicalizado[0]->TOutsurcing}}</h3>
                        <p class="info-text font-12">BLUE COLLAR AIRBUS</p>
                             <span class="hr-line"></span>
                        <h3 class="info-count text-blue">{{$SIOutsurcingSISindicalizado[0]->TOutsurcing}}</h3>
                        <p class="info-text font-12">BLUE COLLAR OUTSOURCING</p>
                    </div>
                </div>
            </div>

        </div>

<style>

.medio {

    display: flex;
   justify-content: center;
   align-items: center;
}

</style>
        <div class="container-fluid">


            <div class="row">
                <div class="medio">
                
                    <div id="carousel-example-captions-1" data-ride="carousel" class="carousel slide">
                                <ol class="carousel-indicators">
                                    @foreach($imagen as $item)
                                        @if($loop->iteration == 1)
                                            <li data-target="#carousel-example-captions-{{$loop->iteration}}" data-slide-to="0" class="active"></li>
                                        @else
                                            <li data-target="#carousel-example-captions-{{$loop->iteration}}" data-slide-to="1" ></li>
                                        @endif
                                    @endforeach

                                </ol>                                     

                                <div role="listbox" class="carousel-inner">
                                    @foreach($imagen as $item)
                                        @if($loop->iteration == 1)
                                            <div class="item active"><img src="/{{$item->imagen}}" alt="{{$item->id}}" style="/*! width: 800px; */height: 400px;">
                                        @else
                                            <div class="item"><img src="/{{$item->imagen}}" alt="{{$item->imagen}}" style="/*! width: 800px; */height: 400px;">
                                        @endif
                                        </div>

                                    @endforeach
                                </div>
                    </div>

                </div>
            </div>
  
        <div class="clearfix"></div>
<br>
<br>

            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="white-box bg-primary color-box">
                        <h1 class="text-white font-light">{{ round($polyProductionUnit_id[0]->C * 100 / $polyProductionUnit_id[0]->N,2) }} % <span class="font-14">POLY POLY WS COVERED ASSEMBLY</span></h1>
                        <div class="ct-polypolyOneTotal chart-pos"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="white-box bg-primary color-box">
                        <h1 class="text-white font-light">{{ round($polyProductionUnit_id[1]->C * 100 / $polyProductionUnit_id[1]->N,2) }}  % <span class="font-14">POLY POLY WS COVERED SINGLE PARTS</span></h1>
                        <div class="ct-polypolyTwoTotal chart-pos"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="white-box bg-danger color-box">
                        <h1 class="text-white font-light m-b-0">{{ round($polyProductionUnGenral[0]->C * 100 / $polyProductionUnGenral[0]->N,2) }} %</h1>
                        <span class="hr-line"></span>
                        <p class="cb-text">Total</p>
                        <div class="chart">
                            <input class="knob" data-min="0" data-max="100" data-bgColor="#f86b4a"
                                   data-fgColor="#ffffff" data-displayInput=false data-width="96" data-height="96"
                                   data-thickness=".1" value="{{$polypolyTotal[0]->porcentaje}}" readonly>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="white-box bg-primary color-box"> 
                        <h1 class="text-white font-light">{{ round($PolyWorkStUnit_id[0]->poly * 100 / $PolyWorkStUnit_id[0]->total,2) }}% <span class="font-14">POLY 3 Employees cert per WS ASSEMBLY</span></h1>
                        <div class="ct-polypolyOneTotalTres chart-pos"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="white-box bg-primary color-box">
                        <h1 class="text-white font-light">{{ round($PolyWorkStUnit_id[1]->poly * 100 / $PolyWorkStUnit_id[1]->total,2) }} % <span class="font-14">POLY 3 Employees cert per WS SINGLE PARTS</span></h1>
                        <div class="ct-polypolyTwoTotalTres chart-pos"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="white-box bg-danger color-box">
                        <h1 class="text-white font-light m-b-0">{{ round($PolyWorkStatTotal[0]->poly * 100 / $PolyWorkStatTotal[0]->total,2) }}  %</h1>
                        <span class="hr-line"></span>
                        <p class="cb-text">Total</p>
                        <div class="chart">
                            <input class="knob" data-min="0" data-max="100" data-bgColor="#f86b4a"
                                   data-fgColor="#ffffff" data-displayInput=false data-width="96" data-height="96"
                                   data-thickness=".1" value="{{ round($PolyWorkStatTotal[0]->poly * 100 / $PolyWorkStatTotal[0]->total,2) }}" readonly>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="white-box bg-primary color-box"> 
                        <h1 class="text-white font-light">{{ round($PolyWorkStUnitPeople_id[0]->poly * 100 / $PolyWorkStUnitPeople_id[0]->headcountTotal,2) }}% <span class="font-10">Poly 3X3 People Certified in minimum 3 Work Stations ASSEMBLY</span></h1>
                        <div class="ct-polypolyOneTotalTresW chart-pos"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="white-box bg-primary color-box">
                        <h1 class="text-white font-light">{{ round($PolyWorkStUnitPeople_id[1]->poly * 100 / $PolyWorkStUnitPeople_id[1]->headcountTotal,2) }} % <span class="font-10">Poly 3X3 People Certified in minimum 3 Work Stations SINGLE PARTS</span></h1>
                        <div class="ct-polypolyTwoTotalTresW chart-pos"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="white-box bg-danger color-box">
                        <h1 class="text-white font-light m-b-0">{{ round($PolyWorkStatPeopleTotal[0]->poly * 100 / $PolyWorkStatPeopleTotal[0]->headcountTotal,2) }}  %</h1>
                        <span class="hr-line"></span>
                        <p class="cb-text">Total</p>
                        <div class="chart">
                            <input class="knob" data-min="0" data-max="100" data-bgColor="#f86b4a"
                                   data-fgColor="#ffffff" data-displayInput=false data-width="96" data-height="96"
                                   data-thickness=".1" value="{{ round($PolyWorkStatPeopleTotal[0]->poly * 100 / $PolyWorkStatPeopleTotal[0]->headcountTotal,2) }}" readonly>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="white-box user-table">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4 class="box-title">Training Status</h4>
                            </div>
                        
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                               
                                    <th>Training</th>
                                    <th>Count</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Validation</td>
                                    <td>{{$training_requests1[0]->total}}</td>
                               
                                </tr>
                                <tr>
                                    <td>Plan </td>
                                    <td>{{$training_requests2[0]->total}}</td>
                                </tr>
                                <tr>
                                    <td>Training</td>
                                    <td>{{$training_requests3[0]->total}}</td>
                                </tr>
                                <tr>
                                    <td>Evaluation</td>
                                    <td>{{$training_requests4[0]->total}}</td>
                                </tr>
                                <tr>
                                    <td>Documented</td>
                                    <td>{{$training_requests5[0]->total}}</td>

                                </tr>
                                <tr>
                                    <td>Archived</td>
                                    <td>{{$training_requests6[0]->total}}</td>

                                </tr>
                                <tr>
                                    <td>Closed</td>
                                    <td>{{$training_requests7[0]->total}}</td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    
                    </div>
                </div>
            </div>

            <div class="row">

            
            <div class="col-sm-6">
                        <div class="white-box small-box-widget">
                            <div class="p-t-10 p-b-10">
                                <div class="icon-box bg-warning">
                                    <i class="icon-refresh"></i>
                                </div>
                                <div class="detail-box">
                                    <h4>Completed Plan<span class="pull-right text-warning font-22 font-normal">@if($porcentajetradingconcode[0]->tradingplan != 0) {{ round($porcentajetradingconcode[0]->trading_exucutades * 100 / $porcentajetradingconcode[0]->tradingplan,2) }} @else 100 @endif %</span>
                                    </h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                             aria-valuenow="@if($porcentajetradingconcode[0]->tradingplan != 0) {{ round($porcentajetradingconcode[0]->trading_exucutades * 100 / $porcentajetradingconcode[0]->tradingplan,2) }} @else 100 @endif %" aria-valuemin="0" aria-valuemax="100"
                                             style="width: @if($porcentajetradingconcode[0]->tradingplan != 0) {{ round($porcentajetradingconcode[0]->trading_exucutades * 100 / $porcentajetradingconcode[0]->tradingplan,2) }} @else 100 @endif %%">
                                            <span class="sr-only">@if($porcentajetradingconcode[0]->tradingplan != 0) {{ round($porcentajetradingconcode[0]->trading_exucutades * 100 / $porcentajetradingconcode[0]->tradingplan,2) }} @else 100 @endif %% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="white-box small-box-widget">
                            <div class="p-t-10 p-b-10">
                                <div class="icon-box bg-warning">
                                    <i class="icon-refresh"></i>
                                </div>
                                <div class="detail-box">
                                    <h4>Completed not Planned<span class="pull-right text-warning font-22 font-normal">@if($porcentajetradingsincode[0]->tradingplan != 0) {{ round($porcentajetradingsincode[0]->trading_exucutades * 100 / $porcentajetradingsincode[0]->tradingplan,2) }} @else 100 @endif %</span>
                                    </h4>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                             aria-valuenow="@if($porcentajetradingsincode[0]->tradingplan != 0) {{ round($porcentajetradingsincode[0]->trading_exucutades * 100 / $porcentajetradingsincode[0]->tradingplan,2) }} @else 100 @endif" aria-valuemin="0" aria-valuemax="100"
                                             style="width: @if($porcentajetradingsincode[0]->tradingplan != 0) {{ round($porcentajetradingsincode[0]->trading_exucutades * 100 / $porcentajetradingsincode[0]->tradingplan,2) }} @else 100 @endif%">
                                            <span class="sr-only">@if($porcentajetradingsincode[0]->tradingplan != 0) {{ round($porcentajetradingsincode[0]->trading_exucutades * 100 / $porcentajetradingsincode[0]->tradingplan,2) }} @else 100 @endif Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="white-box small-box-widget">
                            <ul class="list-inline row">
                                <li class="col-xs-3 p-t-10">
                                    <div class="icon-box bg-primary">
                                        <i class="icon-bag"></i>
                                    </div>
                                </li>
                                <li class="col-xs-9 p-l-20">
                                    <h4>{{$resultado1[0]->numero}}</h4>
                                    <h4>Cost Training Plan</h4>

                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="white-box small-box-widget">
                            <ul class="list-inline row">
                                <li class="col-xs-3 p-t-10">
                                    <div class="icon-box bg-primary">
                                        <i class="icon-bag"></i>
                                    </div>
                                </li>
                                <li class="col-xs-9 p-l-20">
                                    <h4>{{$resultado2[0]->numero}}</h4>
                                    <h4>People Training Plan</h4>

                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="white-box small-box-widget">
                            <ul class="list-inline row">
                                <li class="col-xs-3 p-t-10">
                                    <div class="icon-box bg-primary">
                                        <i class="icon-bag"></i>
                                    </div>
                                </li>
                                <li class="col-xs-9 p-l-20">
                                    <h4>{{$resultado3[0]->numero}}</h4>
                                    <h4>Cost Training Real</h4>

                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="white-box small-box-widget">
                            <ul class="list-inline row">
                                <li class="col-xs-3 p-t-10">
                                    <div class="icon-box bg-primary">
                                        <i class="icon-bag"></i>
                                    </div>
                                </li>
                                <li class="col-xs-9 p-l-20">
                                    <h4>{{$resultado4[0]->numero}}</h4>
                                    <h4>People Training Real</h4>

                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="white-box small-box-widget">
                            <ul class="list-inline row">
                                <li class="col-xs-3 p-t-10">
                                    <div class="icon-box bg-primary">
                                        <i class="icon-bag"></i>
                                    </div>
                                </li>
                                <li class="col-xs-9 p-l-20">
                                    <h4>{{$resultado5[0]->numero}}</h4>
                                    <h4>Training No Planeado</h4>

                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="white-box small-box-widget">
                            <ul class="list-inline row">
                                <li class="col-xs-3 p-t-10">
                                    <div class="icon-box bg-primary">
                                        <i class="icon-bag"></i>
                                    </div>
                                </li>
                                <li class="col-xs-9 p-l-20">
                                    <h4>{{$resultado6[0]->numero}}</h4>
                                    <h4>Training Planeado con APTCODE</h4>

                                </li>
                            </ul>
                        </div>
                    </div>

            </div>


            
   
            <!-- ===== Right-Sidebar ===== -->
        {{--@include('layouts.partials.right-sidebar')--}}
        <!-- ===== Right-Sidebar-End ===== -->
        </div>


@endsection

@push('js')
    <script>
    $(function() {
    "use strict";

    /* ===== Revenue chart ===== */

    
    

    var chart1 = new Chartist.Line('.ct-polypolyOneTotal', {
        labels: [
            @foreach ($polypolyOne as $poly)
                "{{ $poly->Code }} {{ round($poly->porcentaje) }}%" ,
            @endforeach
        ],
        series: [
            [
            @foreach ($polypolyOne as $poly)
                {{ $poly->porcentaje }},
            @endforeach
            ]
        ]
    }, {
        chartPadding: {
            left: -20,
            top: 10,
        },
        low: 1,
        showPoint: true,
        height: '100px',
        fullWidth: true,
        axisX: {
            showLabel: true,
            showGrid: true
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        showArea: true,
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });

    var chart2 = new Chartist.Line('.ct-polypolyTwoTotal', {
        labels: [ @foreach ($polypolyTwo as $poly)
                "{{ $poly->Code }} {{ round($poly->porcentaje) }}%" ,
            @endforeach],
        series: [
            [@foreach ($polypolyTwo as $poly)
                {{ $poly->porcentaje }},
            @endforeach]
        ]
    }, {
        chartPadding: {
            left: -20,
            top: 10,
        },
        low: 1,
        showPoint: true,
        height: '100px',
        fullWidth: true,
        axisX: {
            showLabel: true,
            showGrid: true
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        showArea: true,
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });

    var chart3 = new Chartist.Line('.ct-polypolyOneTotalTres', {
        labels: [ @foreach ($polypolyOneTotalTresTresGrafica as $poly)
                "{{ $poly->Code }} {{ round($poly->poly * 100 / $poly->total) }}%" ,
            @endforeach],
        series: [
            [@foreach ($polypolyOneTotalTresTresGrafica as $poly)
            {{ round($poly->poly * 100 / $poly->total) }},
            @endforeach]
        ]
    }, {
        chartPadding: {
            left: -20,
            top: 10,
        },
        low: 1,
        showPoint: true,
        height: '100px',
        fullWidth: true,
        axisX: {
            showLabel: true,
            showGrid: true
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        showArea: true,
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });


    var chart4 = new Chartist.Line('.ct-polypolyTwoTotalTres', {
        labels: [ @foreach ($polypolyTwoTotalTresTresGrafica as $poly)
        "{{ $poly->Code }} {{ round($poly->poly * 100 / $poly->total) }}%" ,
            @endforeach],
        series: [
            [@foreach ($polypolyTwoTotalTresTresGrafica as $poly)
            {{ round($poly->poly * 100 / $poly->total) }},
            @endforeach]
        ]
    }, {
        chartPadding: {
            left: -20,
            top: 10,
        },
        low: 1,
        showPoint: true,
        height: '100px',
        fullWidth: true,
        axisX: {
            showLabel: true,
            showGrid: true
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        showArea: true,
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });

    var chart5 = new Chartist.Line('.ct-polypolyOneTotalTresW', {
        labels: [ @foreach ($PolyWorkStatPeopleTotalOne as $poly)
                "{{ $poly->Code }} {{ round($poly->totalP * 100 / $poly->headcountTotal) }}%" ,
            @endforeach],
        series: [
            [@foreach ($PolyWorkStatPeopleTotalOne as $poly)
            {{ round($poly->totalP * 100 / $poly->headcountTotal) }},
            @endforeach]
        ]
    }, {
        chartPadding: {
            left: -20,
            top: 10,
        },
        low: 1,
        showPoint: true,
        height: '100px',
        fullWidth: true,
        axisX: {
            showLabel: true,
            showGrid: true
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        showArea: true,
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });


    var chart6 = new Chartist.Line('.ct-polypolyTwoTotalTresW', {
        labels: [ @foreach ($PolyWorkStatPeopleTotalTwo as $poly)
        "{{ $poly->Code }} {{ round($poly->totalP * 100 / $poly->headcountTotal) }}%" ,
            @endforeach],
        series: [
            [@foreach ($PolyWorkStatPeopleTotalTwo as $poly)
            {{ round($poly->totalP * 100 / $poly->headcountTotal) }},
            @endforeach]
        ]
    }, {
        chartPadding: {
            left: -20,
            top: 10,
        },
        low: 1,
        showPoint: true,
        height: '100px',
        fullWidth: true,
        axisX: {
            showLabel: true,
            showGrid: true
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        showArea: true,
        plugins: [
            Chartist.plugins.tooltip()
        ]
    });

    var chart = [chart1, chart2,chart3,chart4,chart6,chart5];

for (var i = 0; i < chart.length; i++) {
    chart[i].on('draw', function(data) {
        if (data.type === 'line' || data.type === 'area') {
            data.element.animate({
                d: {
                    begin: 500 * data.index,
                    dur: 500,
                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                    to: data.path.clone().stringify(),
                    easing: Chartist.Svg.Easing.easeInOutElastic
                }
            });
        }
        if (data.type === 'bar') {
            data.element.animate({
                y2: {
                    dur: 500,
                    from: data.y1,
                    to: data.y2,
                    easing: Chartist.Svg.Easing.easeInOutElastic
                },
                opacity: {
                    dur: 500,
                    from: 0,
                    to: 1,
                    easing: Chartist.Svg.Easing.easeInOutElastic
                }
            });
        }
    });
}


});
    
    </script>
    <script src="{{asset('plugins/components/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('plugins/components/owl.carousel/owl.custom.js')}}"></script>
@endpush
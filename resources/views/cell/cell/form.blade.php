<div class="form-group {{ $errors->has('Code') ? 'has-error' : ''}}">
    <label for="Code" class="col-md-4 control-label">{{ 'Code' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Code" type="text" id="Code" value="{{ $cell->Code or ''}}" >
        {!! $errors->first('Code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
    <label for="Description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Description" type="text" id="Description" value="{{ $cell->Description or ''}}" >
        {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('Program') ? 'has-error' : ''}}">
    <label for="Program" class="col-md-4 control-label">{{ 'Program' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Program" type="text" id="Program" value="{{ $cell->Program or ''}}" >
        {!! $errors->first('Program', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ProductionUnit') ? 'has-error' : ''}}">
    <label for="ProductionUnit" class="col-md-4 control-label">{{ 'Productionunit' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="ProductionUnit" type="text" id="ProductionUnit" value="{{ $cell->ProductionUnit or ''}}" >
        {!! $errors->first('ProductionUnit', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>

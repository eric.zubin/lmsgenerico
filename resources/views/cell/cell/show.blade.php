@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Cell {{ $cell->id }}</h3>
                    @can('view-'.str_slug('Cell'))
                        <a class="btn btn-success pull-right" href="{{ url('/cell/cell') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $cell->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $cell->Code }} </td></tr><tr><th> Description </th><td> {{ $cell->Description }} </td></tr><tr><th> Program </th><td> {{ $cell->Program }} </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Training Request {{ $trainingrequest->id }}</h3>
                    @can('view-'.str_slug('TrainingRequest'))
                        <a class="btn btn-success pull-right" href="{{ url('/training_request/training-request') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $trainingrequest->id }}</td>
                            </tr>
                            <tr><th> User </th><td> {{ $trainingrequest->User }} </td></tr>
                            <tr><th> Training </th><td> {{ $trainingrequest->Training->Name }} </td></tr>
                            <tr><th> Date Today </th><td> {{ $trainingrequest->DateToday }} </td></tr>
                            <tr><th> Manager </th><td> {{ $trainingrequest->Manager }} </td></tr>
                            <tr><th> Curstomer </th><td> {{ $trainingrequest->Curstomer }} </td></tr>
                            <tr><th> Area </th><td> {{ $trainingrequest->Area }} </td></tr>
                            <tr><th> Signature </th><td> {{ $trainingrequest->Signature }} </td></tr>
                            <tr><th> Requestor </th><td> {{ $trainingrequest->Requestor }} </td></tr>
                            <tr><th> Phone </th><td> {{ $trainingrequest->Phone }} </td></tr>
                            <tr><th> Email </th><td> {{ $trainingrequest->Email }} </td></tr>
                            <tr><th> HrAprobal </th><td> {{ $trainingrequest->HrAprobal }} </td></tr>
                            <tr><th> Date </th><td> {{ $trainingrequest->Date }} </td></tr>
                            <tr><th> Current Situation </th><td> {{ $trainingrequest->CurrentSituation }} </td></tr>
                            <tr><th> Desired Situation </th><td> {{ $trainingrequest->DesiredSituation }} </td></tr>
                            <tr><th> Principal Reason </th><td> {{ $trainingrequest->PrincipalReason }} </td></tr>
                            <tr><th> Othre PrincipalReason </th><td> {{ $trainingrequest->OthrePrincipalReason }} </td></tr>
                            <tr><th> Request Name </th><td> {{ $trainingrequest->RequestName }} </td></tr>
                            <tr><th> Training Type </th><td> {{ $trainingrequest->TrainingType }} </td></tr>
                            <tr><th> Course Should </th><td> {{ $trainingrequest->CourseShould }} </td></tr>
                            <tr><th> Training Solutio nRequiered </th><td> {{ $trainingrequest->TrainingSolutionRequiered }} </td></tr>
                            <tr><th> Training Material Status </th><td> {{ $trainingrequest->TrainingMaterialStatus }} </td></tr>
                            <tr><th> Quantity People Trained </th><td> {{ $trainingrequest->QuantityPeopleTrained }} </td></tr>
                            <tr><th> Training Level </th><td> {{ $trainingrequest->TrainingLevel }} </td></tr>
                            <tr><th> Estimated Total Time </th><td> {{ $trainingrequest->EstimatedTotalTime }} </td></tr>
                            <tr><th> Date Trained Nedd </th><td> {{ $trainingrequest->DateTrainedNedd }} </td></tr>
                            <tr><th> Time Want People Want From </th><td> {{ $trainingrequest->DateTrainedNedd }} </td></tr>
                            <tr><th> TimeWantPeopleWantTo </th><td> {{ $trainingrequest->TimeWantPeopleWantTo }} </td></tr>
                            <tr><th> Validation Days </th><td> {{ $trainingrequest->ValidationDays }} </td></tr>
                            <tr><th> PlanDays </th><td> {{ $trainingrequest->PlanDays }} </td></tr>
                            <tr><th> Training Days </th><td> {{ $trainingrequest->TrainingDays }} </td></tr>
                            <tr><th> Evaluation Days </th><td> {{ $trainingrequest->EvaluationDays }} </td></tr>
                            <tr><th> Documented Days </th><td> {{ $trainingrequest->DocumentedDays }} </td></tr>
                            <tr><th> Archived Days </th><td> {{ $trainingrequest->ArchivedDays }} </td></tr>
                            <tr><th> Closes Days </th><td> {{ $trainingrequest->ClosesDays }} </td></tr>

                            
                            
                            
                            
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


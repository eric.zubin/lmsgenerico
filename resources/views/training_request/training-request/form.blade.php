@push('css')
    <link href="{{asset('plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/components/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/components/icheck/skins/all.css')}}" rel="stylesheet">

@endpush

<div class="form-group {{ $errors->has('User') ? 'has-error' : ''}}">
    <label for="User" class="col-md-4 control-label">{{ 'User' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="User" type="text" id="User" value="{{ Auth::user()->name }}" readonly>
        {!! $errors->first('User', '<p class="help-block">:message</p>') !!}
    </div>

<div class="form-group {{ $errors->has('DateToday') ? 'has-error' : ''}}">
    <label for="DateToday" class="col-md-4 control-label">{{ 'Date Today' }}</label>
    <div class="col-md-6">
    Date Today

    @isset($trainingrequest->DateToday)
                <input class="form-control" name="DateToday" type="date" id="DateToday" value="{{ Carbon\Carbon::parse($trainingrequest->DateToday)->format('Y-m-d')}}" readonly >
    @endisset
    @empty($trainingrequest->DateToday)
        <input class="form-control" name="DateToday" type="date" id="DateToday" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" readonly>
    @endisset
                

        {!! $errors->first('DateToday', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Manager') ? 'has-error' : ''}}">
    <label for="Manager" class="col-md-4 control-label">{{ 'Manager' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Manager" type="text" id="Manager" value="{{ $trainingrequest->Manager or ''}}" >
        {!! $errors->first('Manager', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('Curstomer') ? 'has-error' : ''}}">
    <label for="Curstomer" class="col-md-4 control-label">{{ 'Customer' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Curstomer" type="text" id="Curstomer" value="{{ $trainingrequest->Curstomer or ''}}" >
        {!! $errors->first('Curstomer', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Area') ? 'has-error' : ''}}">
    <label for="Area" class="col-md-4 control-label">{{ 'Area' }}</label>
    <div class="col-md-6">
             <select class="form-control select2" name="Area" id="Area" value="{{ $trainingrequest->Area or ''}}">
                                @isset($trainingrequest->Area)
                                        
                                        <option selected hidden value="{{$trainingrequest->Area}}">{{ $trainingrequest->Area }}</option>
                                @endisset
                                @foreach($area as $item)
                                    <option value="{{ $item->id }}">{{ $item->Name }}</option>
                                @endforeach
              </select>
        {!! $errors->first('Area', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('Requestor') ? 'has-error' : ''}}">
    <label for="Requestor" class="col-md-4 control-label">{{ 'Requestor' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Requestor" type="text" id="Requestor" value="{{ $trainingrequest->Requestor or ''}}" >
        {!! $errors->first('Requestor', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Phone') ? 'has-error' : ''}}">
    <label for="Phone" class="col-md-4 control-label">{{ 'Phone' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Phone" type="text" id="Phone" value="{{ $trainingrequest->Phone or ''}}" >
        {!! $errors->first('Phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('Email') ? 'has-error' : ''}}">
    <label for="Email" class="col-md-4 control-label">{{ 'Email' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Email" type="email" id="Email" value="{{ $trainingrequest->Email or ''}}" >
        {!! $errors->first('Email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('PrincipalReason') ? 'has-error' : ''}}">
    <label for="PrincipalReason" class="col-md-4 control-label">{{ 'Principan Reason of Request' }}</label>
    <div class="col-md-6">
        <select class="form-control" name="PrincipalReason" type="text" id="PrincipalReason" value="{{ $trainingrequest->PrincipalReason or ''}}">
                @isset($trainingrequest->PrincipalReason)
                <option selected value="{{$trainingrequest->PrincipalReason}}">{{ $trainingrequest->PrincipalReason }}</option>
                @endisset
                <option>New employee in the area</option>
                <option>Company Expansion</option>
                <option>New techniques or processes</option>
                <option>Re-emtry</option>
                <option>Flexibility / Polyvalence</option>
                <option>Human performance (quality)</option>
                <option>Transfer</option>
                <option>Job promotion</option>
                <option>Training plan requirment</option>
                <option>Other, please exmplain</option>
        </select>
        {!! $errors->first('PrincipalReason', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('OthrePrincipalReason') ? 'has-error' : ''}}">
    <label for="OthrePrincipalReason" class="col-md-4 control-label">{{ 'Other Reason' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="OthrePrincipalReason" type="text" id="OthrePrincipalReason" value="{{ $trainingrequest->OthrePrincipalReason or ''}}" >
        {!! $errors->first('OthrePrincipalReason', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('CurrentSituation') ? 'has-error' : ''}}">
    <label for="CurrentSituation" class="col-md-4 control-label">{{ 'Current Situation (What,When,where,who,how,how often,how many)' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Employees" type="text" id="Employees" value="{{ $trainingrequest->Employees or ''}}" >
        {!! $errors->first('CurrentSituation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('DesiredSituation') ? 'has-error' : ''}}">
<label for="DesiredSituation" class="col-md-4 control-label">{{ 'Desired Situation (What,When,where,who,how,how often,how many)' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="DesiredSituation" type="text" id="DesiredSituation" value="{{ $trainingrequest->DesiredSituation or ''}}" >
        {!! $errors->first('DesiredSituation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('RequestName') ? 'has-error' : ''}}">
    <label for="RequestName" class="col-md-4 control-label">{{ 'Request name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="RequestName" type="text" id="RequestName" value="{{ $trainingrequest->RequestName or ''}}" >
        {!! $errors->first('RequestName', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('TrainingMaterialStatus') ? 'has-error' : ''}}">

<div>
    <label for="TrainingType" class="col-md-3 control-label">{{ 'Training type' }}</label>
    <div class="col-md-3">
        <select class="form-control" name="TrainingType" type="text" id="TrainingType" value="{{ $trainingrequest->TrainingType or ''}}">
                @isset($trainingrequest->Type)
                <option selected value="{{$trainingrequest->Type}}">{{ $trainingrequest->Type }}</option>
                @endisset
                <option>BASIC</option>
                <option>CERTIFICATION</option>
                <option>SOFT (HUMAN)</option>
                <option>SPECIFIC</option>
                <option>CRITICAL JOB</option>
                <option>OTHER</option>
        </select>
        {!! $errors->first('TrainingType', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div>
    <label for="CourseShould1" class="col-md-3 control-label">{{ 'Internal/External' }}</label>
    <div class="col-md-3">
        <select class="form-control" name="CourseShould1" type="text" id="CourseShould1" value="{{ $trainingrequest->CourseShould1 or ''}}">
                @isset($trainingrequest->CourseShould1)
                <option selected value="{{$trainingrequest->CourseShould1}}">{{ $trainingrequest->CourseShould1 }}</option>
                @endisset
                <option>INTERNAL</option>
                <option>EXTERNAL</option>                
        </select>
        {!! $errors->first('CourseShould', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div>
    <label for="CourseShould2" class="col-md-3 control-label">{{ 'Individual/Group' }}</label>
    <div class="col-md-3">
        <select class="form-control" name="CourseShould2" type="text" id="CourseShould2" value="{{ $trainingrequest->CourseShould2 or ''}}">
                @isset($trainingrequest->CourseShould2)
                <option selected value="{{$trainingrequest->CourseShould2}}">{{ $trainingrequest->CourseShould2 }}</option>
                @endisset
                <option>INDIVIDUAL</option>
                <option>GROUP</option>
        </select>
        {!! $errors->first('CourseShould', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CourseShould3" class="col-md-3 control-label">{{ 'Insite/Outsite' }}</label>
    <div class="col-md-3">
        <select class="form-control" name="CourseShould3" type="text" id="CourseShould3" value="{{ $trainingrequest->CourseShould3 or ''}}">
                @isset($trainingrequest->CourseShould3)
                <option selected value="{{$trainingrequest->CourseShould3}}">{{ $trainingrequest->CourseShould3 }}</option>
                @endisset
                <option>INSITE</option>
                <option>OUTSITE</option>
               
        </select>
        {!! $errors->first('CourseShould', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CourseShould4" class="col-md-3 control-label">{{ 'Formative/Informative' }}</label>
    <div class="col-md-3">
        <select class="form-control" name="CourseShould4" type="text" id="CourseShould4" value="{{ $trainingrequest->CourseShould4 or ''}}">
                @isset($trainingrequest->CourseShould)
                <option selected value="{{$trainingrequest->CourseShould4}}">{{ $trainingrequest->CourseShould4 }}</option>
                @endisset
                <option>FORMATIVE</option>
                <option>INFORMATIVE</option>
        </select>
        {!! $errors->first('CourseShould4', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="TrainingSolutionRequiered" class="col-md-3 control-label">{{ 'Training Solution required' }}</label>
    <div class="col-md-3">
    @if(Auth::user()->hasRole('admin'))

        <select class="form-control" name="TrainingSolutionRequiered" type="text" id="TrainingSolutionRequiered" value="{{ $trainingrequest->TrainingSolutionRequiered or ''}}">

                @isset($trainingrequest->TrainingSolutionRequiered)
                <option selected value="{{$trainingrequest->TrainingSolutionRequiered}}">{{ $trainingrequest->TrainingSolutionRequiered }}</option>
                @endisset
                <option>THEORY</option>
                <option>PRACTICE/OJT</option>
                <option>COACHING/MENTORING</option>
                <option>E-LEARNING</option>
                <option>TRAINING ROOM</option>
                <option>TEAMBUILDING</option>
                <option>OTHER</option>

        </select>
        @else
        @isset($trainingrequest->TrainingSolutionRequiered)
        <select disabled class="form-control" name="TrainingSolutionRequiered" type="text" id="TrainingSolutionRequiered" value="{{ $trainingrequest->TrainingSolutionRequiered or ''}}">
                <option selected value="{{$trainingrequest->TrainingSolutionRequiered}}">{{ $trainingrequest->TrainingSolutionRequiered }}</option>
         </select>
        @endisset
        @endif

        {!! $errors->first('TrainingSolutionRequiered', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('TrainingMaterialStatus') ? 'has-error' : ''}}">
    <label for="TrainingMaterialStatus" class="col-md-4 control-label">{{ 'Training Material status' }}</label>
    <div class="col-md-6">
        <select class="form-control" name="TrainingMaterialStatus" type="text" id="TrainingMaterialStatus" value="{{ $trainingrequest->TrainingMaterialStatus or ''}}">
                @isset($trainingrequest->TrainingMaterialStatus)
                <option selected value="{{$trainingrequest->TrainingMaterialStatus}}">{{ $trainingrequest->TrainingMaterialStatus }}</option>
                @endisset
                <option>Training material and program ready to use</option>
                <option>To be translated / updated</option>
                <option>To being developed (Specific information)</option>
                <option>To being developed (Generic information)</option>
        </select>
        {!! $errors->first('TrainingMaterialStatus', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('QuantityPeopleTrained') ? 'has-error' : ''}}">
    <label for="QuantityPeopleTrained" class="col-md-4 control-label">{{ 'Quanty of people to be trained' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="QuantityPeopleTrained" type="number" min="1"  id="QuantityPeopleTrained" value="{{ $trainingrequest->QuantityPeopleTrained or ''}}" >
        {!! $errors->first('QuantityPeopleTrained', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('TrainingLevel') ? 'has-error' : ''}}">
    <label for="TrainingLevel" class="col-md-4 control-label">{{ 'Training level required' }}</label>
    <div class="col-md-6">
        <select class="form-control" name="TrainingLevel" type="text" id="TrainingLevel" value="{{ $trainingrequest->TrainingLevel or ''}}">
                @isset($trainingrequest->TrainingMaterialStatus)
                <option selected value="{{$trainingrequest->TrainingLevel}}">{{ $trainingrequest->TrainingLevel }}</option>
                @endisset
                <option>L1 - Ability Supervised</option>
                <option>L2 - Autonomous ability</option>
                <option>L3 - Coach (Teach others)</option>
        </select>


        {!! $errors->first('TrainingLevel', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('EstimatedTotalTime') ? 'has-error' : ''}}">
    <label for="EstimatedTotalTime" class="col-md-4 control-label">{{ 'Estimated total training time' }}</label>
    <div class="col-md-6">
        <input class="form-control" min=".1" step=".1" name="EstimatedTotalTime" type="number" id="EstimatedTotalTime" value="{{ $trainingrequest->EstimatedTotalTime or ''}}" >
        {!! $errors->first('EstimatedTotalTime', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<label for="DateTrainedNeddFrom" class="col-md-12 ">{{ 'Date you need the people trained' }}</label>

<div class="form-group {{ $errors->has('DateTrainedNeddFrom') ? 'has-error' : ''}}">
    <label for="DateTrainedNeddFrom" class="col-md-2 control-label">{{ 'From' }}</label>
    <div class="col-md-4">
        <input class="form-control" name="DateTrainedNeddFrom" type="time" id="DateTrainedNeddFrom" value="{{ $trainingrequest->DateTrainedNeddFrom or ''}}" >
        {!! $errors->first('DateTrainedNeddFrom', '<p class="help-block">:message</p>') !!}
    </div>


    <label for="DateTrainedNeddTo" class="col-md-2 control-label">{{ 'To' }}</label>
    <div class="col-md-4">
        <input class="form-control" name="DateTrainedNeddTo" type="time" id="DateTrainedNeddTo" value="{{ $trainingrequest->DateTrainedNeddTo or ''}}" >
        {!! $errors->first('DateTrainedNeddTo', '<p class="help-block">:message</p>') !!}
    </div>
</div>





<div class="form-group {{ $errors->has('ShiftsYoWantTRaing') ? 'has-error' : ''}}">
    <label for="ShiftsYoWantTRaing" class="col-md-4 control-label">{{ 'Shifts you want to train' }}</label>
    <div class="col-md-6">
    <ul class="icheck-list">

                                        <li>
                                            <input type="checkbox" class="check" id="ShiftsYoWantTRaing1" name="ShiftsYoWantTRaing1"  value="{{ $trainingrequest->ShiftsYoWantTRaing1 or ''}}" data-checkbox="icheckbox_flat-blue">
                                            <label for="flat-checkbox-2">1st</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" class="check" id="ShiftsYoWantTRaing2" name="ShiftsYoWantTRaing2" value="{{ $trainingrequest->ShiftsYoWantTRaing2 or ''}}" data-checkbox="icheckbox_flat-blue">
                                            <label for="flat-checkbox-1">2st</label>
                                        </li>
                                       
                                        <li>
                                            <input type="checkbox" class="check" id="ShiftsYoWantTRain3" name="ShiftsYoWantTRain3"  value="{{ $trainingrequest->ShiftsYoWantTRain3 or ''}}" data-checkbox="icheckbox_flat-blue">
                                            <label for="flat-checkbox-1">3st</label>
                                        </li>
                                        </ul>

        {!! $errors->first('ShiftsYoWantTRaing', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@if(Auth::user()->hasRole('admin'))
<div class="form-group {{ $errors->has('Employees') ? 'has-error' : ''}}">
    <label for="Employees" class="col-md-4 control-label">{{ 'Training Approval' }}</label>
    <div class="col-md-6">
    <input type="checkbox"   class="js-switch" data-color="#f96262" name="Signature" id="Signature" 
        @isset($trainingrequest->Signature  )
        @if($trainingrequest->Signature == 1) 
        checked 
        @endif 
        @endisset  >
        {!! $errors->first('Employees', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
<div class="form-group {{ $errors->has('Employees') ? 'has-error' : ''}}">
    <label for="Employees" class="col-md-4 control-label">{{ 'Training Approval' }}</label>
    <div class="col-md-6">
    <input type="checkbox" checked  class="js-switch" data-color="#f96262" name="Signature" id="Signature"  readonly="readonly" 
        @isset($trainingrequest->Signature  )
        @if($trainingrequest->Signature == 1) 
        checked 
        @endif 
        @endisset  
  >


        {!! $errors->first('Employees', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif

@if(Auth::user()->hasRole('admin'))
<div class="form-group {{ $errors->has('HrAprobal') ? 'has-error' : ''}}">
    <label for="HrAprobal" class="col-md-4 control-label">{{ 'HR Approval' }}</label>
    <div class="col-md-6">
        <input readonly="readonly" class="form-control" name="HrAprobal" type="text" id="HrAprobal" value="{{ Auth::user()->name }}" >
        {!! $errors->first('HrAprobal', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
<div class="form-group {{ $errors->has('HrAprobal') ? 'has-error' : ''}}">
    <label for="HrAprobal" class="col-md-4 control-label">{{ 'HR Approval' }}</label>
    <div class="col-md-6">
        <input readonly="readonly" class="form-control" name="HrAprobal" type="text" id="HrAprobal" value="{{ $trainingrequest->HrAprobal or ''}}" >
        {!! $errors->first('HrAprobal', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
<!--Son los datos que se van a ver solamente los admin-->
@if(Auth::user()->hasRole('admin'))

@endif



<div class="form-group {{ $errors->has('Date') ? 'has-error' : ''}}">
    <label for="Date" class="col-md-4 control-label">{{ 'Date Approval' }}</label>
    <div class="col-md-6">
        @if(Auth::user()->hasRole('admin'))

            @isset($trainingrequest->Date)
            <input readonly="readonly"  class="form-control" name="Date" type="date" id="Date" value="{{ Carbon\Carbon::parse($trainingrequest->Date)->format('Y-m-d')}}" readonly >
            @endisset

        @else
        @isset($trainingrequest->Date)
        <input readonly="readonly" class="form-control" name="Date" type="date" id="Date" value="{{ Carbon\Carbon::parse($trainingrequest->Date)->format('Y-m-d')}}" readonly>
        @endisset

        @endif

        {!! $errors->first('Date', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(!Auth::user()->hasRole('admin'))

</div><div class="form-group {{ $errors->has('Training') ? 'has-error' : ''}}">
    <label for="Training" class="col-md-4 control-label">{{ 'Training' }}</label>
    <div class="col-md-6">
                         <select readonly class="form-control select2" name="Training_id" id="Training_id" value="{{ $trainingrequest->Training_id or ''}}">
                                @isset($trainingrequest->Training_id)
                                        
                                        <option selected hidden value="{{$trainingrequest->Training_id}}">{{ $trainingrequest->Training->Code }}_ {{$trainingrequest->Training->Name}}</option>
                                @endisset
                                @foreach($training as $item)
                                    <option value="{{ $item->id }}">{{ $item->Code }}_{{ $item->Name }}</option>
                                @endforeach
                            </select>
        {!! $errors->first('Training', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div>
    <label for="ValidationDays" class="col-md-2 control-label">{{ 'Days Validation' }}</label>
    <div class="col-md-2">
        <input  readonly class="form-control" name="ValidationDays" type="number" id="ValidationDays" value="{{ $trainingrequest->ValidationDays or ''}}" >
        {!! $errors->first('ValidationDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckValidation" class="col-md-2 control-label">{{ 'Check Validation' }}</label>
    <div class="col-md-2">
    <input readonly type="checkbox"  class="js-switch" data-color="#f96262" name="CheckValidation" id="CheckValidation"
    @isset($trainingrequest->CheckValidation  )
        @if($trainingrequest->CheckValidation == 1) 
        checked 
        @endif 
        @endisset  
       />
    
        {!! $errors->first('CheckValidation', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="DateValidation" class="col-md-2 control-label">{{ 'Date Validation' }}</label>
    <div class="col-md-2">
        <input  readonly class="form-control" name="DateValidation" type="date" id="DateValidation" value="{{ $trainingrequest->DateValidation or ''}}" >
        {!! $errors->first('DateValidation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="PlanDays" class="col-md-2 control-label">{{ 'Days Plan' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="PlanDays" type="number" id="PlanDays" value="{{ $trainingrequest->PlanDays or ''}}" >
        {!! $errors->first('PlanDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="CheckPlan" class="col-md-2 control-label">{{ 'Check Plan' }}</label>
    <div class="col-md-2">
    <input readonly type="checkbox"  class="js-switch" data-color="#f96262" name="CheckPlan" id="CheckPlan" 
    @isset($trainingrequest->CheckPlan  )
        @if($trainingrequest->CheckPlan == 1) 
        checked 
        @endif 
        @endisset  
       />

    

        {!! $errors->first('CheckPlan', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="DatePlan" class="col-md-2 control-label">{{ 'Date Plan' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="DatePlan" type="date" id="DatePlan" value="{{ $trainingrequest->DatePlan or ''}}" >
        {!! $errors->first('DatePlan', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="TrainingDays" class="col-md-2 control-label">{{ 'Days Training' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="TrainingDays" type="number" id="TrainingDays" value="{{ $trainingrequest->TrainingDays or ''}}" >
        {!! $errors->first('TrainingDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckTraining" class="col-md-2 control-label">{{ 'Check Training' }}</label>
    <div class="col-md-2">
    <input readonly type="checkbox"  class="js-switch" data-color="#f96262" name="CheckTraining" id="CheckTraining" 
    @isset($trainingrequest->CheckTraining  )
        @if($trainingrequest->CheckTraining == 1) 
        checked 
        @endif 
        @endisset  
       />

        {!! $errors->first('CheckTraining', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="DateTraining" class="col-md-2 control-label">{{ 'Date Training' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="DateTraining" type="date" id="DateTraining" value="{{ $trainingrequest->DateTraining or ''}}" >
        {!! $errors->first('DateTraining', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="EvaluationDays" class="col-md-2 control-label">{{ 'Days Evaluation' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="EvaluationDays" type="number" id="EvaluationDays" value="{{ $trainingrequest->EvaluationDays or ''}}" >
        {!! $errors->first('EvaluationDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckEvaluation" class="col-md-2 control-label">{{ 'Check Evaluation' }}</label>
    <div class="col-md-2">
    <input readonly type="checkbox"  class="js-switch" data-color="#f96262" name="CheckEvaluation" id="CheckEvaluation" 
    @isset($trainingrequest->CheckEvaluation  )
        @if($trainingrequest->CheckEvaluation == 1) 
        checked 
        @endif 
        @endisset  
       />

        {!! $errors->first('CheckEvaluation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="DateEvaluation" class="col-md-2 control-label">{{ 'Date Evaluation' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="DateEvaluation" type="date" id="DateEvaluation" value="{{ $trainingrequest->DateEvaluation or ''}}" >
        {!! $errors->first('DateEvaluation', '<p class="help-block">:message</p>') !!}
    </div>
</div>




<div>
    <label for="DocumentedDays" class="col-md-2 control-label">{{ 'Days Documented' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="DocumentedDays" type="number" id="DocumentedDays" value="{{ $trainingrequest->DocumentedDays or ''}}" >
        {!! $errors->first('DocumentedDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckDocumented" class="col-md-2 control-label">{{ 'Check Documented' }}</label>
    <div class="col-md-2">
    <input readonly type="checkbox"  class="js-switch" data-color="#f96262" name="CheckDocumented" id="CheckDocumented"
    @isset($trainingrequest->CheckDocumented  )
        @if($trainingrequest->CheckDocumented == 1) 
        checked 
        @endif 
        @endisset  
       />

        {!! $errors->first('CheckDocumented', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div>
    <label for="DateDocumented" class="col-md-2 control-label">{{ 'Date Documented' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="DateDocumented" type="date" id="DateDocumented" value="{{ $trainingrequest->DateDocumented or ''}}" >
        {!! $errors->first('DateDocumented', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="ArchivedDays" class="col-md-2 control-label">{{ 'Days Archived' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="ArchivedDays" type="number" id="ArchivedDays" value="{{ $trainingrequest->ArchivedDays or ''}}" >
        {!! $errors->first('ArchivedDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="CheckArchived" class="col-md-2 control-label">{{ 'Check Archived' }}</label>
    <div class="col-md-2">
    <input readonly type="checkbox"  class="js-switch" data-color="#f96262" name="CheckArchived" id="CheckArchived" 
    @isset($trainingrequest->CheckArchived  )
        @if($trainingrequest->CheckArchived == 1) 
        checked 
        @endif 
        @endisset  
       />
        {!! $errors->first('CheckArchived', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="DateArchived" class="col-md-2 control-label">{{ 'Date Archived' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="DateArchived" type="date" id="DateArchived" value="{{ $trainingrequest->DateArchived or ''}}" >
        {!! $errors->first('DateArchived', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="ClosesDays" class="col-md-2 control-label">{{ 'Days Closed' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="ClosesDays" type="number" id="ClosesDays" value="{{ $trainingrequest->ClosesDays or ''}}" >
        {!! $errors->first('ClosesDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckClosed" class="col-md-2 control-label">{{ 'Check Closed' }}</label>
    <div class="col-md-2">
    <input readonly type="checkbox"  class="js-switch" data-color="#f96262" name="CheckClosed" id="CheckClosed" 
    @isset($trainingrequest->CheckClosed  )
        @if($trainingrequest->CheckClosed == 1) 
        checked 
        @endif 
        @endisset  
       />

        {!! $errors->first('CheckClosed', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="DateClosed" class="col-md-2 control-label">{{ 'Date Closed' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="DateClosed" type="date" id="DateClosed" value="{{ $trainingrequest->DateClosed or ''}}" >
        {!! $errors->first('DateClosed', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
@if(Auth::user()->hasRole('admin'))
</div><div class="form-group {{ $errors->has('Training') ? 'has-error' : ''}}">
    <label for="Training" class="col-md-4 control-label">{{ 'Training' }}</label>
    <div class="col-md-6">
                         <select class="form-control select2" name="Training_id" id="Training_id" value="{{ $trainingrequest->Training_id or ''}}">
                                @isset($trainingrequest->Training_id)
                                        
                                        <option selected hidden value="{{$trainingrequest->Training_id}}">{{ $trainingrequest->Training->Code }}_ {{$trainingrequest->Training->Name}}</option>
                                @endisset
                                @foreach($training as $item)
                                    <option value="{{ $item->id }}">{{ $item->Code }}_{{ $item->Name }}</option>
                                @endforeach
                            </select>
        {!! $errors->first('Training', '<p class="help-block">:message</p>') !!}
    </div>
</div>





<div>
    <label for="ValidationDays" class="col-md-2 control-label">{{ 'Days Validation' }}</label>
    <div class="col-md-2">
        <input  readonly class="form-control" name="ValidationDays" type="number" id="ValidationDays" value="{{ $trainingrequest->ValidationDays or ''}}" >
        {!! $errors->first('ValidationDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckValidation" class="col-md-2 control-label">{{ 'Check Validation' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="CheckValidation" id="CheckValidation"
    @isset($trainingrequest->CheckValidation  )
        @if($trainingrequest->CheckValidation == 1) 
        checked 
        @endif 
        @endisset  
       />
    
        {!! $errors->first('CheckValidation', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="DateValidation" class="col-md-2 control-label">{{ 'Date Validation' }}</label>
    <div class="col-md-2">
        <input  class="form-control" name="DateValidation" type="date" id="DateValidation" value="{{ $trainingrequest->DateValidation or ''}}" >
        {!! $errors->first('DateValidation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="PlanDays" class="col-md-2 control-label">{{ 'Days Plan' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="PlanDays" type="number" id="PlanDays" value="{{ $trainingrequest->PlanDays or ''}}" >
        {!! $errors->first('PlanDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="CheckPlan" class="col-md-2 control-label">{{ 'Check Plan' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="CheckPlan" id="CheckPlan" 
    @isset($trainingrequest->CheckPlan  )
        @if($trainingrequest->CheckPlan == 1) 
        checked 
        @endif 
        @endisset  
       />

    

        {!! $errors->first('CheckPlan', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="DatePlan" class="col-md-2 control-label">{{ 'Date Plan' }}</label>
    <div class="col-md-2">
        <input class="form-control" name="DatePlan" type="date" id="DatePlan" value="{{ $trainingrequest->DatePlan or ''}}" >
        {!! $errors->first('DatePlan', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="TrainingDays" class="col-md-2 control-label">{{ 'Days Training' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="TrainingDays" type="number" id="TrainingDays" value="{{ $trainingrequest->TrainingDays or ''}}" >
        {!! $errors->first('TrainingDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckTraining" class="col-md-2 control-label">{{ 'Check Training' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="CheckTraining" id="CheckTraining" 
    @isset($trainingrequest->CheckTraining  )
        @if($trainingrequest->CheckTraining == 1) 
        checked 
        @endif 
        @endisset  
       />

        {!! $errors->first('CheckTraining', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="DateTraining" class="col-md-2 control-label">{{ 'Date Training' }}</label>
    <div class="col-md-2">
        <input class="form-control" name="DateTraining" type="date" id="DateTraining" value="{{ $trainingrequest->DateTraining or ''}}" >
        {!! $errors->first('DateTraining', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="EvaluationDays" class="col-md-2 control-label">{{ 'Days Evaluation' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="EvaluationDays" type="number" id="EvaluationDays" value="{{ $trainingrequest->EvaluationDays or ''}}" >
        {!! $errors->first('EvaluationDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckEvaluation" class="col-md-2 control-label">{{ 'Check Evaluation' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="CheckEvaluation" id="CheckEvaluation" 
    @isset($trainingrequest->CheckEvaluation  )
        @if($trainingrequest->CheckEvaluation == 1) 
        checked 
        @endif 
        @endisset  
       />

        {!! $errors->first('CheckEvaluation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="DateEvaluation" class="col-md-2 control-label">{{ 'Date Evaluation' }}</label>
    <div class="col-md-2">
        <input class="form-control" name="DateEvaluation" type="date" id="DateEvaluation" value="{{ $trainingrequest->DateEvaluation or ''}}" >
        {!! $errors->first('DateEvaluation', '<p class="help-block">:message</p>') !!}
    </div>
</div>




<div>
    <label for="DocumentedDays" class="col-md-2 control-label">{{ 'Days Documented' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="DocumentedDays" type="number" id="DocumentedDays" value="{{ $trainingrequest->DocumentedDays or ''}}" >
        {!! $errors->first('DocumentedDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckDocumented" class="col-md-2 control-label">{{ 'Check Documented' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="CheckDocumented" id="CheckDocumented"
    @isset($trainingrequest->CheckDocumented  )
        @if($trainingrequest->CheckDocumented == 1) 
        checked 
        @endif 
        @endisset  
       />

        {!! $errors->first('CheckDocumented', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div>
    <label for="DateDocumented" class="col-md-2 control-label">{{ 'Date Documented' }}</label>
    <div class="col-md-2">
        <input class="form-control" name="DateDocumented" type="date" id="DateDocumented" value="{{ $trainingrequest->DateDocumented or ''}}" >
        {!! $errors->first('DateDocumented', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="ArchivedDays" class="col-md-2 control-label">{{ 'Days Archived' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="ArchivedDays" type="number" id="ArchivedDays" value="{{ $trainingrequest->ArchivedDays or ''}}" >
        {!! $errors->first('ArchivedDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="CheckArchived" class="col-md-2 control-label">{{ 'Check Archived' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="CheckArchived" id="CheckArchived" 
    @isset($trainingrequest->CheckArchived  )
        @if($trainingrequest->CheckArchived == 1) 
        checked 
        @endif 
        @endisset  
       />
        {!! $errors->first('CheckArchived', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="DateArchived" class="col-md-2 control-label">{{ 'Date Archived' }}</label>
    <div class="col-md-2">
        <input class="form-control" name="DateArchived" type="date" id="DateArchived" value="{{ $trainingrequest->DateArchived or ''}}" >
        {!! $errors->first('DateArchived', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="ClosesDays" class="col-md-2 control-label">{{ 'Days Closed' }}</label>
    <div class="col-md-2">
        <input readonly class="form-control" name="ClosesDays" type="number" id="ClosesDays" value="{{ $trainingrequest->ClosesDays or ''}}" >
        {!! $errors->first('ClosesDays', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div>
    <label for="CheckClosed" class="col-md-2 control-label">{{ 'Check Closed' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="CheckClosed" id="CheckClosed" 
    @isset($trainingrequest->CheckClosed  )
        @if($trainingrequest->CheckClosed == 1) 
        checked 
        @endif 
        @endisset  
       />

        {!! $errors->first('CheckClosed', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div>
    <label for="DateClosed" class="col-md-2 control-label">{{ 'Date Closed' }}</label>
    <div class="col-md-2">
        <input class="form-control" name="DateClosed" type="date" id="DateClosed" value="{{ $trainingrequest->DateClosed or ''}}" >
        {!! $errors->first('DateClosed', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('TrainingCost') ? 'has-error' : ''}}">
    <label for="TrainingCost" class="col-md-4 control-label">{{ 'Training cost (USD)' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="TrainingCost" type="text" id="TrainingCost" value="{{ $trainingrequest->TrainingCost or ''}}" >
        {!! $errors->first('TrainingCost', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Coin') ? 'has-error' : ''}}">
    <label for="Coin" class="col-md-4 control-label">{{ 'Coin' }}</label>
    <div class="col-md-6">
        <select class="form-control" name="Coin" type="text" id="Coin" value="{{ $trainingrequest->Coin or ''}}">
                @isset($trainingrequest->TrainingMaterialStatus)
                <option selected value="{{$trainingrequest->Coin}}">{{ $trainingrequest->Coin }}</option>
                @endisset
                <option value="USD">USD</option>
        </select>


        {!! $errors->first('Coin', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('PO') ? 'has-error' : ''}}">
    <label for="PO" class="col-md-4 control-label">{{ 'PO' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="PO" type="text" id="PO" value="{{ $trainingrequest->PO or ''}}" >
        {!! $errors->first('PO', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('TrainingPlan') ? 'has-error' : ''}}">
    <label for="TrainingPlan_id" class="col-md-4 control-label">{{ 'Training Plan' }}</label>
    <div class="col-md-6">
    <select class="form-control select2" name="TrainingPlan" id="TrainingPlan" value="{{ $trainingrequest->TrainingPlan_id or ''}}">
    
                                @isset($trainingrequest->TrainingPlan_id)
                                        <option selected hidden value="{{$trainingrequest->TrainingPlan_id}}">{{ $trainingrequest->TrainingPlan->id }} {{ $trainingrequest->TrainingPlan->Code }} - {{$trainingrequest->TrainingPlan->Name}}</option>
                                @endisset

                                @foreach($trainingPlan as $item)
                                
                                    <option value="{{ $item->id }}">{{ $item->id }}  {{ $item->Code }}_{{ $item->Training->Name }}</option>
                                @endforeach
                            </select>

        </select>
        {!! $errors->first('TrainingPlan_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>





<div class="form-group {{ $errors->has('MaterialText') ? 'has-error' : ''}}">
    <label for="MaterialText" class="col-md-4 control-label">{{ 'Material (Consumables,software,hardware,tools,etc).' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="MaterialText" type="text" id="MaterialText" value="{{ $trainingrequest->MaterialText or ''}}" >
        {!! $errors->first('MaterialText', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('Rejeted') ? 'has-error' : ''}}">
    <label for="ValidationDays" class="col-md-2 control-label">{{ 'Required' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="Rejeted" id="Rejeted" value="{{ $trainingrequest->Rejeted or '0'}}"/>
        {!! $errors->first('Rejeted', '<p class="help-block">:message</p>') !!}
    </div>





    <label for="Company" class="col-md-2 control-label">{{ 'Company' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  data-color="#f96262" name="Company" id="Company" value="{{ $trainingrequest->Company or '0'}}"/>
        {!! $errors->first('Company', '<p class="help-block">:message</p>') !!}
    </div>
    </div>


    <div class="form-group {{ $errors->has('Supplier') ? 'has-error' : ''}}">

    <label for="ValidationDays" class="col-md-2 control-label">{{ 'Supplier' }}</label>
    <div class="col-md-2">
    <input type="checkbox"   data-color="#f96262" name="Supplier" id="Supplier" value="{{ $trainingrequest->Supplier or '0'}}"/>
        {!! $errors->first('Supplier', '<p class="help-block">:message</p>') !!}
    </div>
</div>





<div class="form-group {{ $errors->has('SpecifiationsNeddCouse') ? 'has-error' : ''}}">
    <label for="SpecifiationsNeddCouse" class="col-md-4 control-label">{{ 'Specifiations that need to be covered during the couse' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="SpecifiationsNeddCouse" type="text" id="SpecifiationsNeddCouse" value="{{ $trainingrequest->SpecifiationsNeddCouse or ''}}" >
        {!! $errors->first('SpecifiationsNeddCouse', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('InstructorAvalable') ? 'has-error' : ''}}">
    <label for="InstructorAvalable" class="col-md-2 control-label">{{ 'Instructor/Coach avalable' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="Signature" id="Signature" value="{{ $trainingrequest->Signature or '0'}}"/>
        {!! $errors->first('InstructorAvalable', '<p class="help-block">:message</p>') !!}
    </div>

    <label for="InstructorApproved" class="col-md-2 control-label">{{ 'Instructor/Coach approved' }}</label>
    <div class="col-md-2">
    <input type="checkbox"  class="js-switch" data-color="#f96262" name="InstructorApproved" id="InstructorApproved" value="{{ $trainingrequest->InstructorApproved or '0'}}"/>
        {!! $errors->first('InstructorApproved', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('InstructorCoachExternal') ? 'has-error' : ''}}">
    <label for="InstructorCoachExternal" class="col-md-4 control-label">{{ 'Instructor/Coach external' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="InstructorCoachExternal" type="text" id="InstructorCoachExternal" value="{{ $trainingrequest->InstructorCoachExternal or ''}}" >
        {!! $errors->first('InstructorCoachExternal', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('SuggetedAmountOfPeople') ? 'has-error' : ''}}">
    <label for="SuggetedAmountOfPeople" class="col-md-4 control-label">{{ 'Suggeted amount of people in training' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="SuggetedAmountOfPeople" type="text" id="SuggetedAmountOfPeople" value="{{ $trainingrequest->RequestName or ''}}" >
        {!! $errors->first('SuggetedAmountOfPeople', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('SuggetedInstructor') ? 'has-error' : ''}}">
    <label for="SuggetedInstructor" class="col-md-4 control-label">{{ 'Suggeted instructor / coach time' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="SuggetedInstructor" type="text" id="SuggetedInstructor" value="{{ $trainingrequest->SuggetedInstructor or ''}}" >
        {!! $errors->first('SuggetedInstructor', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('RequestName') ? 'has-error' : ''}}">
    <label for="RequestName" class="col-md-4 control-label">{{ 'Estimated end date of completed training' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="RequestName" type="text" id="RequestName" value="{{ $trainingrequest->RequestName or ''}}" >
        {!! $errors->first('RequestName', '<p class="help-block">:message</p>') !!}
    </div>
</div>




<div class="form-group {{ $errors->has('InstructorComments') ? 'has-error' : ''}}">
    <label for="InstructorComments" class="col-md-4 control-label">{{ 'Instructor Comments' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="InstructorComments" type="text" id="InstructorComments" value="{{ $trainingrequest->InstructorComments or ''}}" >
        {!! $errors->first('InstructorComments', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@endif



<div class="form-group">
    <div class="col-md-12">
        <input class="btn btn-primary btn-lg btn-block" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>



@push('js')
<script src="{{asset('plugins/components/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('plugins/components/icheck/icheck.init.js')}}"></script>
    <script src="{{asset('plugins/components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset('plugins/components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
       
            // For select 2
            $(".select2").select2();
        
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
        });
    </script>
@endpush

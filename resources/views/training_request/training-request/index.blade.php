@extends('layouts.master')

@push('css')
    <link href="{{asset('plugins/components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>
@endpush

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Training Request</h3>
                        <a class="btn btn-success pull-right" href="{{ url('/training_request/training-request/create') }}"><i
                                    class="icon-plus"></i> Add Training Request</a>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table" id="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Actions</th>

                                <th>User</th><th>Training</th><th>DateToday</th>
                                <th> Manager </th>
                                <th> Curstomer </th>
                                <th> Area </th>
                                <th> Signature </th>
                                <th> Requestor </th>
                                <th> Phone </th>
                                <th> Email </th>
                                <th> HrAprobal </th>
                                <th> Date </th>
                                <th> Current Situation </th>
                                <th> Desired Situation </th>
                                <th> Principal Reason </th>
                                <th> Othre PrincipalReason </th>
                                <th> Request Name </th>
                                <th> Training Type </th>
                                <th> Course Should </th>
                                <th> Training Solutio nRequiered </th>
                                <th> Training Material Status </th>
                                <th> Quantity People Trained </th>
                                <th> Training Level </th>
                                <th> Estimated Total Time </th>
                                <th> Date Trained Nedd </th>
                                <th> Time Want People Want From </th>
                                <th> TimeWantPeopleWantTo </th>
                                <th> Validation Days </th>
                                <th> PlanDays </th>
                                <th> Training Days </th>
                                <th> Evaluation Days </th>
                                <th> Documented Days </th>
                                <th> Archived Days </th>
                                <th> Closes Days </th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trainingrequest as $item)
                                <tr>
                                    <td>{{ $loop->iteration or $item->id }}</td>
                                    <td>
                                            <a href="{{ url('/training_request/training-request/' . $item->id) }}"
                                               title="View item">
                                                <button class="btn btn-info btn-sm">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> View
                                                </button>
                                            </a>

                                            <a href="{{ url('/training_request/training-request/' . $item->id . '/edit') }}"
                                               title="Edit item">
                                                <button class="btn btn-primary btn-sm">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                </button>
                                            </a>

                                            @if(Auth::user()->hasRole('admin'))
                                            <form method="POST"
                                                  action="{{ url('/training_request/training-request' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm"
                                                        title="Delete item"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                </button>
                                            </form>
                                        @endif


                                    </td>

                                    <td>{{ $item->User }}</td><td>{{ $item->Training->Name }}</td><td>{{ $item->DateToday }}</td>
                                   <td> {{ $item->Manager }} </td>
                                   <td> {{ $item->Curstomer }} </td>
                                   <td> {{ $item->Area }} </td>
                                   <td> {{ $item->Signature }} </td>
                                   <td> {{ $item->Requestor }} </td>
                                   <td> {{ $item->Phone }} </td>
                                   <td> {{ $item->Email }} </td>
                                   <td> {{ $item->HrAprobal }} </td>
                                   <td> {{ $item->Date }} </td>
                                   <td> {{ $item->CurrentSituation }} </td>
                                   <td> {{ $item->DesiredSituation }} </td>
                                   <td> {{ $item->PrincipalReason }} </td>
                                   <td> {{ $item->OthrePrincipalReason }} </td>
                                   <td> {{ $item->RequestName }} </td>
                                   <td> {{ $item->TrainingType }} </td>
                                   <td> {{ $item->CourseShould }} </td>
                                   <td> {{ $item->TrainingSolutionRequiered }} </td>
                                   <td> {{ $item->TrainingMaterialStatus }} </td>
                                   <td> {{ $item->QuantityPeopleTrained }} </td>
                                   <td> {{ $item->TrainingLevel }} </td>
                                   <td> {{ $item->EstimatedTotalTime }} </td>
                                   <td> {{ $item->DateTrainedNedd }} </td>
                                   <td> {{ $item->DateTrainedNedd }} </td>
                                   <td> {{ $item->TimeWantPeopleWantTo }} </td>
                                   <td> {{ $item->ValidationDays }} </td>
                                   <td> {{ $item->PlanDays }} </td>
                                   <td> {{ $item->TrainingDays }} </td>
                                   <td> {{ $item->EvaluationDays }} </td>
                                   <td> {{ $item->DocumentedDays }} </td>
                                   <td> {{ $item->ArchivedDays }} </td>
                                   <td> {{ $item->ClosesDays }} </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $trainingrequest->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>

    <script src="{{asset('plugins/components/datatables/jquery.dataTables.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>   
    <!-- end - This is for export functionality only -->
    <script>
        $(document).ready(function () {

            @if(\Session::has('message'))
            $.toast({
                heading: 'Success!',
                position: 'top-center',
                text: '{{session()->get('message')}}',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3000,
                stack: 6
            });
            @endif
        })

        $(function () {
            $('#myTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
        ],
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [-1] /* 1st one, start by the right */
                }]
            });


        });
    </script>
   <style>
    #myTable_paginate{
        background-color: white !important;
    }
    </style>
@endpush

@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Competive {{ $competive->id }}</h3>
                    @can('view-'.str_slug('competive'))
                        <a class="btn btn-success pull-right" href="{{ url('/competive/competive') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $competive->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $competive->Code }} </td></tr>
                            <tr><th> Name </th><td> {{ $competive->Name }} </td></tr>
                            <tr><th> Description </th><td> {{ $competive->Description }} </td></tr>
                            <tr><th> Type </th><td> {{ $competive->Type }} </td></tr>
                            <tr><th> Level </th><td> {{ $competive->Level }} </td></tr>
                            <tr><th> Specs </th><td>
                            
                            @foreach($competive->Specs as $item)
                                {{ $item->Name }} ,
                                @endforeach

                              </td></tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


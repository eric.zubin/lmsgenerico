@push('css')
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
@endpush

<div class="form-group {{ $errors->has('Code') ? 'has-error' : ''}}">
    <label for="Code" class="col-md-4 control-label">{{ 'Code' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Code" type="text" id="Code" value="{{ $competive->Code or ''}}" >
        {!! $errors->first('Code', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $competive->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
    <label for="Description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Description" type="text" id="Description" value="{{ $competive->Description or ''}}" >
        {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('Type') ? 'has-error' : ''}}">
    <label for="Type" class="col-md-4 control-label">{{ 'Type' }}</label>
    <div class="col-md-6">

        <select class="form-control" name="Type" type="text" id="Type" value="{{ $competive->Type or ''}}">
                 @isset($competive->Type)
                <option selected hidden value="{{$competive->Type}}">{{ $competive->Type }}</option>
                @endisset
                <option>BASIC</option>
                <option>CERTIFICATION</option>
                <option>CULTURAL</option>
                <option>SOFT</option>
                <option>SPECIFIC</option>
                <option>FUNCIONAL</option>
                <option>CRITICALJOB</option>
        </select>

        {!! $errors->first('Type', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Level') ? 'has-error' : ''}}">
    <label for="Level" class="col-md-4 control-label">{{ 'Level' }}</label>
    <div class="col-md-6">
         <select class="form-control" name="Level" id="Level" value="{{ $competive->Level or ''}}">
                @isset($competive->Level)
                <option selected hidden value="{{$competive->Level}}">{{ $competive->Level }}</option>
                @endisset
                <option>0</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
            </select>
        {!! $errors->first('Level', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Specs') ? 'has-error' : ''}}">
    <h5 for="Specs" class="col-md-4 control-label">{{ 'Specs' }}</h5>
    <div class="col-md-6">
                    <select class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Choose" name="Specs[]" id="Specs">
                        <optgroup label="Selecciona tu Specs">
                        @isset($competive->Specs)
                             @foreach($competive->Specs as $item)
                                <option value="{{$item->id}}" selected="selected">{{ $item->Name }}</option>
                                @endforeach
                        @endisset


                        @foreach($specs as $item)
                            <option value="{{ $item->id }}">{{ $item->Name }}</option>
                        @endforeach
                        </optgroup>
                    </select>
        {!! $errors->first('Specs', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>


@push('js')
    <script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
         
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

        });
    </script>
@endpush

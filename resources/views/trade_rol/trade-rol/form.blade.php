@push('css')
    <link href="{{asset('plugins/components/custom-select/custom-select.css')}}" rel="stylesheet" type="text/css" />
   
@endpush





<div class="form-group {{ $errors->has('ProductionUnit_id') ? 'has-error' : ''}}">
    <label for="ProductionUnit_id" class="col-md-4 control-label">{{ 'ProductionUnit_id' }}</label>
    <div class="col-md-6">
                     <select class="form-control" name="ProductionUnit_id"  id="ProductionUnit_id" value="{{ $traderol->ProductionUnit_id or ''}}">
                        @isset($traderol->ProductionUnit_id)
                                <option selected hidden value="{{$traderol->ProductionUnit_id}}">{{ $traderol->ProductionUnit->Name }}</option>
                        @endisset
                        @foreach($productionUnits as $item)
                                    <option value="{{ $item->id }}">{{ $item->Name }}</option>
                        @endforeach
                    </select>
        {!! $errors->first('ProductionUnit_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Program_id') ? 'has-error' : ''}}">
    <label for="Program_id" class="col-md-4 control-label">{{ 'Program_id' }}</label>
    <div class="col-md-6">
             <select class="form-control" name="Program_id"  id="Program_id" value="{{ $traderol->Program_id or ''}}">
             </select>

        {!! $errors->first('Program_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('id') ? 'has-error' : ''}}">
    <label for="id" class="col-md-4 control-label">{{ 'id' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="id" type="text" id="id" value="{{ $traderol->id or ''}}" >
        {!! $errors->first('id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $traderol->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
    <label for="Description" class="col-md-4 control-label">{{ 'Description' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Description" type="text" id="Description" value="{{ $traderol->Description or ''}}" >
        {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('Competives') ? 'has-error' : ''}}">
    <label for="Competives" class="col-md-4 control-label">{{ 'Competence' }}</label>
    <div class="col-md-6">
                    <select class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Choose" name="Competives[]" id="Competives">
                        <optgroup label="Choose Competives">
                        @isset($traderol->Competives)
                                @foreach($traderol->Competives as $item)
                                <option value="{{$item->id}}" selected="selected">{{ $item->Name }}</option>
                                @endforeach
                        @endisset
                        @foreach($competives as $item)
                            <option value="{{ $item->id }}">{{ $item->Name }}</option>
                        @endforeach
                        </optgroup>
                    </select>
        {!! $errors->first('Competives', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('RolLevel') ? 'has-error' : ''}}">
    <label for="RolLevel" class="col-md-4 control-label">{{ 'Rol Level' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="RolLevel" type="text" id="RolLevel" value="{{ $traderol->RolLevel or ''}}" >
        {!! $errors->first('RolLevel', '<p class="help-block">:message</p>') !!}
    </div>
</div>




<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>



@push('js')

<script src="{{asset('plugins/components/custom-select/custom-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('plugins/components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function() {
         
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();

        });
    </script>

<script>
   $("#ProductionUnit_id").change(function () {
					$("#ProductionUnit_id option:selected").each(function () {
						var id = $("#ProductionUnit_id").val();

                            $.ajax({
                                url: "/select_program/"+id,
                                type: "post",
                                dataType: 'json',
                                data: { "_token": "{{ csrf_token() }}"} ,
                                success: function (response) {
                                                $('#Program_id').empty().append( response.html);
                                        },
                                        error: function(jqXHR, textStatus, errorThrown) {
                                        console.log(textStatus, errorThrown);
                                        }


                            });

					});
				})


var id = $("#ProductionUnit_id").val();
$.ajax({
    url: "/select_program/"+id,
    dataType: 'json',
    type: "post",
    data: { "_token": "{{ csrf_token() }}"} ,
    success: function (response) {
                    $('#Program_id').empty().append( response.html);
                    @isset($traderol->Program_id)
                        $("#Program_id").val({{$traderol->Program_id}});
                    @endisset
            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }


});

</script>
@endpush
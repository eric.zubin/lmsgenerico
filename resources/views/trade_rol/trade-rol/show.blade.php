@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Trade Rol {{ $traderol->id }}</h3>
                    @can('view-'.str_slug('TradeRol'))
                        <a class="btn btn-success pull-right" href="{{ url('/trade_rol/trade-rol') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $traderol->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $traderol->Code }} </td></tr>
                            <tr><th> Production Unit </th><td> 
                            @if($traderol->ProductionUnit != null)
                            {{ $traderol->ProductionUnit->Name  }} 
                            @endif
                            </td></tr>
                            <tr><th> Program </th><td> 
                            @if($traderol->Program != null)
                            {{ $traderol->Program->Name  }} 
                            @endif

                             </td></tr>
                            <tr><th> Name </th><td> {{ $traderol->Name }} </td></tr>
                            <tr><th> Description </th><td> {{ $traderol->Description }} </td></tr>
                            <tr><th> Competence </th><td> 
                            @foreach($traderol->Competives as $item)
                            <ul>{{ $item->Name }} </ul>
                            @endforeach
                            </td></tr>

                         
                            
                         
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


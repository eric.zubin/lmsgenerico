@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Company Instructor {{ $companyinstructor->id }}</h3>
                    @can('view-'.str_slug('companyInstructor'))
                        <a class="btn btn-success pull-right" href="{{ url('/company_instructor/company-instructor') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $companyinstructor->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $companyinstructor->Code }} </td></tr><tr><th> Name </th><td> {{ $companyinstructor->Name }} </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


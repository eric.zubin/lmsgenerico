@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Production Unit {{ $productionunit->id }}</h3>
                    @can('view-'.str_slug('productionUnit'))
                        <a class="btn btn-success pull-right" href="{{ url('/productionUnit/production-unit') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <td>{{ $productionunit->id }}</td>
                            </tr>
                            <tr><th> Code </th><td> {{ $productionunit->Code }} </td></tr><tr><th> Name </th><td> {{ $productionunit->Name }} </td></tr><tr><th> Description </th><td> {{ $productionunit->Description }} </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('css')
    <link href="{{asset('plugins/components/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('plugins/components/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{asset('plugins/components/icheck/skins/all.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet"/>
    {{--<link href="{{asset('plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">--}}
    <link href="{{asset('plugins/components/jqueryui/jquery-ui.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">

@endpush


<input type="checkbox" checked class="js-switch" data-color="#f96262" name="Status" id="Status" value="{{ $instructor->Status or '1'}}"/>



<div class="form-group {{ $errors->has('Num') ? 'has-error' : ''}}">
    <label for="Num" class="col-md-4 control-label">{{ 'Num' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Num" type="text" id="Num" value="{{ $instructor->Num or ''}}" >
        {!! $errors->first('Num', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
    <label for="Name" class="col-md-4 control-label">{{ 'Name' }}</label>
    <div class="col-md-6">
        <input class="form-control" name="Name" type="text" id="Name" value="{{ $instructor->Name or ''}}" >
        {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('Company') ? 'has-error' : ''}}">
    <label for="Company" class="col-md-4 control-label">{{ 'Company' }}</label>
    <div class="col-md-6">
    
            <select class="form-control" name="company_instructor_id" id="company_instructor_id"  value="{{ $instructor->company_instructor_id or ''}}">
            @isset($instructor->company_instructor_id)
            <option selected disabled hidden value="$instructor->companyInstructor->id">{{ $instructor->companyInstructor->Name }}</option>
            @endisset
            @foreach($companyInstructors as $item)
            <option value="{{ $item->id }}" >{{ $item->Name }}</option>
            @endforeach
               
            </select>
        {!! $errors->first('Company', '<p class="help-block">:message</p>') !!}
    </div>
</div>

                <div class="form-group {{ $errors->first('pic_file', 'has-error') }}">
                    <label for="pic" class="col-md-4  control-label">Profile picture</label>
                    <div class="col-md-6">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail"
                                    style="width: 200px; height: 200px;">
                                <img src="/storage/uploads/instructor/{{ $instructor->pic or 'no_avatar.png'}}" alt="profile pic">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                    style="max-width: 200px; max-height: 200px;"></div>
                            <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input id="pic" name="pic_file" type="file" class="form-control"/>
                            </span>
                                <a href="#" class="btn btn-danger fileinput-exists"
                                    data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                        <span class="help-block">{{ $errors->first('pic_file', ':message') }}</span>
                    </div>
                </div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Create' }}">
    </div>
</div>


@push('js')
<script src="{{asset('plugins/components/switchery/dist/switchery.min.js')}}"></script>
<script src="{{ asset('plugins/components/jasny-bootstrap/js/jasny-bootstrap.js') }}"></script>
    <script src="{{asset('plugins/components/icheck/icheck.min.js')}}"></script>
    <script src="{{asset('plugins/components/icheck/icheck.init.js')}}"></script>
    <script src="{{asset('plugins/components/moment/moment.js')}}"></script>
    {{--<script src="{{asset('plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>--}}
    <script src="{{asset('plugins/components/jqueryui/jquery-ui.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"
            type="text/javascript"></script>
    <script src="{{asset('plugins/components/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{ asset('/js/adduser.js') }}"></script>
    
<script>

jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

});
</script>
@endpush
@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title pull-left">Instructor {{ $instructor->id }}</h3>
                    @can('view-'.str_slug('instructor'))
                        <a class="btn btn-success pull-right" href="{{ url('/instructor/instructor') }}">
                            <i class="icon-arrow-left-circle" aria-hidden="true"></i> Back</a>
                    @endcan
                    <div class="clearfix"></div>
                    <img src="/storage/uploads/instructor/{{ $instructor->pic or 'no_avatar.png'}}" alt="profile pic">

                    <hr>
                    <div class="table-responsive">
                        <table class="table table">
                            <tbody>
                            <tr>
                                <th>ID</th>

                                
                                <td>{{ $instructor->id }}</td>



                            </tr>
                    
                            <tr><th> Num </th><td> {{ $instructor->Num }} </td></tr><tr><th> Name </th><td> {{ $instructor->Name }} </td></tr><tr><th> Company </th><td> {{ $instructor->companyInstructor->Name }} </td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
    img{
        position: relative;
        width: 100px;
        height: 100px;
    }
    </style>
@endsection

